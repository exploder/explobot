# GDPR-related commands.

import logging
import json
from io import StringIO
from datetime import datetime, timezone

import discord
from discord.ext import commands

import db
import processors
from util import CustomContext


# Discord max file size is exactly 25 MiB
DISCORD_MAX_FILE_SIZE = 25 * 1024 * 1024

GDPR_HOW_TO_READ = """
Lähetän yhden tai useamman tiedoston. Tässä ohjeet niiden lukemiseen:
`gdpr.json`:
- sisältää kaikkien tallennettujen viestien tekstisisällön, lähetysajan, kanavan ja botin viesti-ID:n
- tämä `message_id` EI OLE alkuperäisen viestisi ID, vaan botin poistolokiin lähettämän viestin ID

`attachments_<numero>.zip`:
- kaikki tallennetut viestien liitteet lähetetään ZIP-tiedostoina
- liitteet on jaoteltu kansioihin botin viesti-ID:n mukaan (selitetty edellä)
- koska botti saa lähettää korkeintaan 25 MiB tiedostoja, ZIP jaetaan useampiin osiin tarpeen mukaan
"""


class GDPR(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='gdpr')
    @commands.guild_only()
    async def gdpr(self, ctx: CustomContext):
        """
        Tekee GDPR-pyynnön eli lähettää kaiken poistolokiin käyttäjästä
        tallennetun tiedon käyttäjälle.
        """
        user = ctx.author
        async with ctx.db_session.begin():
            setting = await db.GuildSetting.settings_for(
                ctx.db_session,
                ctx.guild.id,
                settings=db.GuildSettingInfo.DELETE_CHANNEL
            )

        if setting is None:
            await ctx.send(
                f'{user.mention}, tällä serverillä ei ole poistolokia!'
            )
            return

        dms = user.dm_channel
        if dms is None:
            dms = await user.create_dm()

        self._logger.info(f'Processing a GDPR request {ctx.author.id}.')
        await dms.send('Prosessoidaan GDPR-pyyntöä. Tässä menee hetki.')

        channel = self.bot.get_channel(setting.get_value())
        converter = commands.UserConverter()
        found_messages = []
        async for message in channel.history(limit=None):
            if message.author != self.bot.user:
                continue

            for embed in message.embeds:
                if embed.title is None or embed.title != '[POISTETTU VIESTI]':
                    continue

                for field in embed.fields:
                    if field.name != 'Lähettäjä':
                        continue

                    try:
                        del_user = await converter.convert(ctx, field.value)
                        if del_user == user:
                            found_messages.append(message)
                    except commands.BadArgument:
                        self._logger.debug(
                            'Failed to convert to User',
                            exc_info=True
                        )
                    break
                break

        if len(found_messages) == 0:
            await dms.send('Viestejä ei löytynyt!')
            return

        await dms.send(
            f'Löytyi {len(found_messages)} viesti'
            f'{"ä" if len(found_messages) != 1 else ""}.'
        )
        await dms.send(GDPR_HOW_TO_READ)

        send_json = {
            'guild_id': ctx.guild.id,
            'guild_name': ctx.guild.name,
            'time': datetime.now(timezone.utc).isoformat(),
            'user_id': ctx.author.id,
            'user_name': ctx.author.name,
            'user_discriminator': ctx.author.discriminator
        }

        converters = [
            commands.TextChannelConverter(),
            commands.ThreadConverter(),
            commands.VoiceChannelConverter()
        ]
        bases = [
            'channel',
            'thread',
            'voice_channel'
        ]

        json_messages = []

        all_files = {}

        for message in found_messages:
            msg = {'bot_message_id': message.id}
            for embed in message.embeds:
                if embed.title != '[POISTETTU VIESTI]':
                    continue
                if embed.description is not None:
                    msg['content'] = embed.description
                for field in embed.fields:
                    if field.name == 'Kanava':
                        for conv, base in zip(converters, bases):
                            try:
                                channel = await conv.convert(
                                    ctx, field.value
                                )
                                break
                            except commands.BadArgument:
                                continue
                        else:
                            msg['channel_mention'] = field.value
                            continue
                        msg[f'{base}_id'] = channel.id
                        msg[f'{base}_name'] = channel.name
                    elif field.name == 'Sisältö':
                        msg['content'] = field.value
                    elif field.name == 'Muu sisältö':
                        msg['other_content'] = field.value
                    elif field.name == 'Luotu':
                        msg['created_at'] = field.value
                    elif field.name == 'Muokattu':
                        msg['edited_at'] = field.value
                files = []
                for attachment in message.attachments:
                    if message.id not in all_files:
                        all_files[message.id] = []
                    all_files[message.id].append(attachment)
                    files.append(attachment.filename)
                if len(files) > 0:
                    msg['files'] = files
                json_messages.append(msg)
                break

        send_json['messages'] = json_messages
        with StringIO() as json_file:
            json.dump(send_json, json_file, indent=4, ensure_ascii=False)
            json_file.seek(0)
            await dms.send(file=discord.File(json_file, filename='gdpr.json'))

        zip_files = await processors.gdpr_zip_files(all_files)
        for zip_file in zip_files:
            await dms.send(file=zip_file)
        await dms.send('Valmista!')
        self._logger.info(
            f'GDPR request {ctx.author.id} successful.'
        )


async def setup(bot):
    await bot.add_cog(GDPR(bot))
