import re
import logging
from datetime import timedelta, datetime

import discord
from discord import app_commands
from discord.ext import commands
from discord.utils import format_dt

import db
from util import CustomContext, timeout_member

TIME_REGEX = re.compile(r'^(\d+)([smhdw])$')
TIME_PARAMS = {
    's': 'seconds',
    'm': 'minutes',
    'h': 'hours',
    'd': 'days',
    'w': 'weeks'
}
MAX_TIME = timedelta(days=28)


class MuteTime(commands.Converter):
    async def convert(self, ctx: commands.Context, argument: str):
        argument = argument.lower()
        match = TIME_REGEX.match(argument)
        if match is None:
            raise commands.BadArgument(f'Failed to parse time: \'{argument}\'')
        groups = match.groups()
        mute_time = timedelta(**{
            TIME_PARAMS[groups[1]]: int(groups[0])
        })
        return mute_time


class MuteTimeTrans(app_commands.Transformer):
    async def transform(self, interaction: discord.Interaction, value: str) -> timedelta:
        argument = value.lower()
        match = TIME_REGEX.match(argument)
        if match is None:
            raise ValueError(f'Failed to parse time: \'{argument}\'')
        groups = match.groups()
        mute_time = timedelta(**{
            TIME_PARAMS[groups[1]]: int(groups[0])
        })
        return mute_time

    async def autocomplete(self, interaction: discord.Interaction, value: str, /) -> list[app_commands.Choice[str]]:
        try:
            int_val = int(value)
        except ValueError:
            return [app_commands.Choice(name=f'1{c}', value=f'1{c}') for c in 'smhdw']
        return [app_commands.Choice(name=f'{int_val}{c}', value=f'{int_val}{c}') for c in 'smhdw']


class ModerationCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='timeout', aliases=['mute', 'tempmute'])
    @commands.has_permissions(moderate_members=True)
    @commands.guild_only()
    async def timeout(
            self,
            ctx: CustomContext,
            member: discord.Member,
            timeout_time: MuteTime,
            *,
            reason: str = None
    ):
        if timeout_time > MAX_TIME:
            await ctx.rsend('Muten kesto on enintään 28 päivää!')
            return

        try:
            new_member = await timeout_member(
                member,
                timeout_time,
                reason=reason
            )
        except discord.Forbidden:
            await ctx.rsend(
                'Minulla ei ole oikeuksia laittaa tätä käyttäjää mutelle!'
            )
        else:
            if new_member is not None:
                member = new_member
            embed = discord.Embed(color=discord.Color.red())
            embed.title = f'[MUTE] {member}'
            embed.add_field(name='Käyttäjä', value=member.mention)
            embed.add_field(name='Moderaattori', value=ctx.author.mention)
            embed.add_field(
                name='Mute päättyy',
                value=format_dt(member.timed_out_until, style='R')
            )
            if reason is not None:
                embed.add_field(name='Syy', value=reason)
            await ctx.rsend(embed=embed, mention_author=False)

            async with ctx.db_session.begin():
                setting = await db.GuildSetting.settings_for(
                    ctx.db_session,
                    ctx.guild.id, settings=db.GuildSettingInfo.LOG_CHANNEL
                )
            if setting is None:
                return
            channel = self.bot.get_channel(setting.get_value())

            await channel.send(embed=embed)

    @commands.command(name='unmute')
    @commands.has_permissions(moderate_members=True)
    @commands.guild_only()
    async def unmute(
            self,
            ctx: CustomContext,
            member: discord.Member,
            *,
            reason: str = None
    ):
        if member.timed_out_until is None:
            return
        try:
            await timeout_member(member, None, reason=reason)
        except discord.Forbidden:
            await ctx.rsend('Minulla ei ole oikeuksia poistaa mutea!')
        else:
            embed = discord.Embed(color=discord.Color.green())
            embed.title = f'[UNMUTE] {member}'
            embed.add_field(name='Käyttäjä', value=member.mention)
            embed.add_field(name='Moderaattori', value=ctx.author.mention)
            if reason is not None:
                embed.add_field(name='Syy', value=reason)
            await ctx.rsend(embed=embed, mention_author=False)

            async with ctx.db_session.begin():
                setting = await db.GuildSetting.settings_for(
                    ctx.db_session,
                    ctx.guild.id, settings=db.GuildSettingInfo.LOG_CHANNEL
                )
            if setting is None:
                return
            channel = self.bot.get_channel(setting.get_value())

            await channel.send(embed=embed)

    @app_commands.command(name='ban', description='Bannivasara!')
    @app_commands.default_permissions(ban_members=True)
    @app_commands.rename(member='käyttäjä', reason='bannin_syy', remove_time='viestien_poistoväli')
    @app_commands.describe(
        member='Käyttäjä, joka bannitaan.',
        reason='Syy bannille, näkyy audit logissa ja bannilistassa',
        remove_time='Aikaväli, jolta bannittavan käyttäjän viestit poistetaan (max 7 päivää)'
    )
    @app_commands.guild_only()
    async def slash_ban(
            self,
            interaction: discord.Interaction,
            member: discord.Member,
            remove_time: app_commands.Transform[timedelta, MuteTimeTrans],
            reason: str
    ):
        if remove_time > timedelta(days=7):
            await interaction.response.send_message('Viestit voi poistaa enimmillään viikon ajalta!', ephemeral=True)
            return

        full_reason = f'Aika: {datetime.now().isoformat()}\nModeraattori: {interaction.user}\nSyy:\n{reason}'
        try:
            await member.ban(delete_message_seconds=int(remove_time.total_seconds()), reason=full_reason)
        except discord.Forbidden:
            await interaction.response.send_message('Minulla ei ole bannioikeuksia!', ephemeral=True)
            return

        embed = discord.Embed(colour=discord.Colour.red(), title='[BAN]')
        embed\
            .add_field(name='Käyttäjä', value=f'`{member}`')\
            .add_field(name='Moderaattori', value=f'{interaction.user.mention}')\
            .add_field(name='Syy', value=reason, inline=False)
        await interaction.response.send_message(embed=embed)

    @timeout.error
    async def timeout_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadArgument):
            await ctx.rsend('Käyttäjää ei löydy tai aika on väärässä muodossa!')
        else:
            ctx.error_handled = False

    @unmute.error
    async def unmute_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadArgument):
            await ctx.rsend('Käyttäjää ei löydy!')
        else:
            ctx.error_handled = False


async def setup(bot):
    await bot.add_cog(ModerationCommands(bot))
