import logging
import typing
from pprint import pformat
from io import BytesIO
from datetime import timezone

import discord
import aiohttp
from discord.ext import commands

import processors
from util import DiscordFile, CustomContext
from custom_ui import AnimePaginator, MangaPaginator


class Utility(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

        self._mal_client = None

        tokens = self.bot.config.get('tokens')
        mal_client_id = tokens.get('myanimelist')
        if mal_client_id is not None:
            self._mal_client = processors.MALClient(mal_client_id, self.bot.session)

    @commands.command(name='sää', aliases=['weather'])
    async def weather(
            self,
            ctx: CustomContext,
            map_zoom: typing.Optional[float] = 4,
            *,
            query: str = None
    ):
        """Hakee OpenWeatherMapista tämänhetkisiä säätietoja annetulle paikalle.
        Jos paikkaa ei anneta, oletuksena on Helsinki. Karttakuvan zoomaustasoa
        voi säätää antamalla ennen paikannimeä luvun väliltä [1..20]. Lisäämällä
        queryn loppuun tekstin 'nomap' (ilman lainausmerkkejä) voi skipata
        kartan hakemisen."""

        show_map = True

        if not 1 <= map_zoom <= 20:
            await ctx.rsend('Kartan zoomauksen pitää olla välillä [1..20].')
            show_map = False

        if query is None:
            query = 'Suomi'
        elif query.lower() == 'nomap':
            show_map = False
            query = 'Suomi'
        elif query.lower().endswith('nomap'):
            show_map = False
            query = query[:-5].strip()

        async with ctx.typing():
            tokens = self.bot.config.get('tokens')
            api_key = tokens.get('openweathermap')
            if api_key is None:
                await ctx.rsend('OpenWeatherMapia ei ole konfiguroitu.')
                return
            try:
                weather_data = await processors.owm_get_current_weather(
                    api_key,
                    query,
                    self.bot.session
                )
            except aiohttp.ClientResponseError as err:
                if err.status == 404:
                    await ctx.rsend('Säätietoja ei löytynyt!')
                else:
                    await ctx.rsend('Säätietojen haku epäonnistui!')
                    self._logger.error(
                        f'Could not retrieve weather info! code={err.status} '
                        f'msg={err.message}'
                    )
                return

            if weather_data.status_code != 200:
                self._logger.error(f'Error retrieving OpenWeatherMap data.')
                self._logger.error(pformat(weather_data.orig_json))
                await ctx.rsend('Säätietojen haku epäonnistui!')
                return

            title = weather_data.name
            if weather_data.country_code is not None:
                title += f' ({weather_data.country_code})'

            embed = discord.Embed(
                title=title,
                color=discord.Color.blue(),
                description=weather_data.description
            )

            visibility_str = None
            if weather_data.visibility is not None:
                if weather_data.visibility < 1000:
                    visibility_str = \
                        f'{weather_data.visibility:3d} m'.replace('.', ',')
                else:
                    visibility_str = \
                        f'{weather_data.visibility / 1000:4.1f} ' \
                        f'km'.replace('.', ',')

            embed.set_thumbnail(
                url=weather_data.icon_url
            ).add_field(
                name='Lämpötila',
                value=f'{weather_data.temp:6.2f} °C'.replace('.', ',')
            ).add_field(
                name='Tuntuu kuin',
                value=f'{weather_data.feels_like:6.2f} °C'.replace('.', ',')
            )

            wind_str = ''
            if weather_data.wind_dir_readable is not None:
                wind_str += f'{weather_data.wind_dir_readable}'

            if weather_data.wind_speed is not None:
                if weather_data.wind_dir_readable is not None:
                    wind_str += ' '
                wind_str += \
                    f'{weather_data.wind_speed:.1f} m/s'.replace('.', ',')

            if len(wind_str) > 0:
                embed.add_field(name='Tuuli', value=wind_str)

            if weather_data.humidity is not None:
                embed.add_field(
                    name='Ilmankosteus',
                    value=f'{weather_data.humidity} %'
                )

            if visibility_str is not None:
                embed.add_field(name='Näkyvyys', value=visibility_str)

            if weather_data.rain_1h is not None:
                embed.add_field(
                    name='Sademäärä (1 h)',
                    value=f'{weather_data.rain_1h} mm'.replace('.', ',')
                )

            if weather_data.rain_3h is not None:
                embed.add_field(
                    name='Sademäärä (3 h)',
                    value=f'{weather_data.rain_3h} mm'.replace('.', ',')
                )

            if weather_data.snow_1h is not None:
                embed.add_field(
                    name='Lumisademäärä (1 h)',
                    value=f'{weather_data.snow_1h} mm'.replace('.', ','))

            if weather_data.snow_3h is not None:
                embed.add_field(
                    name='Lumisademäärä (3 h)',
                    value=f'{weather_data.snow_3h} mm'.replace('.', ','))

            embed.add_field(
                name='Auringonnousu (paikallista aikaa)',
                value=weather_data.sunrise_local.strftime('%H.%M')
            ).add_field(
                name='Auringonlasku (paikallista aikaa)',
                value=weather_data.sunset_local.strftime('%H.%M')
            ).set_footer(
                text='Lähde: openweathermap.org - tiedot päivitetty'
            )
            embed.timestamp = \
                weather_data.calc_time_utc.replace(tzinfo=timezone.utc)

            # Try to get map image
            geoapi_token = tokens.get('geoapify')

            image_data = None
            if show_map and None not in \
                    [geoapi_token, weather_data.lat, weather_data.lon]:
                try:
                    image_data = BytesIO(
                        await processors.geoapify_get_static_map(
                            self.bot.session,
                            geoapi_token,
                            weather_data.lat,
                            weather_data.lon,
                            zoom_level=map_zoom
                        )
                    )
                except aiohttp.ClientResponseError:
                    self._logger.debug(
                        'Failed to get map image!',
                        exc_info=True
                    )

            if show_map and image_data is not None:
                embed.set_image(url='attachment://map.jpg')
                await ctx.rsend(
                    file=DiscordFile(image_data, filename='map.jpg'),
                    embed=embed,
                    mention_author=False
                )
            else:
                await ctx.rsend(embed=embed, mention_author=False)

    @commands.command('anime')
    async def search_anime(self, ctx: CustomContext, *, query: str):
        if self._mal_client is None:
            await ctx.rsend('MyAnimeList-hakua ei ole konfiguroitu!')
            return
        if len(query) < 3:
            await ctx.rsend('Hakutermissä pitää olla vähintään kolme merkkiä!')
            return
        elif len(query) > 64:
            await ctx.rsend('Hakutermissä saa olla enintään 64 merkkiä!')
            return

        animes = await self._mal_client.search_anime(query, fields=[
            'synopsis',
            'rank',
            'num_episodes',
            'average_episode_duration',
            'title',
            'main_picture',
            'id',
            'mean',
            'status',
            'media_type',
            'genres',
            'start_date',
            'end_date',
            'studios',
        ])

        if len(animes) == 0:
            await ctx.rsend('Tuloksia ei löytynyt!')
            return

        paginator = AnimePaginator(animes, ctx.author, timeout=180)
        msg = await ctx.rsend(embed=paginator.build_embed(animes[0]), view=paginator, mention_author=False)
        paginator.message = msg

    @commands.command('manga')
    async def search_manga(self, ctx: CustomContext, *, query: str):
        if self._mal_client is None:
            await ctx.rsend('MyAnimeList-hakua ei ole konfiguroitu!')
            return
        if len(query) < 3:
            await ctx.rsend('Hakutermissä pitää olla vähintään kolme merkkiä!')
            return
        elif len(query) > 64:
            await ctx.rsend('Hakutermissä saa olla enintään 64 merkkiä!')
            return

        mangas = await self._mal_client.search_manga(query, fields=[
            'synopsis',
            'rank',
            'num_volumes',
            'num_chapters',
            'title',
            'main_picture',
            'id',
            'mean',
            'status',
            'media_type',
            'genres',
            'start_date',
            'end_date',
            'authors{node{first_name,last_name}}',
            'first_name',
            'last_name',
        ])

        if len(mangas) == 0:
            await ctx.rsend('Tuloksia ei löytynyt!')
            return

        paginator = MangaPaginator(mangas, ctx.author, timeout=180)
        msg = await ctx.rsend(embed=paginator.build_embed(mangas[0]), view=paginator, mention_author=False)
        paginator.message = msg


async def setup(bot):
    await bot.add_cog(Utility(bot))
