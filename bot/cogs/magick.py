"""
Commands that use ImageMagick
"""

import typing
import logging
import re
import enum
import functools
from io import BytesIO

import discord
from discord.ext import commands
from wand.exceptions import MissingDelegateError, CorruptImageError

import processors
from processors import image_utils
from util import DiscordFile, CustomContext


class GifMakiaFlag(enum.Enum):
    FLAG_NOLOOP = 'noloop'
    FLAG_REVERSE = 'reverse'

    @classmethod
    async def convert(cls, ctx, argument):
        for flag in GifMakiaFlag:
            if flag.value == argument.lower():
                return flag
        else:
            raise commands.BadArgument(
                f'Could not convert \'{argument}\' to a gif-makia flag!'
            )


_FRAME_COUNT_PATTERN = re.compile(r'^(\d+)f$')


@functools.total_ordering
class FrameCount:
    def __init__(self, count: int):
        self.count = count

    @classmethod
    async def convert(cls, ctx, argument):
        match = re.match(_FRAME_COUNT_PATTERN, argument)

        if match is None:
            raise commands.BadArgument()

        count = int(match.group(1))
        return cls(count)

    def __eq__(self, other):
        return self.count == other

    def __lt__(self, other):
        return self.count < other

    def __str__(self):
        return f'<FrameCount: count={self.count}>'

    def __repr__(self):
        return f'<FrameCount: count={self.count}>'


async def _process_flags(
        ctx: CustomContext,
        flags: typing.List[typing.Union[GifMakiaFlag, FrameCount]] = None
):
    reverse = False
    no_loop = False
    frame_count = None

    if flags is not None:
        for flag in flags:
            if flag == GifMakiaFlag.FLAG_REVERSE:
                reverse = True
            elif flag == GifMakiaFlag.FLAG_NOLOOP:
                no_loop = True
            elif isinstance(flag, FrameCount):
                if frame_count is not None:
                    await ctx.rsend('Anna vain yksi kuvamäärä!')
                    raise RuntimeError('Too many frame counts specified!')
                if flag < 5:
                    await ctx.rsend(
                        'Antamasi kuvamäärä on liian pieni (min 5)!'
                    )
                    raise RuntimeError('Too few frames!')
                elif flag > 50:
                    await ctx.rsend(
                        'Antamasi kuvamäärä on liian suuri (max 50)!'
                    )
                    raise RuntimeError('Too many frames!')
                frame_count = flag.count
    return reverse, no_loop, frame_count


class Magick(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='makia')
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def makia(
            self,
            ctx: CustomContext,
            strength: typing.Optional[float] = 1,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """Tekee taikoja kuvalle.
        Numeroparametrillä (>= 0) voi säätää taian voimakkuutta."""

        if strength is None:
            strength = 1
        # Negative strength causes segfault in liblqr for some reason
        # Documentation does not say that delta_x can't be negative ¯\_(ツ)_/¯
        if strength < 0:
            await ctx.rsend('Voimakkuus ei saa olla negatiivinen!')
            return
        elif strength > processors.MAGIC_STRENGTH_MAX:
            await ctx.rsend('Taikasi on liian voimakas!')
            return
        async with ctx.typing():

            with BytesIO() as image_bytes:
                try:
                    await image_utils.get_image(
                        ctx, target, image_bytes=image_bytes
                    )
                except RuntimeError:
                    # TODO Maybe create a custom error?
                    return

                # Async magic
                final = await self.bot.loop.run_in_executor(
                    self.bot.executor,
                    functools.partial(
                        processors.makia,
                        image_bytes,
                        strength
                    )
                )
                final.seek(0)

                # Send it
                await ctx.rsend(
                    file=DiscordFile(final, filename='makia.png'),
                    mention_author=False
                )
                self._logger.info('Makia command succeeded.')

    @commands.command(name='gif-makia', aliases=['makia-gif'])
    async def gif_makia(
            self,
            ctx: CustomContext,
            flags: commands.Greedy[
                typing.Union[GifMakiaFlag, FrameCount]
            ] = None,
            strength: typing.Optional[float] = 1,
            sharpen_amount: typing.Optional[float] = 0.4,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """Tekee animoitua taikaa.
        Numeroparametrilla (>= 0) voi säätää taian voimakkuutta.
        Jos komennolle antaa tavallisen kuvan eikä gifiä, toisella numero-
        parametrillä voi säätää terävöitysefektin voimakkuutta.
        Lisäämällä flageja ennen numeroita / kohdetta voi säätää lopputulosta.
        Seuraavat flagit ovat saatavilla:
        noloop - estää gifin looppaamisen
        reverse - gif palaa takaisin alkutilaan toistamalla samat kuvat
        käänteisessä järjestyksessä
        Lisäksi flagilla muotoa <numero>f (esim. 30f) voi määrätä gifin
        kuvien määrän (oletus 15, minimi 5 ja maksimi 50).
        Käyttämällä ^-merkkiä kohteena voi tehdä taian viimeksi kanavalle
        lähetetylle kuvalle."""

        try:
            reverse, no_loop, frame_count = await _process_flags(ctx, flags)
        except RuntimeError:
            return

        if frame_count is None:
            frame_count = 15

        # Negative strength causes segfault in liblqr for some reason
        # Documentation does not say that delta_x can't be negative ¯\_(ツ)_/¯
        if strength < 0:
            await ctx.rsend('Voimakkuus ei saa olla negatiivinen!')
            return
        # Limit the strength to (INT32_MAX - 1) / 2
        elif strength > processors.MAGIC_STRENGTH_MAX:
            await ctx.rsend('Taikasi on liian voimakas!')
            return
        async with ctx.typing():
            with BytesIO() as image_bytes:
                try:
                    await image_utils.get_image(
                        ctx, target, image_bytes=image_bytes
                    )
                except RuntimeError:
                    return

                # Async magic
                final, should_ping = await self.bot.run_exclusive(
                    functools.partial(
                        processors.gif_makia,
                        image_bytes,
                        strength,
                        sharpen_amount,
                        frame_count,
                        reverse,
                        no_loop
                    )
                )
                final.seek(0)

                # Send it
                await ctx.rsend(
                    file=DiscordFile(final, filename='makia.gif'),
                    mention_author=should_ping
                )

                self._logger.info('Gif-makia command succeeded.')

    @commands.command(name='gif-makia2', aliases=['makia-gif2', 'makia2-gif'])
    async def gif_makia2(
            self,
            ctx: CustomContext,
            flags: commands.Greedy[typing.Union[GifMakiaFlag, FrameCount]],
            strength: typing.Optional[float] = 1,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """Tekee vaihtoehtoista animoitua taikaa.
        Numeroparametrilla (>= 0) voi säätää taian voimakkuutta.
        Lisäämällä flageja ennen numeroita / kohdetta voi säätää lopputulosta.
        Seuraavat flagit ovat saatavilla:
        noloop - estää gifin looppaamisen
        reverse - gif palaa takaisin alkutilaan toistamalla samat kuvat käänteisessä järjestyksessä
        Lisäksi flagilla muotoa <numero>f (esim. 30f) voi määrätä gifin
        kuvien määrän.
        Käyttämällä ^-merkkiä kohteena voi tehdä taian viimeksi kanavalle
        lähetetylle kuvalle."""

        try:
            reverse, no_loop, frame_count = await _process_flags(ctx, flags)
        except RuntimeError:
            return

        if frame_count is None:
            frame_count = 15

        # Negative strength causes segfault in liblqr for some reason
        # Documentation does not say that delta_x can't be negative ¯\_(ツ)_/¯
        if strength < 0:
            await ctx.rsend('Voimakkuus ei saa olla negatiivinen!')
            return
        elif strength > processors.MAGIC_STRENGTH_MAX:
            await ctx.rsend('Taikasi on liian voimakas!')
            return
        async with ctx.typing():
            with BytesIO() as image_bytes:
                try:
                    await image_utils.get_image(
                        ctx, target, image_bytes=image_bytes
                    )
                except RuntimeError:
                    return

                # Async magic
                final, should_ping = await self.bot.run_exclusive(
                    functools.partial(
                        processors.gif_makia2,
                        image_bytes,
                        strength,
                        frame_count,
                        reverse,
                        no_loop
                    )
                )
                final.seek(0)

                # Send it
                await ctx.rsend(
                    file=DiscordFile(final, filename='makia.gif'),
                    mention_author=should_ping
                )

                self._logger.info('Gif-makia2 command succeeded.')

    @makia.error
    @gif_makia.error
    @gif_makia2.error
    async def makia_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadUnionArgument):
            await ctx.rsend('Antamaasi käyttäjää ei löydy tai url ei kelpaa!')

        elif isinstance(orig_error, (OSError, MissingDelegateError)):
            await ctx.rsend(
                'Antamasi URL:n takaa ei löydy kuvaa, tai jotain muuta kivaa '
                'tapahtui!'
            )
            if isinstance(orig_error, MissingDelegateError):
                self._logger.warning(
                    'Missing delegate for image format!',
                    exc_info=(
                        type(orig_error),
                        orig_error,
                        orig_error.__traceback__
                    )
                )
        elif isinstance(orig_error, CorruptImageError):
            await ctx.rsend('Antamassasi kuvassa on jotain vikaa!')

        elif isinstance(orig_error, processors.TooBigImage):
            await ctx.rsend('Antamasi kuva on liian iso!')

        else:
            ctx.error_handled = False


async def setup(bot):
    await bot.add_cog(Magick(bot))
