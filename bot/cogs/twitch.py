import logging
import random
from pprint import pformat

import discord
from discord.ext import commands
from discord import utils as dutils
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from twitchAPI.twitch import Twitch
from twitchAPI.eventsub import EventSub
from twitchAPI.helper import first
from twitchAPI.types import EventSubSubscriptionTimeout

import db
from util import CustomContext


TWITCH_PURPLE = discord.Colour.from_str('#9146FF')


class EventSubscription:
    def __init__(self, broadcaster):
        self.broadcaster = broadcaster
        self.topic_id = None

    def __hash__(self):
        return hash(self.broadcaster)

    def __eq__(self, other):
        if isinstance(other, str):
            return self.broadcaster == other
        return self.broadcaster == other.broadcaster

    def __str__(self):
        return self.broadcaster

    def __repr__(self):
        return f'EventSubscription<broadcaster={self.broadcaster}, topic_id={self.topic_id}>'


class TwitchEvents(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')
        bot.twitch = self
        self.twitch = None
        self.event_sub = None

        self._subscribed = set()
        self._should_subscribe = set()

    async def initialize(self):
        twitch_config = self.bot.config.get('twitch', None)
        app_id = None
        app_secret = None
        webhook_url = None

        if twitch_config is not None:
            app_id = twitch_config.get('app_id', None)
            app_secret = twitch_config.get('app_secret', None)
            eventsub_secret = twitch_config.get('eventsub_secret', 'A secret for development')
            webhook_url = twitch_config.get('webhook_url')
            webhook_port = twitch_config.get('port', 8008)

        if twitch_config is None or app_id is None or app_secret is None or webhook_url is None:
            self._logger.info('Twitch not configured or the configuration is incomplete!')
            return

        self.twitch = await Twitch(app_id, app_secret)
        self.event_sub = EventSub(webhook_url, app_id, webhook_port, self.twitch)
        self.event_sub.secret = eventsub_secret

    async def subscribe(self, db_session: AsyncSession):
        if self.twitch is None or self.event_sub is None:
            self._logger.info('Twitch not initialized, not subscribing!')
            return

        self._logger.info('Subscribing to Twitch events...')
        await self.event_sub.unsubscribe_all()
        self.event_sub.start()

        async with db_session.begin():
            result = await db_session.scalars(select(db.TwitchLiveNotification.broadcaster_user_id).distinct())
            broadcasters = result.all()
        for broadcaster in broadcasters:
            self._should_subscribe.add(EventSubscription(broadcaster))
        await self._update_subscriptions()

    async def _add_subscription(self, broadcaster_id: str):
        self._should_subscribe.add(EventSubscription(broadcaster_id))
        await self._update_subscriptions()

    async def _remove_subscription(self, broadcaster_id: str):
        self._should_subscribe.remove(broadcaster_id)
        await self._update_subscriptions()

    async def _update_subscriptions(self):
        not_subscribed = self._should_subscribe - self._subscribed
        try:
            for broadcaster_info in not_subscribed:
                self._logger.debug(f'Subscribing to broadcaster ID {broadcaster_info} being live')
                sub_id = await self.event_sub.listen_stream_online(broadcaster_info.broadcaster, self.on_broadcaster_live)
                broadcaster_info.topic_id = sub_id
                self._subscribed.add(broadcaster_info)
        except EventSubSubscriptionTimeout as e:
            self._logger.exception('Event subscription failed!')
            return

        should_unsub = self._subscribed - self._should_subscribe
        for broadcaster_info in should_unsub:
            if broadcaster_info.topic_id is None:
                self._logger.debug(f'topic_id empty for broadcaster ID {broadcaster_info}')
            else:
                self._logger.debug(f'Unsubscribing from broadcaster ID {broadcaster_info} being live')
                await self.event_sub.unsubscribe_topic(broadcaster_info.topic_id)
            self._subscribed.remove(broadcaster_info)

    async def teardown(self):
        self._logger.info('Tearing down Twitch event subscriptions...')
        await self.event_sub.stop()
        await self.twitch.close()

    async def on_broadcaster_live(self, data: dict):
        # Hack: the twitchAPI library creates its own event loop for running these callbacks, so instead of performing
        # the work here, just run the actual embed-sending function in the bot's event loop.
        self.bot.loop.create_task(self._real_on_broadcaster_live(data))

    async def _real_on_broadcaster_live(self, data: dict):
        self._logger.debug(pformat(data))
        event_data = data.get('event', None)
        if event_data is None:
            self._logger.debug(f'Received stream online event with no event data!\n{data}')
            return

        stream_id = event_data.get('id', None)
        broadcaster_id = event_data.get('broadcaster_user_id')
        if stream_id is None or broadcaster_id is None:
            self._logger.debug(f'Received incomplete stream online event!\n{data}')
            return

        async with self.bot.create_db_session() as db_session:
            async with db_session.begin():
                notifications = await db.TwitchLiveNotification.notifications_for(
                    db_session,
                    broadcaster_id=broadcaster_id
                )

                if notifications is None:
                    self._logger.debug(
                        f'Received stream online event for streamer ID {broadcaster_id}, '
                        f'but no subscribing guilds found in the database! Unsubscribing!'
                    )
                    await self._remove_subscription(broadcaster_id)
                    return

                guild_ids = [notification.guild for notification in notifications]
                channel_settings = await db.GuildSetting.settings_for(
                    db_session,
                    guild_ids,
                    settings=db.GuildSettingInfo.TWITCH_NOTIFICATION_CHANNEL
                )

        async for stream in self.twitch.get_streams(user_id=broadcaster_id):
            if stream.id == stream_id:
                break
        else:
            self._logger.debug(
                f'Received live event for streamer ID {broadcaster_id} and stream ID {stream_id}, '
                f'but the stream was not found!'
            )
            return
        self._logger.debug(pformat(stream.to_dict()))

        user_info = await first(self.twitch.get_users(user_ids=[broadcaster_id]))
        self._logger.debug(pformat(user_info.to_dict()))

        embed = discord.Embed(
            color=TWITCH_PURPLE,
            title=stream.title,
            url=f'https://twitch.tv/{stream.user_login}',
            timestamp=stream.started_at
        ).set_author(
            name=user_info.display_name,
            icon_url=user_info.profile_image_url
        ).set_thumbnail(
            url=user_info.profile_image_url
        ).set_image(
            # I suppose the r parameter is for making sure that discord does not use any caches (copied from mee6)
            url=f'{stream.thumbnail_url.format(width=320, height=180)}?r={random.randint(0, 10000)}'
        ).set_footer(
            text='Striimi alkoi'
        ).add_field(
            name='Katsojia',
            value=f'{stream.viewer_count}',
            inline=True
        ).add_field(
            name='Peli / Kategoria',
            value=stream.game_name,
            inline=True
        )

        for setting in channel_settings:
            channel = self.bot.get_channel(setting.get_value())
            if channel is None:
                continue
            notification = dutils.get(notifications, guild=setting.guild)
            await channel.send(notification.message, embed=embed)

    @commands.command(name='add-twitch-notification')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def add_twitch(self, ctx: CustomContext, broadcaster_login: str, *, message_template: str):
        if self.twitch is None or self.event_sub is None:
            await ctx.rsend('Twitch-tapahtumia ei ole konfiguroitu oikein! Ota yhteys botin ylläpitäjään!')
            return

        async with ctx.typing():
            user_info = await first(self.twitch.get_users(logins=[broadcaster_login]))
            user_info = user_info.to_dict()
            if user_info is None:
                await ctx.rsend('Antamaasi Twitch-striimaajaa ei löydy!')
                return

            async with ctx.db_session.begin():
                db_event = db.TwitchLiveNotification(
                    guild=ctx.guild.id,
                    broadcaster_user_id=user_info['id'],
                    message=message_template
                )
                await ctx.db_session.merge(db_event)
            await ctx.db_session.commit()

            await self._add_subscription(user_info['id'])

            await ctx.rsend(
                f'Striimi-ilmoitukset tilattu Twitch-striimaajalle {user_info["display_name"]}!',
                mention_author=False
            )

    @commands.command(name='remove-twitch-notification')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def del_twitch(self, ctx: CustomContext, broadcaster_login: str):
        if self.twitch is None or self.event_sub is None:
            await ctx.rsend('Twitch-tapahtumia ei ole konfiguroitu oikein! Ota yhteys botin ylläpitäjään!')
            return

        async with ctx.typing():
            user_info = await first(self.twitch.get_users(logins=[broadcaster_login]))
            user_info = user_info.to_dict()
            if user_info is None:
                await ctx.rsend('Antamaasi Twitch-striimaajaa ei löydy!')
                return

            async with ctx.db_session.begin():
                sub_info = await ctx.db_session.get(db.TwitchLiveNotification, (ctx.guild.id, user_info['id']))
                if sub_info is None:
                    await ctx.rsend('Antamasi striimaajan striimi-ilmoituksia ei ole tilattu!')
                    return
                await ctx.db_session.delete(sub_info)

                result = await ctx.db_session.scalars(select(db.TwitchLiveNotification.broadcaster_user_id).distinct())
            await ctx.db_session.commit()

            broadcasters = result.all()
            if user_info['id'] not in broadcasters:
                await self._remove_subscription(user_info['id'])

            await ctx.rsend(
                f'Striimi-ilmoitusten tilaus peruttu Twitch-striimaajalta {user_info["display_name"]}!',
                mention_author=False
            )

    @commands.command(name='twitch-notification-channel')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def twitch_channel(self, ctx: CustomContext, channel: discord.TextChannel = None):
        if channel is not None:
            db_item = db.GuildSetting(
                guild=ctx.guild.id,
                setting_info=db.GuildSettingInfo.TWITCH_NOTIFICATION_CHANNEL,
                value=str(channel.id)
            )
        async with ctx.db_session.begin():
            if channel is None:
                db_item = await db.GuildSetting.settings_for(
                    ctx.db_session,
                    ctx.guild.id,
                    settings=db.GuildSettingInfo.TWITCH_NOTIFICATION_CHANNEL
                )
                if db_item is not None:
                    await ctx.db_session.delete(db_item)
            else:
                await ctx.db_session.merge(db_item)
        await ctx.db_session.commit()

        if channel is None:
            await ctx.rsend('Twitch-ilmoitukset poistettu käytöstä!', mention_author=False)
        else:
            await ctx.rsend(f'Twitch-ilmoitukset lähetetään nyt kanavalle {channel.mention}!', mention_author=False)

    @commands.command(name='twitch-notifications')
    @commands.guild_only()
    async def list_twitch_subscriptions(self, ctx: CustomContext):
        if self.twitch is None or self.event_sub is None:
            await ctx.rsend('Twitch-tapahtumia ei ole konfiguroitu oikein! Ota yhteys botin ylläpitäjään!')
            return

        async with ctx.db_session.begin():
            results = await db.TwitchLiveNotification.notifications_for(ctx.db_session, guild_id=ctx.guild.id)
        embed = discord.Embed(color=TWITCH_PURPLE, title='Striimi-ilmoitukset (Twitch)')

        texts = []

        if results is None:
            embed.description = 'Tällä palvelimella ei ole striimi-ilmoituksia!'
        else:
            user_ids = [result.broadcaster_user_id for result in results]
            async for user_info in self.twitch.get_users(user_ids=user_ids):
                texts.append(f'[{user_info.display_name}](https://twitch.tv/{user_info.login})')
            embed.description = '\n'.join(texts)

        await ctx.rsend(embed=embed, mention_author=False)

    # NOTE: I wanted to implement a search, but reading the number of followers requires User authentication.


async def setup(bot):
    await bot.add_cog(TwitchEvents(bot))
    await bot.twitch.initialize()


async def teardown(bot):
    if bot.twitch is not None:
        await bot.twitch.teardown()
        bot.twitch = None
