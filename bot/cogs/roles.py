import typing
import logging

import discord
import emoji
import discord.utils as dutils
from discord.ext import commands
from sqlalchemy.exc import IntegrityError, NoResultFound

import db
from util import CustomContext
from custom_ui import RoleView


DEFAULT_EMBED_COLOR = discord.Color.green()
DEFAULT_EMBED_TITLE = 'Roolinapit'


class UnicodeEmoji(commands.Converter):
    async def convert(self, ctx: CustomContext, argument: str) -> str:
        if not emoji.is_emoji(argument):
            raise commands.BadArgument(f'\'{argument}\' is not a single emoji!')
        return argument


async def _send_removal_message(
        ctx: CustomContext,
        removed_roles: typing.List[db.ButtonRole]
):
    if len(removed_roles) == 0:
        return
    msg = 'Seuraavia rooleja ei löytynyt, ja ne poistettiin ' \
          'tietokannasta:\n' + '\n'.join([
        f'{role.emoji} (ID: {role.role}): {role.description}'
        for role in removed_roles
    ])
    await ctx.rsend(msg)


def _get_desc(ctx: CustomContext, roles: typing.List[db.ButtonRole]) -> str:
    if len(roles) == 0:
        return 'Ei rooleja!'
    desc = ''
    for role in roles:
        desc += \
            f'{role.emoji}: ' \
            f'{dutils.get(ctx.guild.roles, id=role.role).mention}: ' \
            f'{role.description}\n'
    return desc


async def _get_embed(ctx: CustomContext, roles: typing.Iterable[db.ButtonRole]) -> discord.Embed:
    async with ctx.db_session.begin():
        color = await ctx.db_session.get(
            db.GuildSetting,
            (ctx.guild.id, db.GuildSettingInfo.BUTTON_ROLE_EMBED_COLOR)
        )
        if color is None:
            color = DEFAULT_EMBED_COLOR
        else:
            color = discord.Color.from_str(color.get_value())

        title = await ctx.db_session.get(
            db.GuildSetting,
            (ctx.guild.id, db.GuildSettingInfo.BUTTON_ROLE_EMBED_TITLE)
        )
        if title is None:
            title = DEFAULT_EMBED_TITLE
        else:
            title = title.get_value()

    embed = discord.Embed()
    embed.title = title
    embed.description = _get_desc(ctx, roles)
    embed.colour = color

    return embed


async def _update_role_message(ctx: CustomContext, logger: logging.Logger):
    async with ctx.db_session.begin():
        settings = await db.GuildSetting.settings_for(
            ctx.db_session,
            ctx.guild.id,
            settings=[
                db.GuildSettingInfo.BUTTON_ROLE_MESSAGE,
                db.GuildSettingInfo.BUTTON_ROLE_CHANNEL
            ]
        )

        if len(settings) == 0:
            return

        roles = await db.ButtonRole.roles_for(ctx.db_session, ctx.guild.id)

        view, added_roles, removed_roles = \
            await RoleView.from_roles(ctx, roles)

    await ctx.db_session.commit()

    await _send_removal_message(ctx, removed_roles)

    channel_id = dutils.get(
        settings,
        setting_info=db.GuildSettingInfo.BUTTON_ROLE_CHANNEL
    ).get_value()
    message_id = dutils.get(
        settings,
        setting_info=db.GuildSettingInfo.BUTTON_ROLE_MESSAGE
    ).get_value()

    channel_message_found = True
    channel = ctx.guild.get_channel_or_thread(channel_id)
    if channel is None:
        channel_message_found = False
    if channel_message_found:
        try:
            message = await channel.fetch_message(message_id)
        except discord.HTTPException:
            channel_message_found = False

    if not channel_message_found:
        async with ctx.db_session.begin():
            for setting in settings:
                await ctx.db_session.delete(setting)
        await ctx.db_session.commit()
        await ctx.rsend(
            'Kanavaa tai viestiä ei löydy tai minulla ei ole oikeuksia lukea '
            'niitä, joten viestin tiedot poistettiin tietokannasta.'
        )
        return

    embed = await _get_embed(ctx, added_roles)

    await message.edit(embed=embed, view=view if len(added_roles) > 0 else None)


class RoleCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='add-reaction-role')
    @commands.has_guild_permissions(administrator=True)
    async def add_reaction_role(
            self,
            ctx: CustomContext,
            role: discord.Role,
            # This has to be done like this because d.py PartialEmojiConverter
            # won't support Unicode emojis, see
            # https://github.com/Rapptz/discord.py/pull/8192
            role_emoji: typing.Union[discord.PartialEmoji, UnicodeEmoji],
            *,
            desc: str
    ):
        db_role = db.ButtonRole(
            guild=ctx.guild.id,
            role=role.id,
            emoji=str(role_emoji),
            description=desc
        )
        async with ctx.db_session.begin():
            ctx.db_session.add(db_role)
        await ctx.db_session.commit()
        await _update_role_message(ctx, self._logger)
        await ctx.rsend('Rooli lisätty nappeihin!')

    @commands.command(name='remove-reaction-role')
    @commands.has_guild_permissions(administrator=True)
    async def remove_reaction_role(
            self,
            ctx: CustomContext,
            role: discord.Role
    ):
        async with ctx.db_session.begin():
            db_role = \
                await ctx.db_session.get(db.ButtonRole, (ctx.guild.id, role.id))
            if db_role is None:
                await ctx.rsend('Roolin tietoja ei löydy tietokannasta!')
                return
            await ctx.db_session.delete(db_role)
        await ctx.db_session.commit()
        await _update_role_message(ctx, self._logger)
        await ctx.rsend('Rooli poistettu napeista!')

    @commands.command(name='autoupdate-role-embed')
    @commands.has_guild_permissions(administrator=True)
    async def autoupdate(self, ctx: CustomContext):
        await _update_role_message(ctx, self._logger)

    @commands.command(name='send-role-embed')
    @commands.has_guild_permissions(administrator=True)
    async def send_reaction_role_message(
            self,
            ctx: CustomContext,
            channel: typing.Union[
                discord.TextChannel,
                discord.Thread,
                discord.VoiceChannel
            ] = None
    ):
        if channel is None:
            channel = ctx.channel

        async with ctx.db_session.begin():
            role_count = await db.ButtonRole.count(ctx.db_session, ctx.guild.id)
            if role_count == 0:
                await ctx.rsend('Rooleja ei ole!')
                return

            roles = await db.ButtonRole.roles_for(ctx.db_session, ctx.guild.id)
            view, added_roles, removed_roles = \
                await RoleView.from_roles(ctx, roles)

        await ctx.db_session.commit()

        await _send_removal_message(ctx, removed_roles)

        embed = await _get_embed(ctx, added_roles)

        async with ctx.db_session.begin():
            settings = await db.GuildSetting.settings_for(
                ctx.db_session,
                ctx.guild.id,
                settings=[
                    db.GuildSettingInfo.BUTTON_ROLE_MESSAGE,
                    db.GuildSettingInfo.BUTTON_ROLE_CHANNEL
                ]
            )
            if len(settings) > 0:
                channel_id = dutils.get(
                    settings,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_CHANNEL
                ).get_value()
                message_id = dutils.get(
                    settings,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_MESSAGE
                ).get_value()

                try:
                    old_channel = ctx.guild.get_channel(channel_id)
                    message = await old_channel.fetch_message(message_id)
                except (discord.NotFound, AttributeError):
                    self._logger.debug(
                        'Failed to get message (channel/message deleted?)',
                        exc_info=True
                    )
                else:
                    await message.delete()
                for setting in settings:
                    await ctx.db_session.delete(setting)

            msg = await channel.send(embed=embed, view=view)

            ctx.db_session.add_all([
                db.GuildSetting(
                    guild=ctx.guild.id,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_CHANNEL,
                    value=str(channel.id)
                ),
                db.GuildSetting(
                    guild=ctx.guild.id,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_MESSAGE,
                    value=str(msg.id)
                )
            ])
        await ctx.db_session.commit()

    @commands.command(name='role-embed-color')
    @commands.has_guild_permissions(administrator=True)
    async def embed_color(self, ctx: CustomContext, *, color: discord.Color = None):
        async with ctx.db_session.begin():
            if color is None:
                db_color = await ctx.db_session.get(
                    db.GuildSetting,
                    (ctx.guild.id, db.GuildSettingInfo.BUTTON_ROLE_EMBED_COLOR)
                )
                if db_color is not None:
                    await ctx.db_session.delete(db_color)
            else:
                db_color = db.GuildSetting(
                    guild=ctx.guild.id,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_EMBED_COLOR,
                    value=str(color)
                )
                await ctx.db_session.merge(db_color)

        await ctx.db_session.commit()
        await _update_role_message(ctx, self._logger)

        if color is None:
            await ctx.rsend(f'Väri palautettu oletukseen ({DEFAULT_EMBED_COLOR}).')
        else:
            await ctx.rsend(f'Väri on nyt {color}.')

    @commands.command(name='role-embed-title')
    @commands.has_guild_permissions(administrator=True)
    async def embed_title(self, ctx: CustomContext, *, title: str = None):
        async with ctx.db_session.begin():
            if title is None:
                db_title = await ctx.db_session.get(
                    db.GuildSetting,
                    (ctx.guild.id, db.GuildSettingInfo.BUTTON_ROLE_EMBED_TITLE)
                )
                if db_title is not None:
                    await ctx.db_session.delete(db_title)
            else:
                db_title = db.GuildSetting(
                    guild=ctx.guild.id,
                    setting_info=db.GuildSettingInfo.BUTTON_ROLE_EMBED_TITLE,
                    value=str(title)
                )
                await ctx.db_session.merge(db_title)

        await ctx.db_session.commit()
        await _update_role_message(ctx, self._logger)

        if title is None:
            await ctx.rsend(f'Otsikko palautettu oletukseen ({DEFAULT_EMBED_TITLE}).')
        else:
            await ctx.rsend(f'Otsikko on nyt {title}.')

    @add_reaction_role.error
    @send_reaction_role_message.error
    @remove_reaction_role.error
    async def reaction_role_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, IntegrityError):
            await ctx.rsend('Rooli on jo lisätty!')
        elif isinstance(orig_error, commands.RoleNotFound):
            await ctx.rsend('Roolia ei ole olemassa!')
        elif isinstance(orig_error, NoResultFound):
            await ctx.rsend('Roolin tietoja ei löydy tietokannasta!')
        else:
            ctx.error_handled = False


async def setup(bot):
    await bot.add_cog(RoleCommands(bot))
