"""
Commands and events related to automatic greetings (join, leave)
"""

import logging
from datetime import timedelta

import discord
import discord.utils as dutils
from discord.ext import commands

import db
from util import CustomContext

NO_DELAY = timedelta(minutes=0)

# The embed the bot posts
simpson_embed = discord.Embed()
simpson_embed.set_image(
    url='https://media.giphy.com/media/fDO2Nk0ImzvvW/giphy.gif'
)


class Greetings(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        now = discord.utils.utcnow()
        join = member.joined_at

        async with self.bot.create_db_session() as db_session:
            async with db_session.begin():
                settings = await db.GuildSetting.settings_for(
                    db_session,
                    member.guild.id, settings=[
                        db.GuildSettingInfo.GIF_DELAY,
                        db.GuildSettingInfo.LOG_CHANNEL,
                        db.GuildSettingInfo.LEAVE_GREETING_TEMPLATE
                    ]
                )

        channel_setting = dutils.get(
            settings,
            setting_info=db.GuildSettingInfo.LOG_CHANNEL
        )
        if channel_setting is None:
            return
            # TODO: Maybe send a message to a channel about this?
        channel = self.bot.get_channel(channel_setting.get_value())

        template = dutils.get(
            settings,
            setting_info=db.GuildSettingInfo.LEAVE_GREETING_TEMPLATE
        )
        if template is not None:
            template = template.get_value()
            if len(template) > 0:
                await channel.send(template.format(user=str(member)))

        delay_setting = dutils.get(
            settings,
            setting_info=db.GuildSettingInfo.GIF_DELAY
        )

        if delay_setting is None:
            delay = NO_DELAY
        else:
            delay = timedelta(minutes=delay_setting.get_value())

        if now - join > delay:
            return

        await channel.send(embed=simpson_embed)
        self._logger.info('GIF Embed sent!')

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        async with self.bot.create_db_session() as db_session:
            async with db_session.begin():
                settings = await db.GuildSetting.settings_for(
                    db_session,
                    member.guild.id, settings=[
                        db.GuildSettingInfo.JOIN_GREETING_CHANNEL,
                        db.GuildSettingInfo.JOIN_GREETING_TEMPLATE
                    ]
                )

        channel_setting = dutils.get(
            settings,
            setting_info=db.GuildSettingInfo.JOIN_GREETING_CHANNEL
        )
        if channel_setting is None:
            return
        channel = self.bot.get_channel(channel_setting.get_value())

        template = dutils.get(
            settings,
            setting_info=db.GuildSettingInfo.JOIN_GREETING_TEMPLATE
        )
        if template is None:
            return
        template = template.get_value()
        if len(template) == 0:
            return
        await channel.send(template.format(mention=member.mention))

    @commands.command(name='greeting-channel')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def set_greeting_channel(self, ctx: CustomContext, channel: discord.TextChannel = None):
        """
        Asettaa kanavan, jonne liittymistervehdykset lähetetään.
        """
        if channel is None:
            channel = ctx.channel
            if not isinstance(channel, discord.TextChannel):
                await ctx.rsend('Kanavan on oltava normaali tekstikanava!')
                return
        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.JOIN_GREETING_CHANNEL,
            value=str(channel.id)
        )
        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(f'Liittymistervehdysten kanava on nyt {channel.mention}.', mention_author=False)

    @commands.command(name='join-greeting-template')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def join_template(self, ctx: CustomContext, *, template: str = None):
        """
        Asettaa viestipohjan, jonka perusteella liittymistervehdys tehdään.
        Pohjassa {mention} korvautuu liittyjän maininnalla.
        Jos pohja on tyhjä, tervehdykset poistetaan käytöstä.
        """
        if template is None:
            template = ''
        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.JOIN_GREETING_TEMPLATE,
            value=template
        )
        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        if len(template) == 0:
            await ctx.rsend('Liittymistervehdykset poistettu käytöstä.', mention_author=False)
            return
        await ctx.rsend(f'Liittymistervehdysten viestipohja on nyt seuraava:\n```\n{template}```', mention_author=False)

    @commands.command(name='leave-greeting-template')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def leave_template(self, ctx: CustomContext, *, template: str = None):
        """
        Asettaa viestipohjan, jonka perusteella poistumisilmoitus tehdään.
        Pohjassa {user} korvautuu poistujan nimellä.
        Jos pohja on tyhjä, ilmoitukset poistetaan käytöstä.
        """
        if template is None:
            template = ''
        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.LEAVE_GREETING_TEMPLATE,
            value=template
        )
        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        if len(template) == 0:
            await ctx.rsend('Poistumisilmoitukset poistettu käytöstä.', mention_author=False)
            return
        await ctx.rsend(f'Poistumisilmoitusten viestipohja on nyt seuraava:\n```\n{template}```', mention_author=False)


async def setup(bot):
    await bot.add_cog(Greetings(bot))
