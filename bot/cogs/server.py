# Commands related to server-specific settings.

import logging
import typing

import discord
from discord.ext import commands

import db
from util import CustomContext


TIMEDELTA_MINUTE_LIMIT = 1440000000000  # Must be smaller than that


class ServerCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='log-channel', aliases=['gif-channel'])
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def gif_channel(
            self,
            ctx: CustomContext,
            channel: typing.Union[
                discord.TextChannel,
                discord.Thread,
                discord.VoiceChannel
            ] = None
    ):
        """
        Asettaa botin lokikanavan.
        """
        if channel is None:
            channel = ctx.channel
        if isinstance(channel, (discord.Thread, discord.VoiceChannel)):
            await ctx.rsend('Lokikanavan on oltava normaali tekstikanava!')
            return

        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.LOG_CHANNEL,
            value=str(channel.id)
        )
        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(
            f'Lokikanava on nyt {channel.mention}.',
            mention_author=False
        )

    @commands.command(name='image-link-warn', aliases=['ilw'])
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def toggle_image_link_warnings(
            self,
            ctx: CustomContext,
            value: bool = None
    ):

        if value is None:
            async with ctx.db_session.begin():
                setting = await ctx.db_session.get(
                    db.GuildSetting,
                    (ctx.guild.id, db.GuildSettingInfo.IMAGE_LINK_WARNING_TOGGLE)
                )
            if setting is not None:
                value = not bool(setting.get_value())
            # Default: False, so toggling makes it True
            else:
                value = True

        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.IMAGE_LINK_WARNING_TOGGLE,
            value=str(int(value))
        )

        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(
            f'Kuvalinkkivaroitukset ovat nyt '
            f'{"päällä" if value else "pois päältä"}!',
            mention_author=False
        )

    @commands.command(name='prefix')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()  # TODO: Allow personal prefix setting?
    async def set_prefix(self, ctx: CustomContext, prefix: str = None):
        """
        Vaihtaa botin prefixiä.
        """
        if not prefix:
            await ctx.rsend('Anna prefix.')
            return

        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.COMMAND_PREFIX,
            value=prefix
        )

        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(f'Prefix on nyt {prefix}.', mention_author=False)

    @commands.command(name='gif-time')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def gif_delay(self, ctx: CustomContext, time: int = None):
        # FIXME: Only minutes allowed
        if time is None:
            await ctx.rsend('Anna luku!')
            return
        elif time >= TIMEDELTA_MINUTE_LIMIT:
            await ctx.rsend(
                f'Luku voi olla maksimissaan {TIMEDELTA_MINUTE_LIMIT - 1}.'
            )
            return

        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.GIF_DELAY,
            value=str(time)
        )
        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(
            f'Maksimiaika liittymisen ja poistumisen välillä on nyt {time} '
            f'minuuttia.',
            mention_author=False
        )

    @commands.command(name='delete-log')
    @commands.has_permissions(administrator=True)
    @commands.guild_only()
    async def set_delete_log(
            self,
            ctx: CustomContext,
            channel: typing.Union[
                discord.TextChannel,
                discord.Thread,
                discord.VoiceChannel
            ] = None
    ):
        if not channel:
            channel = ctx.channel
        if isinstance(channel, (discord.Thread, discord.VoiceChannel)):
            await ctx.rsend('Poistolokin on oltava normaali tekstikanava!')
            return

        setting = db.GuildSetting(
            guild=ctx.guild.id,
            setting_info=db.GuildSettingInfo.DELETE_CHANNEL,
            value=str(channel.id)
        )

        async with ctx.db_session.begin():
            await ctx.db_session.merge(setting)
        await ctx.db_session.commit()

        await ctx.rsend(
            f'Poistolokikanava on nyt {channel.mention}.',
            mention_author=False
        )

    @gif_delay.error
    async def gif_delay_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadArgument):
            await ctx.rsend('Anna numero!')
        else:
            ctx.error_handled = False


async def setup(bot):
    await bot.add_cog(ServerCommands(bot))
