# Contains image manipulation commands.

import logging
import typing
import functools
from io import BytesIO

import discord
from discord.ext import commands

import processors
from processors import image_utils
from util import DiscordFile, CustomContext


class Images(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='tank')
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def tank(
            self,
            ctx: CustomContext,
            *,
            tank_target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee käyttäjästä tai mainitusta käyttäjästä tankin.
        """

        async with ctx.typing():
            with BytesIO() as image_bytes:
                try:
                    is_avatar = await image_utils.get_image(
                        ctx,
                        tank_target,
                        return_info=True,
                        image_bytes=image_bytes
                    )
                except RuntimeError:
                    return

                # Async magic
                final = await self.bot.loop.run_in_executor(
                    self.bot.executor,
                    functools.partial(
                        processors.tankify,
                        image_bytes,
                        not is_avatar
                    )
                )
                final.seek(0)

                # Send it
                await ctx.rsend(
                    file=DiscordFile(final, filename='tank.png'),
                    mention_author=False
                )
                self._logger.info('Tank command succeeded.')

    @commands.command(name='meandtheboys', aliases=['matb'])
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def matb(
            self,
            ctx: CustomContext,
            members: commands.Greedy[discord.Member],
            *,
            text=''
    ):
        """
        Tekee Me and the boys -meemin annetuista käyttäjistä annetulla tekstillä.
        """

        async with ctx.typing():
            if len(members) not in [0, 1, 2, 3, 4]:
                await ctx.rsend(
                    'Mainitse 1-4 käyttäjää tai älä mainitse ketään!'
                )
                return

            all_bytes = []
            if len(members) == 0:
                # Let's use the caller
                avatar = ctx.author.display_avatar.with_static_format('png')
                avatar_bytes = BytesIO()
                await avatar.save(avatar_bytes)
                avatar_bytes.seek(0)
                all_bytes.append(avatar_bytes)
            else:
                for i in range(len(members)):
                    avatar = members[i].display_avatar.with_static_format('png')
                    avatar_bytes = BytesIO()
                    await avatar.save(avatar_bytes)
                    avatar_bytes.seek(0)
                    all_bytes.append(avatar_bytes)

            # Same async magic again
            final = await self.bot.loop.run_in_executor(
                self.bot.executor,
                functools.partial(processors.matbify, all_bytes, text)
            )
            final.seek(0)
            await ctx.rsend(
                file=DiscordFile(final, filename='meandtheboys.png'),
                mention_author=False
            )
            self._logger.info('MATB command succeeded.')

    @commands.command(name='kodamoment', aliases=['koda'])
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def koda_moment(
            self,
            ctx: CustomContext,
            *,
            koda: typing.Union[discord.Member, discord.User, str] = None
    ):
        """Tekee koda moment -sertifikaatin, jossa on käyttäjä tai teksti."""

        if isinstance(koda, str) and \
                koda.startswith('"') and koda.endswith('"'):
            await ctx.rsend('Parametri ei tarvitse lainausmerkkejä.')

        async with ctx.typing():
            if koda is None:
                await ctx.rsend('Anna käyttäjä tai teksti!')
                return

            if isinstance(koda, (discord.Member, discord.User)):
                name = koda.display_name
            else:
                name = koda

            # Same async magic again
            final = await self.bot.loop.run_in_executor(
                self.bot.executor,
                functools.partial(
                    processors.put_certified_name_and_server,
                    name,
                    ctx.guild.name if ctx.guild is not None
                    else ctx.author.display_name
                )
            )
            final.seek(0)
            await ctx.rsend(
                file=DiscordFile(final, filename='kodamoment.png'),
                mention_author=False
            )
            self._logger.info('Koda moment command succeeded.')

    @commands.command(name='moment')
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def moment(
            self,
            ctx: CustomContext,
            template: typing.Union[discord.Member, discord.User, str] = None,
            *,
            certified: typing.Union[discord.Member, discord.User, str] = ''
    ):
        """Tekee x moment -sertifikaatin, jossa on "certified" sertifioituna"""

        if isinstance(certified, str) and \
                certified.startswith('"') and certified.endswith('"'):
            await ctx.rsend('Toinen parametri ei tarvitse lainausmerkkejä.')

        async with ctx.typing():
            if template is None:
                await ctx.rsend(
                    'Anna nyt edes tieto mitä moment-sertifikaattia tehdään.'
                )
                return

            if isinstance(template, (discord.Member, discord.User)):
                template_name = template.display_name
            else:
                template_name = template

            if isinstance(certified, (discord.Member, discord.User)):
                cert_name = certified.display_name
            else:
                cert_name = certified

            # Same async magic again
            final = await self.bot.loop.run_in_executor(
                self.bot.executor,
                functools.partial(
                    processors.create_full_certificate,
                    template_name,
                    cert_name,
                    ctx.guild.name if ctx.guild is not None
                    else ctx.author.display_name
                )
            )
            final.seek(0)
            await ctx.rsend(
                file=DiscordFile(final, filename='moment.png'),
                mention_author=False
            )
            self._logger.info('Certified moment command succeeded.')

    @commands.command(name='höpsismi', aliases=['höps', 'hopsismi'])
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def hopsismi(self, ctx: CustomContext, *, text: str):
        async with ctx.typing():
            try:
                final = await self.bot.loop.run_in_executor(
                    self.bot.executor,
                    functools.partial(processors.hopsify, text)
                )
            except (OSError, IOError):
                self._logger.debug(
                    'Error while creating Höpsismi image.',
                    exc_info=True
                )
                await ctx.rsend(
                    'Tekstisi on liian pitkä tai siinä on jotain muuta vikaa!'
                )
            else:
                final.seek(0)
                await ctx.rsend(
                    file=DiscordFile(final, filename='höpsismi.png'),
                    mention_author=False
                )
                self._logger.info('Höpsismi command succeeded.')

    @commands.command(name='turri', hidden=True, aliases=['furry'])
    @commands.max_concurrency(3, commands.BucketType.default, wait=True)
    async def furry(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[discord.Member, discord.User] = None
    ):
        if target is None:
            target = ctx.author

        async with ctx.typing():
            with BytesIO() as image_bytes:
                avatar = target.display_avatar.with_static_format('png')
                await avatar.save(image_bytes)
                image_bytes.seek(0)
                final = await self.bot.loop.run_in_executor(
                    self.bot.executor,
                    functools.partial(processors.furrify, image_bytes)
                )
                final.seek(0)

                await ctx.rsend(
                    file=DiscordFile(final, filename='furry.png'),
                    mention_author=False
                )
                self._logger.info('Someone got furrified!')

    @tank.error
    @furry.error
    async def tank_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        await image_utils.handle_image_error(ctx, error)


async def setup(bot):
    await bot.add_cog(Images(bot))
