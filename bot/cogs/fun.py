# Fun related commands.
import functools
import typing
import random
import logging

import discord
from aiohttp import ClientResponseError
from discord import ui
from discord.ext import commands

import processors
from util import CustomContext, DiscordFile
from custom_ui import HangmanView, TeletextPager, HSComicPager, TicTacToeView

coin_sides = ['kruuna', 'klaava']

SHIP_VIDEOS = {
    0:   'https://www.youtube.com/watch?v=SLEdsI731J4',
    10:  'https://www.youtube.com/watch?v=2TOX3WeZJKw',
    20:  'https://www.youtube.com/watch?v=oOM5c5wpsMo',
    30:  'https://www.youtube.com/watch?v=DRKotRezfC0&t=7',
    40:  'https://www.youtube.com/watch?v=LCcZqcPOlNM',
    49:  'https://www.youtube.com/watch?v=qao8FeW5K8g',
    50:  'https://www.youtube.com/watch?v=NKteK8gaOWc',
    60:  'https://www.youtube.com/watch?v=R1zHwbohxDI&t=14',
    68:  'https://www.youtube.com/watch?v=9RLq-8wsfzc',
    69:  'https://www.youtube.com/watch?v=UBX8MWYel3s',
    80:  'https://www.youtube.com/watch?v=vjUqUVrXclE',
    90:  'https://www.youtube.com/watch?v=MTLtFDkyryI',
    98:  'https://www.youtube.com/watch?v=syy9J-yQZJQ&t=27',
    99:  'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
    100: 'https://www.youtube.com/watch?v=1Bix44C1EzY'
}

SHIP_TEXTS = {
    0:   'rip',
    10:  'Iha kaka',
    20:  'Tosi huono',
    30:  'Aika huono',
    40:  'meh',
    49:  'Keskimääräistä huonompi',
    50:  'Tasan keskiarvo!',
    60:  'Keskimääräistä parempi',
    68:  'Iha ok',
    69:  '<:spicy:682570268924837906> höhö seski luku',
    80:  'Aika hyvä',
    90:  'Tosi hyvä',
    99:  'Lähes täydellinen',
    100: 'TÄYDELLISTÄ'
}

EIGHT_BALL_MESSAGES = [
    # Affirmative
    'Varmasti.',
    'Näin on päätetty.',
    'Ei epäilystäkään.',
    'Kyllä, ehdottomasti.',
    'Voit luottaa siihen.',
    'Nähdäkseni kyllä.',
    'Mitä todennäköisimmin.',
    'Hyvältä näyttää.',
    'Kyllä.',
    'Kaikkien merkkien mukaan kyllä.',
    # Non-committal
    'Vastaus on hämärän peitossa, yritä uudelleen.',
    'Yritä myöhemmin uudelleen.',
    'Parempi etten kerro.',
    'Ei pysty just ny.',
    'Keskity ja yritä uudelleen.',
    # Negative
    'En laskisi sen varaan.',
    'Vastaukseni on ei.',
    'Lähteideni mukaan ei.',
    'Eipä hyvältä näytä.',
    'Epäilen.',
]

HM_MESSAGE = \
    '''```{title}
{hangman}
{word}
Väärät: {wrong}```'''

HM_TITLE_ONGOING = 'HIRSIPUU - peli käynnissä'
HM_TITLE_GAME_OVER_TIMEOUT = '''HIRSIPUU - peli ohi - aika loppui
Sana oli {word}'''
HM_TITLE_GAME_OVER_RIP = '''HIRSIPUU - peli ohi - liikaa vääriä arvauksia
Sana oli {word}'''
HM_TITLE_GAME_WON = '''HIRSIPUU - voitit pelin!'''

OFFSET = 127365  # = ord("🇦:regional_indicator_A") - ord("a")

HM_MAPPING = {
    '🅰️': 'a',
    '🅱️': 'b',
    '🅾️': 'o',
    'Ⓜ️': 'm',
    'ℹ️': 'i',
    '🅿️': 'p'
}

REGIONAL_EMOJI_LIMITS = (127462, 127487)


DAILY_DOSE_NUTRIENTS = {
    'VITA':     'A-vitamiinia',
    'VITD':     'D-vitamiinia',
    'VITE':     'E-vitamiinia',
    'THIA':     'tiamiinia',
    'RIBF':     'riboflaviinia',
    'NIAEQ':    'niasiinia',
    'VITPYRID': 'B6-vitamiinia',
    'FOL':      'folaattia',
    'VITB12':   'B12-vitamiinia',
    'VITC':     'C-vitamiinia',
    'CA':       'kalsiumia',
    'P':        'fosforia',
    'K':        'kaliumia',
    'MG':       'magnesiumia',
    'FE':       'rautaa',
    'ZN':       'sinkkiä',
    'CU':       'kuparia',
    'ID':       'jodia',
    'SE':       'seleeniä',
    'VITK':     'K-vitamiinia',
    'MN':       'mangaania',
    'CR':       'kromia',
    'MO':       'molybdeeniä',
    'FAT':      'rasvaa',
    'CHOAVL':   'hiilihydraatteja',
    'SUGAR':    'sokereita',
    'PROT':     'proteiinia',
    'NACL':     'suolaa',
}


_POSITIVE_REPLIES = [
    'a', 'al', 'all', 'k', 'ka', 'kaik', 'kaikki', 'j', 'jo', 'ju', 'joo',
    'juu', 'kyl', 'kyllä', 'y', 'ye', 'yes', 'yep', 'yus', 'yeah', 'tru',
    'true', 'tosi'
]


class AllAllowed(commands.Converter):
    async def convert(self, ctx: commands.Context, argument: str):
        if argument.lower() in _POSITIVE_REPLIES:
            return True
        raise commands.BadArgument(f'Unknown AllAllowed value: {argument}')


def shipping_algorithm(words):
    all_words = ''.join(words).lower()
    ascii_sum = sum(map(ord, all_words))

    return (ascii_sum + 32) % 101


class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')
        with open('files/hangman.txt') as hangmen:
            processors.Hangman.set_ascii_art(hangmen.read().split(','))

        self._yle_client = None
        tokens = self.bot.config.get('tokens')
        app_id = tokens.get('yle_app_id')
        app_key = tokens.get('yle_app_key')
        if app_id is not None and app_key is not None:
            self._yle_client = processors.YleClient(self.bot.session, app_id, app_key)

    @commands.command(name='coinflip', aliases=['kolikko'])
    async def coinflip(self, ctx: CustomContext, coin_side: str = None):
        """Heittää kolikkoa."""
        if coin_side is None or coin_side.lower() not in coin_sides:
            await ctx.rsend('Sinun täytyy valita joko kruuna tai klaava.')
            return

        random_coinside = random.choice(coin_sides)

        if coin_side == random_coinside:
            await ctx.rsend(f'{random_coinside} tuli! Voitit pelin!')
        else:
            await ctx.rsend(f'{random_coinside} tuli! Hävisit pelin.')

    @commands.command(
        name='compatibility',
        aliases=['ys', 'yhteensopivuus', 'comp']
    )
    async def ship(
            self,
            ctx: CustomContext,
            list_of_params: commands.Greedy[
                typing.Union[
                    discord.Member,
                    discord.User,
                    str
                ]
            ]
    ):
        """Tarkistaa, miten hyvin asiat sopivat yhteen."""
        word_list = []
        for param in list_of_params:
            if isinstance(param, discord.Member) or \
                    isinstance(param, discord.User):
                word_list.append(param.display_name)
            else:
                word_list.append(param)
        percentage = shipping_algorithm(word_list)

        msg = f'**YHTEENSOPIVUUS:**\n' \
              f'`{" ".join(word_list)}`\n'

        red = int((100 - percentage) / 100 * 255)
        green = int(percentage / 100 * 255)
        blue = 0
        embed = discord.Embed(color=discord.Color.from_rgb(red, green, blue))

        video = ''
        comment = ''
        for key in SHIP_VIDEOS:
            if percentage <= key:
                video = SHIP_VIDEOS[key]
                break

        for key in SHIP_TEXTS:
            if percentage <= key:
                comment = SHIP_TEXTS[key]
                break

        embed.description = \
            f'[`{processors.get_progress_bar(percentage, 20)}`]({video}) ' \
            f'{percentage} % {comment}'

        await ctx.rsend(msg, embed=embed, mention_author=False)

    @commands.command(name='hirsipuu', aliases=['hangman'])
    async def hangman(
            self,
            ctx: CustomContext,
            min_or_exact_len: typing.Optional[int] = None,
            max_len: typing.Optional[int] = None,
            allow_all: typing.Optional[AllAllowed] = False
    ):
        """Pelaa hirsipuu-peliä.

        Sanan kirjainmäärää voi säätää parametreillä. Yksi numero tuottaa tasan
        tämän numeron mittaisen sanan, ja kaksi numeroa sanan, jonka kirjaimäärä
        on näiden kahden numeron väliltä, numerot mukaanlukien.

        Oletuksena ainoastaan komennon käyttäjä pystyy arvaamaan kirjaimia.
        Jos komennolle antaa jotenkin "tosiarvoisen" (esim. k) kolmannen
        parametrin, kaikki käyttäjät voivat arvata kirjaimia.

        Kirjaimien arvaaminen tapahtuu napeilla.
        """

        with open('files/fi.txt', 'r') as word_file:
            words = word_file.readlines()

        # First line tells which lengths the file contains
        lengths = [int(thing) for thing in words[0].split()]
        words = words[1:]
        the_word = random.choice(words).strip()
        if min_or_exact_len is not None:
            if max_len is None:
                if min_or_exact_len not in lengths:
                    await ctx.rsend(
                        f'{min_or_exact_len} merkin mittaisia sanoja ei ole '
                        f'saatavilla!'
                    )
                    return

                count = int(the_word.split()[0])
                while count != min_or_exact_len:
                    the_word = random.choice(words).strip()
                    count = int(the_word.split()[0])
            else:
                if min_or_exact_len > max_len:
                    await ctx.rsend(
                        f'Minimimerkkimäärän pitää olla vähintään yhtä suuri '
                        f'kuin maksimin, nyt minimi on {min_or_exact_len} ja '
                        f'maksimi {max_len}.'
                    )
                    return

                elif not any([
                    min_or_exact_len <= x <= max_len for x in lengths
                ]):
                    await ctx.rsend(
                        'Antamaltasi alueelta ei löydy yhtään sanaa!'
                    )
                    return

                # Each line contains word length and then the word itself
                count = int(the_word.split()[0])
                while not min_or_exact_len <= count <= max_len:
                    the_word = random.choice(words).strip()
                    count = int(the_word.split()[0])

        the_word = ' '.join(the_word.split()[1:])
        self._logger.debug(f'Hangman: word is {the_word}.')

        game = processors.Hangman(
            the_word,
            processors.HM_ALLOWED_LETTERS
        )

        view = HangmanView(
            game,
            None if allow_all else ctx.author,
            timeout=float(self.bot.bot_config.get('hangman_timeout', 180))
        )
        msg = await ctx.rsend(
            game.get_message_content(),
            mention_author=False,
            view=view
        )
        view.message = msg

    @commands.command(name='ristinolla', aliases=['tictactoe', 'tic-tac-toe'])
    async def tictactoe(self, ctx: CustomContext):
        view = TicTacToeView(ctx.author)
        msg = await ctx.rsend('Odotetaan vastustajaa...', mention_author=False, view=view)
        view.message = msg

    @commands.command(name='8pallo', aliases=['8ball', 'kasipallo', '8baalssi'])
    async def eight_ball(self, ctx: CustomContext, *, question: str = None):
        reply = random.choice(EIGHT_BALL_MESSAGES)
        await ctx.rsend(reply)

    @commands.command(name='xkcd')
    async def xkcd(self, ctx: CustomContext, number_or_random: typing.Union[int, str] = None):
        if isinstance(number_or_random, str) and number_or_random.lower() not in ('r', 'random', 'satunnainen'):
            await ctx.rsend(
                'Tuntematon parametrin arvo, anna `random`, `r` tai `satunnainen`, jos haluat satunnaisen sarjakuvan!'
            )
            return
        async with ctx.typing():
            async with self.bot.session.get('https://xkcd.com/info.0.json') as resp:
                comic_data = await resp.json()
            latest_comic = comic_data['num']
            if number_or_random is None:
                number = latest_comic
            elif isinstance(number_or_random, str):
                number = random.randint(1, latest_comic)
            else:
                number = number_or_random
        if number <= 0 or number > latest_comic:
            await ctx.rsend(f'Sarjakuvaa numerolla {number} ei ole olemassa!')
            return
        await ctx.rsend(f'https://xkcd.com/{number}', mention_author=False)

    async def _hs_comic(self, ctx: CustomContext, comic_id: processors.ComicID):
        async with ctx.typing():
            comics = await processors.get_comic_data(self.bot.session, comic_id)
            view = HSComicPager(
                comics,
                ctx.author,
                functools.partial(
                    processors.get_comic_data,
                    session=self.bot.session,
                    comic_id=comic_id
                )
            )
            msg = await ctx.rsend(mention_author=False, view=view, embed=view.build_embed(view.data[0]))
            view.message = msg

    @commands.command(name='fingerpori')
    async def fingerpori(self, ctx: CustomContext):
        await self._hs_comic(ctx, processors.ComicID.FINGERPORI)

    @commands.command(name='viivi-ja-wagner', aliases=['vw', 'viivi', 'wagner'])
    async def viivi(self, ctx: CustomContext):
        await self._hs_comic(ctx, processors.ComicID.VIIVI_JA_WAGNER)

    @commands.command(name='fok_it', aliases=['fok-it', 'fokit'])
    async def fok_it(self, ctx: CustomContext):
        await self._hs_comic(ctx, processors.ComicID.FOK_IT)

    @commands.command(name='jarla')
    async def jarla(self, ctx: CustomContext):
        await self._hs_comic(ctx, processors.ComicID.JARLA)

    @commands.command(name='wumo')
    async def wumo(self, ctx: CustomContext):
        await self._hs_comic(ctx, processors.ComicID.WUMO)

    @commands.command(name='päivänannos', aliases=['pa'])
    async def daily_dose(self, ctx: CustomContext):
        async with ctx.typing():
            foodid, nutrient, mass = \
                await processors.get_random_food_daily_nutrient()
            nutrient2, days = await processors.get_random_nutrient_dose(
                foodid, nutrient, mass
            )
            foodname = await processors.get_food_name(foodid)

            if mass > 1000:
                mass_str = f'{mass / 1000:.1f} kg'.replace('.', ',')
            else:
                mass_str = f'{mass:.0f} g'

            if days < 10:
                days_str = f'{days:.2f}'.replace('.', ',')
            else:
                days_str = f'{days:.1f}'.replace('.', ',')

            embed = discord.Embed(color=discord.Color.blue())
            embed.title = 'Päivän annos'
            embed.description = \
                f'Saat päivän annoksen {DAILY_DOSE_NUTRIENTS[nutrient]} ' \
                f'syömällä {mass_str} {foodname.lower()}. Samalla saat ' \
                f'{days_str} päivän annoksen ' \
                f'{DAILY_DOSE_NUTRIENTS[nutrient2]}.'
            embed.set_footer(
                text='Lähde: Terveyden ja hyvinvoinnin laitos, Fineli'
            )

            await ctx.rsend(
                embed=embed,
                mention_author=False
            )

    @commands.command(name='teksti-tv', aliases=['tekstitv', 'ttv'])
    async def teksti_tv(self, ctx: CustomContext, page: int = None):
        if self._yle_client is None:
            await ctx.rsend('Teksti-TV:tä ei ole konfiguroitu!')
            return
        if page is None:
            page = 100
        elif page < 100 or page > 999:
            await ctx.rsend('Sivun pitää olla välillä 100-999.')
            return
        try:
            page_info = await self._yle_client.page_info(page)
        except ClientResponseError as e:
            if e.status == 401:
                await ctx.rsend('Teksti-TV:n sivuja pyydetty liian nopeasti, yritä uudelleen hetken kuluttua.')
                return
            elif e.status == 403:
                await ctx.rsend('Teksti-TV:n konfiguraatiossa on jotain pielessä!')
                self._logger.error('YLE teletext configuration incorrect! Setting to None.')
                self._yle_client = None
                return
            elif e.status == 404:
                await ctx.rsend(f'Teksti-TV:n sivua {page} ei ole olemassa!')
                return
            raise e
        try:
            image_data = await self._yle_client.page_image(page, 1)
        except ClientResponseError as e:
            if e.status == 401:
                await ctx.rsend('Teksti-TV:n sivuja pyydetty liian nopeasti, yritä uudelleen hetken kuluttua.')
                return
            elif e.status == 403:
                await ctx.rsend('Teksti-TV:n konfiguraatiossa on jotain pielessä!')
                self._logger.error('YLE teletext configuration incorrect! Setting to None.')
                self._yle_client = None
                return
            elif e.status == 404:
                await ctx.rsend(f'Teksti-TV:n sivua {page} ei ole olemassa!')
                return
            raise e
        image_data.seek(0)
        pager = TeletextPager(ctx.author, self._yle_client, page_info)
        msg = await ctx.rsend(
            view=pager,
            embed=pager.build_embed(),
            file=DiscordFile(image_data, filename='teksti-tv.png'),
            mention_author=False
        )
        pager.message = msg

    @commands.command(name='say', hidden=True)
    @commands.is_owner()
    @commands.guild_only()
    async def say(
            self,
            ctx: CustomContext,
            channel: typing.Union[
                discord.TextChannel,
                discord.Thread,
                discord.VoiceChannel
            ],
            *,
            text: str
    ):
        await channel.send(text)

    @hangman.error
    async def hangman_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        ctx.error_handled = True
        orig_error = getattr(error, 'original', error)

        if isinstance(orig_error, commands.BadArgument):
            await ctx.rsend('Parametrien arvot eivät kelpaa!')
        else:
            ctx.error_handled = False


async def setup(bot):
    await bot.add_cog(Fun(bot))
