import enum
import logging
from io import BytesIO
from urllib.parse import urlparse
from datetime import timedelta

import discord
from discord.ext import commands

import db
# Maximum delay between join and leave
from util import CustomContext

_IMAGE_LINK_MESSAGE = \
    """Discord on alkanut piilottaa kuvalinkkejä, jos viestissä on vain \
linkki. Tämä toimii myös linkeille, jotka käyttävät tietojenkalastelusta \
tuttuja taktiikoita: käytetään domainia, joka on hyvin lähellä virallista \
domainia, ja toivotaan, että käyttäjä klikkaa linkkiä. Tähän saakka nämä \
linkit ovat onneksi olleet rickrolleja."""
_IMAGE_SUFFIXES = ['.jpg', '.png']
_IMAGE_EMBED_TYPES = ['image', 'gifv']


# Message activity types, because apparently discord.py does not include these
class MessageActivityType(enum.Enum):
    JOIN = 1
    SPECTATE = 2
    LISTEN = 3
    JOIN_REQUEST = 5


async def _send_image_link_embed(message: discord.Message):
    info = discord.Embed(color=discord.Color.red())
    info.title = 'EI-DISCORD-KUVALINKKI HAVAITTU!'
    info.description = _IMAGE_LINK_MESSAGE
    info.add_field(
        name='Alkuperäinen URL',
        value=message.content,
        inline=False
    )
    await message.channel.send(embed=info)


class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    async def _process_maybe_image_link(self, message: discord.Message):
        if message.guild is None:
            return

        content = message.content
        # #55: check (very pythonically) for any whitespace character, not just
        # space and ignore if the message really contains whitespace
        if any([ch.isspace() for ch in content]):
            return

        try:
            parsed = urlparse(content)
        except ValueError:
            self._logger.debug(
                f'Failed to parse as URL: {content}',
                exc_info=True
            )
            return

        if parsed.netloc in self.bot.bot_config.get('safe_image_domains', []):
            return

        if len(message.embeds) == 1:
            self._logger.debug(f'Embed type: {message.embeds[0].type}')
            if message.embeds[0].type not in _IMAGE_EMBED_TYPES:
                return

            # Reduce DB operations by putting the DB call here
            async with self.bot.create_db_session() as db_session:
                async with db_session.begin():
                    setting = await db.GuildSetting.settings_for(
                        db_session,
                        message.guild.id,
                        settings=db.GuildSettingInfo.IMAGE_LINK_WARNING_TOGGLE
                    )
            if setting is None or not setting.get_value():
                return

            await _send_image_link_embed(message)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author == self.bot.user or message.author.bot:
            return

        await self._process_maybe_image_link(message)

        await self.bot.process_commands(message)

    @commands.Cog.listener()
    async def on_message_edit(
            self,
            prev_message: discord.Message,
            new_message: discord.Message
    ):
        if new_message.author == self.bot.user or new_message.author.bot:
            return

        # Let's be generous and assume it can take up to 5 seconds for an embed
        # to appear
        if discord.utils.utcnow() - new_message.created_at < \
                timedelta(seconds=5):
            await self._process_maybe_image_link(new_message)

    @commands.Cog.listener()
    async def on_message_delete(self, message: discord.Message):
        # Do not detect bot messages
        if message.author.bot:
            return

        # Do not detect DMs
        if message.guild is None:
            return

        # Do not detect Halloween bot commands
        if message.content.lower().strip() == 'h!trick' or \
                message.content.lower().strip() == 'h!treat':
            return

        async with self.bot.create_db_session() as db_session:
            async with db_session.begin():
                setting = await db.GuildSetting.settings_for(
                    db_session,
                    message.guild.id,
                    settings=db.GuildSettingInfo.DELETE_CHANNEL
                )

        if setting is None:
            return

        embed = discord.Embed(
            title='[POISTETTU VIESTI]',
            color=discord.Color.red()
        )
        embed.add_field(name='Lähettäjä', value=message.author.mention)
        embed.add_field(name='Kanava', value=message.channel.mention)
        embed.description = message.content
        cont = ''
        files = []
        if len(message.attachments) > 0:
            for attachment in message.attachments:
                with BytesIO() as data:
                    try:
                        await attachment.save(data, use_cached=True)
                        data.seek(0)
                        files.append(
                            discord.File(data, filename=attachment.filename)
                        )
                    except (discord.HTTPException, discord.NotFound):
                        self._logger.debug(
                            'Failed to download attachment!',
                            exc_info=True
                        )

            cont += f'{len(message.attachments)} liite' \
                    f'{"ttä" if len(message.attachments) > 1 else ""} ' \
                    f'({len(files)} tallennettu)'

        if len(message.embeds) > 0:
            if len(cont) > 0:
                cont += '\n'
            cont += f'{len(message.embeds)} embed' \
                    f'{"iä" if len(message.embeds) > 1 else ""}'

        if message.activity is not None:
            if len(cont) > 0:
                cont += '\n'
            try:
                act_type = MessageActivityType(message.activity['type']).name
            except ValueError:
                act_type = message.activity['type']
            cont += f'Aktiviteetti (type = `{act_type}`)'

        if message.application is not None:
            if len(cont) > 0:
                cont += '\n'
            cont += 'Rich presence -embed'
            self._logger.debug(f'Rich Presence embed: {message.application}')

        if len(cont) > 0:
            embed.add_field(name='Muu sisältö', value=cont, inline=False)

        embed.add_field(name='Luotu', value=message.created_at)
        if message.edited_at is not None:
            embed.add_field(name='Muokattu', value=message.edited_at)

        channel = self.bot.get_channel(setting.get_value())
        await channel.send(embed=embed, files=files)

    @commands.Cog.listener()
    async def on_command_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        """
        Based on
        https://gist.github.com/EvieePy/7822af90858ef65012ea500bcecf1612
        """
        # If the command already has a handler that handled the error, this
        # function is unnecessary
        if ctx.error_handled:
            return

        # Allows us to check for original exceptions raised and sent to
        # CommandInvokeError. If nothing is found, we keep the exception
        # passed to on_command_error.
        orig_error = getattr(error, 'original', error)

        # Ignore command not found errors
        if isinstance(orig_error, commands.CommandNotFound):
            return

        if isinstance(orig_error, commands.DisabledCommand):
            await ctx.rsend(f'`{ctx.command}`-komento on poissa käytöstä.')

        elif isinstance(orig_error, commands.NoPrivateMessage):
            await ctx.author.send(
                f'`{ctx.command}`-komentoa ei voi käyttää täällä.'
            )

        elif isinstance(orig_error, commands.BadColourArgument):
            await ctx.rsend('Väri on annettu väärässä muodossa!')

        elif isinstance(orig_error, commands.BadUnionArgument):
            await ctx.rsend('Parametrien arvot eivät kelpaa!')
            self._logger.exception(
                f'Error in command {ctx.command}.',
                exc_info=(
                    type(orig_error),
                    orig_error,
                    orig_error.__traceback__
                )
            )
            if len(orig_error.errors) > 0:
                self._logger.error(
                    'This exception was caused by the following exceptions:'
                )
                for index, error in enumerate(orig_error.errors):
                    self._logger.exception(
                        f'Exception {index}:',
                        exc_info=(
                            type(error),
                            error,
                            error.__traceback__
                        )
                    )

        elif isinstance(orig_error, commands.BadArgument):
            await ctx.rsend('Parametrien arvot eivät kelpaa!')
            self._logger.exception(
                f'Error in command {ctx.command}.',
                exc_info=(
                    type(orig_error),
                    orig_error,
                    orig_error.__traceback__
                )
            )

        elif isinstance(orig_error, commands.MissingRequiredArgument):
            await ctx.rsend('Komennosta puuttuu vaadittuja parametrejä!')

        elif isinstance(orig_error, commands.MissingPermissions):
            say = f'Sinulla ei ole tarpeeksi oikeuksia! `{ctx.command}`-' \
                  f'komento vaatii '
            if ctx.command.qualified_name in [
                    'gif-channel',
                    'prefix',
                    'gif-time',
                    'delete-log',
            ]:
                say += 'admin-oikeudet!'
            elif ctx.command.qualified_name in ['mute', 'timeout']:
                say += 'moderate members -oikeudet!'
            else:
                say += 'lisää oikeuksia!'
            await ctx.rsend(say)

        elif isinstance(orig_error, commands.NotOwner):
            await ctx.rsend('Tämä komento on vain botin omistajan käytössä!')

        elif isinstance(orig_error, commands.InvalidEndOfQuotedStringError):
            await ctx.rsend(
                f'Sulkevan lainausmerkin jälkeen pitää tulla välilyönti, nyt '
                f'siinä on `{orig_error.char}`!'
            )

        elif isinstance(orig_error, commands.UnexpectedQuoteError):
            await ctx.rsend('Odottamaton lainausmerkki!')

        elif isinstance(orig_error, commands.ExpectedClosingQuoteError):
            await ctx.rsend('Lainausmerkit pitää sulkea!')

        else:
            self._logger.exception(
                f'Error in command {ctx.command}.',
                exc_info=(
                    type(orig_error),
                    orig_error,
                    orig_error.__traceback__
                )
            )
            await ctx.rsend('Tapahtui odottamaton virhe! Explo tietää jo.')


async def setup(bot):
    await bot.add_cog(Events(bot))
