"""
Contains all commands that make calls to Microsoft Azure based services.
"""

import typing
import logging
import re
from io import BytesIO

import aiohttp
import discord
from discord.ext import commands
from aiohttp.web import HTTPError

import processors
from processors import (
    image_utils,
    JobStatus,
    ClusterStatus,
    ProcessingError,
    NotFound,
    IncorrectState
)
from util import DiscordFile, CustomContext


STATUS_TO_HUMAN = {
    JobStatus.WAITING: 'Odotetaan tilaa',
    JobStatus.QUEUED: 'Jonossa',
    JobStatus.PROCESSING: 'Prosessoidaan',
    JobStatus.DONE: 'Valmis',
    JobStatus.ERROR: 'Virhe',
    JobStatus.CANCELLED: 'Peruutettu',
}


STATUS_COLORS = {
    JobStatus.WAITING: discord.Color.dark_gray(),
    JobStatus.QUEUED: discord.Color.blue(),
    JobStatus.PROCESSING: discord.Color.gold(),
    JobStatus.DONE: discord.Color.green(),
    JobStatus.ERROR: discord.Color.red(),
    JobStatus.CANCELLED: discord.Color.orange(),
}


UUID_REGEX = re.compile(
    r'^[a-f\d]{8}-[a-f\d]{4}-4[a-f\d]{3}-[89aAbB][a-f\d]{3}-[a-f\d]{12}$'
)


def _create_embed(
        job_id: str,
        status: JobStatus,
        cluster: ClusterStatus
) -> discord.Embed:
    embed = discord.Embed(title='Prosessoinnin tila').add_field(
        name='ID',
        value=job_id or '-',
        inline=False
    ).add_field(
        name='Tila',
        value=STATUS_TO_HUMAN.get(status, '-'),
        inline=False
    ).add_field(
        name='Klusterin tila',
        value=cluster.value if cluster is not None else '-',
        inline=False
    )
    embed.colour = STATUS_COLORS.get(
        status, discord.Color.default()
    )
    return embed


class StatusUpdater:
    def __init__(self, msg: discord.Message, logger: logging.Logger):
        self.msg = msg
        self._logger = logger

    async def callback(
            self,
            job_id: str,
            status: JobStatus,
            cluster: ClusterStatus = None
    ):
        self._logger.info(
            f'{job_id}: Status changed to {status} - {cluster}'
        )
        self.msg = \
            await self.msg.edit(embed=_create_embed(job_id, status, cluster))


class Azure(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

        azure_config = self.bot.bot_config.get('azure')
        func_url = azure_config.get('function_url')
        func_key = azure_config.get('key')

        self._azure = processors.AzureJobManager(
            func_url, func_key, self.bot.session
        )

    async def _deepfake(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None,
            driving_id: int = 0,
            filename: str = 'deepfake.mp4'
    ):
        async with ctx.typing():
            with BytesIO() as image_bytes:
                try:
                    await image_utils.get_image(
                        ctx, target, image_bytes=image_bytes
                    )
                except RuntimeError:
                    return
                embed = discord.Embed(title='Prosessoinnin tila')
                embed.add_field(name='ID', value='-', inline=False)
                embed.add_field(name='Tila', value='-', inline=False)
                embed.add_field(name='Klusterin tila', value='-', inline=False)

                msg = await ctx.rsend(embed=embed, mention_author=False)
                updater = StatusUpdater(msg, self._logger)

                azure_config = self.bot.bot_config.get('azure')
                skip_cluster = azure_config.get('skip_cluster', False)
                try:
                    video_bytes = await self._azure.submit_job(
                        job_data=image_bytes,
                        status_callback=updater.callback,
                        user_id=ctx.author.id,
                        driving_video_id=driving_id,
                        skip_cluster=skip_cluster
                    )
                except ProcessingError:
                    self._logger.error('Deepfake failed!', exc_info=True)
                    await ctx.rsend(
                        f'Komennon suoritus epäonnistui, pingaa Explo!'
                    )
                    return
                except aiohttp.ClientResponseError:
                    self._logger.error('Azure call failed!', exc_info=True)
                    await ctx.rsend(
                        'Komennon suoritus epäonnistui, yritä hetken kuluttua '
                        'uudelleen ja pingaa Explo jos ei vieläkään toimi!'
                    )
                    return

            if video_bytes is not None:
                await ctx.rsend(
                    file=DiscordFile(video_bytes, filename=filename)
                )
                await msg.delete()
                self._logger.info('Deepfake done!')
            else:
                self._logger.info('Deepfake cancelled')

    @commands.command(name='damedane', aliases=['bakamitai'])
    async def damedane(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee annetusta kuvasta deepfaken (Baka mitai).
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=0,
            filename='damedane.mp4'
        )

    @commands.command(name='riseandshine')
    async def riseandshine(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee annetusta kuvasta deepfaken (G-Manin Rise and shine -puhe).
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=1,
            filename='riseandshine.mp4'
        )

    @commands.command(name='sairasta')
    async def sairasta(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee ihan sairaan deepfaken.
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=2,
            filename='sairasta.mp4'
        )

    @commands.command(name='egg')
    async def egg(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee deepfaken, joka antaa +100 sosiaalista krediittiä.
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=3,
            filename='egg.mp4'
        )

    @commands.command(name='tainted', aliases=['taintedlove', 'jamming'])
    async def tainted_love(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee tainted love -deepfaken.
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=4,
            filename='taintedlove.mp4'
        )

    @commands.command(name='bingchilling')
    async def bingchilling(
            self,
            ctx: CustomContext,
            *,
            target: typing.Union[
                image_utils.PreviousImage, discord.Member, image_utils.Url
            ] = None
    ):
        """
        Tekee bing chilling -deepfaken.
        Lähdekuva tulee samalla logiikalla kuin monissa muissa kuvakomennoissa
        (ilman parametrejä tulee profiilikuva, voi pingata, laittaa liitteenä,
        antaa URL:n tai käyttää ^, jolloin viimeksi chattiin postattu kuva
        otetaan).

        HUOM: tämän komennon suoritus voi kestää hyvin kauan koska Azure
        """
        await self._deepfake(
            ctx=ctx,
            target=target,
            driving_id=5,
            filename='bingchilling.mp4'
        )

    @commands.command('cancel')
    async def cancel(self, ctx: CustomContext, job_id: str):
        """
        Peruu ID:tä vastaavan jobin.
        Ainoastaan itse lähetetyt (!damedane) jobit voi perua. ID löytyy botin
        komennon jälkeen lähettämästä embedistä. Lisäksi jobin on oltava
        jonossa, muussa tilassa olevia jobeja ei voi perua. Botin omistaja voi
        perua minkä tahansa jobin.
        """
        if not UUID_REGEX.match(job_id):
            await ctx.rsend('ID ei ole oikeassa muodossa!')
            return

        async with ctx.typing():
            # Owner can cancel any job
            if not self._azure.check_job(ctx.author.id, job_id) and \
                    not await self.bot.is_owner(ctx.author):
                await ctx.rsend(
                    'Yrität perua jonkun toisen lähettämää työtä tai ID:llä ei '
                    'löydy mitään!'
                )
                return
            try:
                await self._azure.cancel_job(job_id)
            except NotFound:
                # NOTE: These 2 first excepts are technically unneeded due to
                # the user check above, but these checks can be needed for the
                # owner
                await ctx.rsend('ID:llä ei löydy mitään!')
            except IncorrectState:
                await ctx.rsend('Ainoastaan jonossa olevat työt voi perua!')
            except HTTPError:
                self._logger.exception(f'Error cancelling job {job_id}')
                await ctx.rsend('Peruminen epäonnistui!')
            else:
                await ctx.rsend('Peruttu!')

    @commands.command('queue')
    async def queue(self, ctx: CustomContext):
        """
        Näyttää jonotilanteen.
        Teknisistä syistä jonossa näkyvät myös peruutetut työt, jotka kuitenkin
        hypätään yli, kun niitä alettaisiin prosessoida.
        """
        async with ctx.typing():
            try:
                queue = await self._azure.get_queue()
            except HTTPError:
                self._logger.exception('Failed to get queue')
                await ctx.rsend('Jonon hakeminen epäonnistui!')
                return

            embed = discord.Embed(
                color=STATUS_COLORS.get(
                    JobStatus.QUEUED,
                    discord.Color.default()
                )
            )
            embed.title = 'Jonon tila'
            processing_jobs = self._azure.get_processing_jobs()
            if len(queue) == 0 and len(processing_jobs) == 0:
                embed.description = 'Jono on tyhjä!'
            else:
                queue_list = []
                processing_list = []
                for job_id in processing_jobs:
                    user = self.bot.get_user(processing_jobs[job_id].user_id)
                    processing_list.append(f'{job_id} ({user.mention})')

                idx = 1
                for job_id in queue:
                    if job_id in processing_jobs:
                        continue
                    queue_str = f'{idx}: {job_id}'
                    user_id = self._azure.get_job_user(job_id)
                    if user_id is not None:
                        queue_str += f' ({self.bot.get_user(user_id).mention})'
                    queue_list.append(queue_str)
                    idx += 1

                if len(processing_list) > 0:
                    embed.add_field(
                        name='Prosessoidaan',
                        value='\n'.join(processing_list),
                        inline=False
                    )
                if len(queue_list) > 0:
                    embed.add_field(
                        name='Jonossa',
                        value='\n'.join(queue_list),
                        inline=False
                    )
                embed.set_footer(
                    text='Huom: myös perutut työt voivat näkyä jonossa.'
                )
            await ctx.rsend(embed=embed, mention_author=False)

    @damedane.error
    @riseandshine.error
    @sairasta.error
    @egg.error
    @tainted_love.error
    @bingchilling.error
    async def azure_error(
            self,
            ctx: CustomContext,
            error: commands.CommandError
    ):
        await image_utils.handle_image_error(ctx, error)


async def setup(bot):
    await bot.add_cog(Azure(bot))
