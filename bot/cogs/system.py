# Commands related to the bot system itself

import logging
import enum
import typing
from pprint import pformat

import aiohttp
import discord
from discord.ext import commands

import processors
from util import CustomContext


_LOGLEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warn': logging.WARNING,
    'err': logging.ERROR,
    'crit': logging.CRITICAL
}


class Logger(enum.Enum):
    BOT_LOGGER = 'bot'
    DISCORD_LOGGER = 'discord'

    @classmethod
    async def convert(cls, ctx: commands.Context, argument: str):
        for log in cls:
            if log.value.startswith(argument.lower()):
                return log
        else:
            raise commands.BadArgument(
                f'Failed to parse \'{argument}\' as a known logger instance!'
            )


class System(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @commands.command(name='reload-extension', hidden=True, aliases=['rel'])
    @commands.is_owner()
    async def reload(self, ctx: CustomContext, ext: str = None):
        try:
            if ext is None:
                to_reload = self.bot.extensions.copy()
                for extension in to_reload:
                    await self._reload_extension(extension)
            else:
                await self._reload_extension(ext)
            await ctx.message.add_reaction('✅')
        except commands.ExtensionError as e:
            self._logger.exception(f'Extension {e.name} could not be reloaded!')
            await ctx.message.add_reaction('❌')
            return

    @commands.command(name='issue', hidden=True)
    @commands.is_owner()
    async def issue(
            self,
            ctx: CustomContext,
            title: str,
            desc: str,
            labels: str = None
    ):
        if labels is None:
            labels = ''

        gitlab_config = self.bot.config.get('gitlab')
        if gitlab_config is None:
            await ctx.send(
                f'{ctx.author.mention}, gitlabia ei ole konfiguroitu.'
            )
            return

        gitlab_token = self.bot.tokens.get('gitlab')
        gitlab_repo = gitlab_config.get('project', None)

        if gitlab_token is None or gitlab_repo is None:
            await ctx.rsend('Gitlabia ei ole konfiguroitu.')
            return

        try:
            resp_json = await processors.create_issue(
                gitlab_token,
                gitlab_repo,
                title,
                desc,
                labels,
                self.bot.session
            )
        except aiohttp.ClientResponseError as err:
            await ctx.rsend('Issuen teko epäonnistui!')
            self._logger.error('Failed to create a new issue!')
            self._logger.error(f'Status: {err.status}, Message: {err.message}')
            return

        self._logger.info(
            f'New issue created by {ctx.author} (ID={ctx.author.id})!'
        )
        self._logger.info(pformat(resp_json))
        await ctx.rsend(f'Issue tehty! {resp_json["web_url"]}')

    async def _reload_extension(self, extension: str):
        self._logger.warning(f'Reloading extension {extension}!')
        await self.bot.reload_extension(extension)
        # Ugly hack for reloading the subscriptions because it needs the database
        if extension == 'cogs.twitch':
            async with self.bot.create_db_session() as db_session:
                await self.bot.twitch.subscribe(db_session)

    @commands.command(name='loglevel', hidden=True)
    @commands.is_owner()
    async def set_loglevel(
            self,
            ctx: CustomContext,
            loggers: commands.Greedy[Logger] = None,
            loglevel: str = None
    ):
        if loglevel is None:
            self._logger.setLevel(logging.INFO)

            discord_logger = logging.getLogger('discord')
            discord_logger.setLevel(logging.NOTSET)

            self._logger.info('Loglevels have been reset!')
        else:
            level = _LOGLEVELS.get(loglevel.lower(), None)
            if level is None:
                await ctx.message.add_reaction('❌')
                return

            if loggers is None:
                loggers = []
                for logger in Logger:
                    loggers.append(logger)

            for i in range(len(loggers)):
                logging.getLogger(loggers[i].value).setLevel(level)

            self._logger.log(level, f'Loglevel set to {loglevel}!')

        await ctx.message.add_reaction('✅')

    @commands.command(hidden=True)
    @commands.guild_only()
    @commands.is_owner()
    async def sync(
            self,
            ctx: CustomContext,
            guilds: commands.Greedy[discord.Object],
            spec: typing.Optional[typing.Literal['~', '*', '^']] = None
    ) -> None:
        # From https://about.abstractumbra.dev/discord.py/2023/01/29/sync-command-example.html
        async with ctx.typing():
            if not guilds:
                if spec == '~':
                    # This will sync all guild commands for the current context’s guild.
                    synced = await ctx.bot.tree.sync(guild=ctx.guild)
                elif spec == '*':
                    # This command copies all global commands to the current guild (within the CommandTree) and syncs.
                    ctx.bot.tree.copy_global_to(guild=ctx.guild)
                    synced = await ctx.bot.tree.sync(guild=ctx.guild)
                elif spec == '^':
                    # This command will remove all guild commands from the CommandTree and syncs, which effectively
                    # removes all commands from the guild.
                    ctx.bot.tree.clear_commands(guild=ctx.guild)
                    await ctx.bot.tree.sync(guild=ctx.guild)
                    synced = []
                else:
                    synced = await ctx.bot.tree.sync()

                await ctx.send(
                    f'Synkronoitu {len(synced)} komento{"a" if len(synced) != 1 else ""} '
                    f'{"kaikkialle." if spec is None else "tähän servuun."}'
                )
                return

            ret = 0
            for guild in guilds:
                try:
                    await ctx.bot.tree.sync(guild=guild)
                except discord.HTTPException:
                    pass
                else:
                    ret += 1

            await ctx.send(f'Servusynkronoinnin onnistuminen: {ret}/{len(guilds)}.')


async def setup(bot):
    await bot.add_cog(System(bot))
