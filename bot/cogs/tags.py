import logging

import discord
from discord.ext import commands
from discord import app_commands

import db
from util import CustomContext, try_except
from custom_ui import TagType, TagEditor, TagSelector, TagList, get_reserved_types


async def _allowed_types_for(member: discord.Member, bot):
    allowed_types = TagType.USER

    if await bot.is_owner(member):
        allowed_types |= TagType.BOT
    if member is None:
        raise ValueError('Member is None in allowed_types_for!')
    if member.guild_permissions.administrator:
        allowed_types |= TagType.GUILD
    return allowed_types


def _expand_taglist(tags: list[db.Tag], user_id: int) -> tuple[db.Tag, db.Tag, db.Tag, db.Tag]:
    user_tag = None
    user_global_tag = None
    guild_tag = None
    bot_tag = None

    for tag in tags:
        if tag.owner_type == db.TagOwnerType.BOT:
            bot_tag = tag
            break
        elif tag.owner_type == db.TagOwnerType.GUILD:
            guild_tag = tag
        elif tag.owner_type == db.TagOwnerType.USER:
            if tag.owner == user_id:
                user_tag = tag
            if tag.is_global:
                user_global_tag = tag

    return user_tag, user_global_tag, guild_tag, bot_tag


def _limit_choice_length(search_results: list[str]) -> list[app_commands.Choice]:
    choices = []
    for result in search_results:
        name = value = result
        if len(result) > 100:
            continue
        choices.append(app_commands.Choice(name=name, value=value))
    return choices


class Tags(commands.Cog):
    group = app_commands.Group(name='tag', description='Luo, muokkaa ja näytä tageja!', guild_only=True)

    def __init__(self, bot):
        self.bot = bot
        self._logger = logging.getLogger('bot')

    @group.command(name='add', description='Lisää tagi')
    @app_commands.rename(name='nimi')
    @app_commands.describe(name='Tagin nimi')
    async def add_tag(self, interaction: discord.Interaction, name: str):
        member = interaction.user
        if not isinstance(member, discord.Member):
            member = interaction.guild.get_member(member.id)

        async with interaction.db_session.begin():
            reserved_types = await get_reserved_types(
                interaction.db_session,
                name,
                interaction.user.id,
                interaction.guild_id
            )

        if TagType.BOT in reserved_types:
            await interaction.response.send_message('Antamasi nimi on varattu!', ephemeral=True)
            return

        tag = db.Tag(key=name, creation_guild=interaction.guild_id, is_global=False)

        allowed_types = await _allowed_types_for(member, self.bot)

        if (allowed_types & reserved_types) == allowed_types:
            await interaction.response.send_message('Antamasi nimi on varattu!', ephemeral=True)
            return

        editor = TagEditor(self.bot, allowed_types, reserved_types, tag, interaction.user, interaction)
        await interaction.response.send_message(
            editor.message_content(),
            view=editor,
            allowed_mentions=discord.AllowedMentions.none()
        )

    @group.command(name='show', description='Näytä tagi')
    @app_commands.rename(name='nimi')
    @app_commands.describe(name='Tagin nimi')
    async def show_tag(self, interaction: discord.Interaction, name: str):
        async with interaction.db_session.begin():
            tags = await db.Tag.get(interaction.db_session, name, interaction.user.id, interaction.guild_id)

        if len(tags) == 0:
            await interaction.response.send_message('Tagia ei löydy!', ephemeral=True)
            return

        user_tag, user_global_tag, guild_tag, bot_tag = _expand_taglist(tags, interaction.user.id)

        if bot_tag is not None:
            await interaction.response.send_message(
                content=bot_tag.content,
                allowed_mentions=discord.AllowedMentions.none()
            )
        elif len(tags) == 1:
            await interaction.response.send_message(
                content=tags[0].content,
                allowed_mentions=discord.AllowedMentions.none()
            )
        else:
            async def op(inter: discord.Interaction, selected_tag: db.Tag):
                await inter.response.edit_message(
                    content=selected_tag.content,
                    view=None,
                    allowed_mentions=discord.AllowedMentions.none()
                )
            await interaction.response.send_message(
                content='Useita tageja löytyi samalla nimellä. Valitse, mikä niistä näytetään:',
                view=TagSelector(
                    self.bot,
                    interaction.user,
                    interaction,
                    user_tag,
                    user_global_tag,
                    guild_tag,
                    None,  # bot_tag
                    op
                )
            )

    @commands.command(name='tag', aliases=['t'])
    async def show_tag_cmd(self, ctx: CustomContext, name: str):
        async with ctx.db_session.begin():
            tags = await db.Tag.get(ctx.db_session, name, ctx.author.id, ctx.guild.id)

        if len(tags) == 0:
            await ctx.rsend('Tagia ei löydy!', ephemeral=True)
            return

        user_tag, user_global_tag, guild_tag, bot_tag = _expand_taglist(tags, ctx.author.id)

        if bot_tag is not None:
            await ctx.rsend(
                content=bot_tag.content,
                mention_author=False,
                allowed_mentions=discord.AllowedMentions.none()
            )
        elif len(tags) == 1:
            await ctx.rsend(
                content=tags[0].content,
                mention_author=False,
                allowed_mentions=discord.AllowedMentions.none()
            )
        else:
            async def op(inter: discord.Interaction, selected_tag: db.Tag):
                await inter.response.edit_message(
                    content=selected_tag.content,
                    view=None,
                    allowed_mentions=discord.AllowedMentions.none()
                )
            selector = TagSelector(self.bot, ctx.author, None, user_tag, user_global_tag, guild_tag, None, op)
            msg = await ctx.rsend(
                content='Useita tageja löytyi samalla nimellä. Valitse, mikä niistä näytetään:',
                view=selector,
                mention_author=False
            )
            selector.message = msg

    @group.command(name='delete', description='Poista tagi')
    @app_commands.rename(name='nimi')
    @app_commands.describe(name='Tagin nimi')
    async def delete_tag(self, interaction: discord.Interaction, name: str):
        member = interaction.user
        if not isinstance(member, discord.Member):
            member = interaction.guild.get_member(member.id)

        allowed_types = await _allowed_types_for(member, self.bot)

        async with interaction.db_session.begin():
            tags = await db.Tag.get(interaction.db_session, name, interaction.user.id, interaction.guild_id)

        if len(tags) == 0:
            await interaction.response.send_message('Tagia ei löydy!', ephemeral=True)
            return

        user_tag, user_global_tag, guild_tag, bot_tag = _expand_taglist(tags, interaction.user.id)

        async def op(inter: discord.Interaction, selected_tag: db.Tag):
            async with inter.db_session.begin():
                await inter.db_session.delete(selected_tag)
            await inter.db_session.commit()
            await inter.response.edit_message(content='Tagi poistettu.', view=None)

        await interaction.response.send_message(
            'Valitse poistettavan tagin tyyppi:',
            view=TagSelector(
                self.bot,
                interaction.user,
                interaction,
                user_tag if TagType.USER in allowed_types else None,
                None,
                guild_tag if TagType.GUILD in allowed_types else None,
                bot_tag if TagType.BOT in allowed_types else None,
                op
            ),
            ephemeral=True
        )

    @group.command(name='global', description='Tee tagista globaali tai poista globaalius')
    @app_commands.rename(name='nimi')
    @app_commands.describe(name='Tagin nimi')
    async def globalize(self, interaction: discord.Interaction, name: str):
        async with interaction.db_session.begin():
            tag = await db.Tag.get(
                interaction.db_session,
                name,
                interaction.user.id,
                interaction.guild_id,
                include_user_global=False,
                include_guild=False,
                include_bot=False,
                return_single=True,
            )
            if tag is None:
                await interaction.response.send_message('Tagia ei ole olemassa!', ephemeral=True)
                return

            if not tag.is_global:
                if not await tag.can_globalize(interaction.db_session):
                    await interaction.response.send_message('Tagista ei voi tehdä globaalia!', ephemeral=True)
                    return

                tag.is_global = True
                await interaction.db_session.merge(tag)
            else:
                tag.is_global = False
                await interaction.db_session.merge(tag)

        await interaction.db_session.commit()

        if tag.is_global:
            await interaction.response.send_message('Tagi on nyt globaali!', ephemeral=True)
        else:
            await interaction.response.send_message('Tagi ei ole enää globaali!', ephemeral=True)

    @group.command(name='info', description='Näytä tagin tiedot')
    @app_commands.rename(name='nimi')
    @app_commands.describe(name='Tagin nimi')
    async def info_tag(self, interaction: discord.Interaction, name: str):
        async with interaction.db_session.begin():
            tags = await db.Tag.get(interaction.db_session, name, interaction.user.id, interaction.guild_id)

        if len(tags) == 0:
            await interaction.response.send_message('Tagia ei löydy!', ephemeral=True)
            return

        user_tag, user_global_tag, guild_tag, bot_tag = _expand_taglist(tags, interaction.user.id)

        if bot_tag is not None:
            await interaction.response.send_message(embed=self._build_info_embed(bot_tag), ephemeral=True)
            return

        if user_tag is not None and user_global_tag is not None and user_tag == user_global_tag:
            user_global_tag = None

        first = True
        for tag in (user_tag, user_global_tag, guild_tag):
            if tag is None:
                continue
            if first:
                await interaction.response.send_message(embed=self._build_info_embed(tag), ephemeral=True)
                first = False
            else:
                await interaction.followup.send(embed=self._build_info_embed(tag), ephemeral=True)

    @group.command(name='list', description='Näytä lista omistamistasi tageista')
    async def list_tags(self, interaction: discord.Interaction):
        member = interaction.user
        if not isinstance(member, discord.Member):
            member = interaction.guild.get_member(member.id)

        allowed_types = await _allowed_types_for(member, self.bot)
        async with interaction.db_session.begin():
            tags = await db.Tag.get_owned_by(
                interaction.db_session,
                interaction.user.id,
                interaction.guild_id,
                bot_allowed=TagType.BOT in allowed_types,
                guild_allowed=TagType.GUILD in allowed_types,
            )
        if TagType.BOT in allowed_types or TagType.GUILD in allowed_types:
            texts = [f'{i + 1}. ({tag.owner_type.name[0]}) {tag.key}' for i, tag in enumerate(tags)]
        else:
            texts = [f'{i + 1}. {tag.key}' for i, tag in enumerate(tags)]

        view = TagList(
            texts,
            allowed_user=member,
            display_abbrevs=TagType.BOT in allowed_types or TagType.GUILD in allowed_types
        )
        await interaction.response.send_message(embed=view.build_embed(view.data[0]), ephemeral=True, view=view)

    def _build_info_embed(self, tag: db.Tag):
        if tag.owner_type == db.TagOwnerType.BOT:
            color = discord.Color.red()
        elif tag.owner_type == db.TagOwnerType.GUILD:
            color = discord.Color.blue()
        else:
            if tag.is_global:
                color = discord.Color.green()
            else:
                color = discord.Color.dark_green()
        embed = discord.Embed(
            title=tag.key,
            description=tag.content,
            color=color
        ).add_field(
            name='Tyyppi',
            value=tag.owner_type.translate()
        )
        if tag.owner_type == db.TagOwnerType.USER:
            user = self.bot.get_user(tag.owner)
            embed.add_field(
                name='Omistaja',
                value=user.display_name if user is not None else f'`{tag.owner}`'
            ).add_field(
                name='Globaali',
                value='Kyllä' if tag.is_global else 'Ei'
            )
        return embed

    @show_tag.autocomplete('name')
    @info_tag.autocomplete('name')
    @try_except
    async def tag_show_autocomplete(self, interaction: discord.Interaction, value: str):
        async with interaction.db_session.begin():
            search_results = \
                await db.Tag.search(interaction.db_session, value, interaction.user.id, interaction.guild_id)
        return _limit_choice_length(search_results)

    @delete_tag.autocomplete('name')
    @try_except
    async def tag_delete_autocomplete(self, interaction: discord.Interaction, value: str):
        member = interaction.user
        if not isinstance(member, discord.Member):
            member = interaction.guild.get_member(member.id)
        allowed_types = await _allowed_types_for(member, self.bot)

        async with interaction.db_session.begin():
            search_results = await db.Tag.search(
                    interaction.db_session,
                    value,
                    interaction.user.id,
                    interaction.guild_id,
                    include_bot=TagType.BOT in allowed_types,
                    include_guild=TagType.GUILD in allowed_types,
                    include_user_global=False,
                )
        return _limit_choice_length(search_results)

    @globalize.autocomplete('name')
    @try_except
    async def tag_global_autocomplete(self, interaction: discord.Interaction, value: str):
        async with interaction.db_session.begin():
            search_results = await db.Tag.search(
                interaction.db_session,
                value,
                interaction.user.id,
                interaction.guild_id,
                include_bot=False,
                include_guild=False,
                include_user_global=False,
            )
        return _limit_choice_length(search_results)


async def setup(bot):
    await bot.add_cog(Tags(bot))
