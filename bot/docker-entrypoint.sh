#!/bin/bash

set -eo pipefail

echo "Running Alembic migrations"
CONFIG_PATH=/app/config.toml alembic upgrade head

echo "Starting Circus"
exec "$@"
