import enum
from typing import Union, Iterable

from sqlalchemy import BigInteger, Column, Text, Enum, select, func, and_, Boolean, or_
from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession


def escape_like(string, escape_char='*'):
    """
    Escape the string parameter used in SQL LIKE expressions.

    Stolen from sqlalchemy-utils

    ::

        from sqlalchemy_utils import escape_like


        query = session.query(User).filter(
            User.name.ilike(escape_like('John'))
        )


    :param string: a string to escape
    :param escape_char: escape character
    """
    return (
        string
        .replace(escape_char, escape_char * 2)
        .replace('%', escape_char + '%')
        .replace('_', escape_char + '_')
    )


Base = declarative_base()


class ButtonRole(Base):
    __tablename__ = 'button_role'
    guild = Column(BigInteger, primary_key=True)
    role = Column(BigInteger, primary_key=True)
    emoji = Column(Text, nullable=False)
    description = Column(Text, nullable=False)

    @staticmethod
    async def count(db_session: AsyncSession, guild_id: int) -> int:
        return await db_session.scalar(
            select(func.count())
            .select_from(ButtonRole)
            .where(ButtonRole.guild == guild_id)
        )

    @staticmethod
    async def roles_for(
            db_session: AsyncSession,
            guild_id: int
    ) -> list["ButtonRole"]:
        return await db_session.scalars(
            select(ButtonRole)
            .where(ButtonRole.guild == guild_id)
        )


@enum.unique
class GuildSettingInfo(enum.Enum):
    def __new__(cls, setting_id: int, value_type: callable):
        obj = object.__new__(cls)
        obj._value_ = setting_id
        obj.value_type = value_type
        return obj

    # Button roles
    BUTTON_ROLE_CHANNEL = (0, int)
    BUTTON_ROLE_MESSAGE = (1, int)
    BUTTON_ROLE_EMBED_COLOR = (2, str)
    BUTTON_ROLE_EMBED_TITLE = (3, str)

    # Migrations
    COMMAND_PREFIX = (4, str)
    DELETE_CHANNEL = (5, int)
    LOG_CHANNEL = (6, int)
    GIF_DELAY = (7, int)
    IMAGE_LINK_WARNING_TOGGLE = (8, int)

    # Greetings
    LEAVE_GREETING_TEMPLATE = (9, str)
    JOIN_GREETING_TEMPLATE = (10, str)
    JOIN_GREETING_CHANNEL = (11, int)

    # Channel for posting twitch notifications
    TWITCH_NOTIFICATION_CHANNEL = (12, int)


class GuildSetting(Base):
    __tablename__ = 'guild_setting'
    guild = Column(BigInteger, primary_key=True)
    setting_info = Column(Enum(GuildSettingInfo), primary_key=True)
    value = Column(Text, nullable=True)

    def get_value(self):
        # Cast the value
        return self.setting_info.value_type(self.value)

    @staticmethod
    async def settings_for(
            db_session: AsyncSession,
            guild_id: Union[int, Iterable[int]],
            *,
            settings: Union[GuildSettingInfo, Iterable[GuildSettingInfo]] = None
    ) -> Union["GuildSetting", list["GuildSetting"]]:
        return_first = False
        if settings is None:
            if isinstance(guild_id, int):
                where_clause = GuildSetting.guild == guild_id
            else:
                where_clause = GuildSetting.guild.in_(guild_id)
        else:
            if isinstance(settings, GuildSettingInfo) and isinstance(guild_id, int):
                settings = [settings]
                guild_id = [guild_id]
                return_first = True
            elif isinstance(settings, GuildSettingInfo):
                settings = [settings]
            elif isinstance(guild_id, int):
                guild_id = [guild_id]
            where_clause = and_(
                GuildSetting.guild.in_(guild_id),
                GuildSetting.setting_info.in_(settings)
            )

        # Note: assumes that Session.begin() was called elsewhere
        result = await db_session.scalars(
            select(GuildSetting)
            .where(where_clause)
        )
        scalars = result.all()
        if return_first:
            if len(scalars) == 0:
                return None
            return scalars[0]
        return scalars


class TwitchLiveNotification(Base):
    __tablename__ = 'twitch_live_notification'
    guild = Column(BigInteger, primary_key=True)
    broadcaster_user_id = Column(Text, primary_key=True)
    message = Column(Text, nullable=False)

    @staticmethod
    async def notifications_for(db_session: AsyncSession, *, guild_id: int = None, broadcaster_id: str = None):
        if (guild_id is None and broadcaster_id is None) or (guild_id is not None and broadcaster_id is not None):
            raise ValueError('You need to provide either guild_id or broadcaster_id!')
        if guild_id is not None:
            result = await db_session.scalars(
                select(TwitchLiveNotification)
                .where(TwitchLiveNotification.guild == guild_id)
            )
        else:
            result = await db_session.scalars(
                select(TwitchLiveNotification)
                .where(TwitchLiveNotification.broadcaster_user_id == broadcaster_id)
            )
        scalars = result.all()
        if len(scalars) == 0:
            return None
        return scalars


@enum.unique
class TagOwnerType(enum.Enum):
    USER = 'user'
    GUILD = 'guild'
    BOT = 'bot'

    def translate(self):
        if self == TagOwnerType.USER:
            return 'Käyttäjä'
        if self == TagOwnerType.GUILD:
            return 'Palvelin'
        if self == TagOwnerType.BOT:
            return 'Botti'


class Tag(Base):
    __tablename__ = 'tag'
    key = Column(Text, primary_key=True)
    owner_type = Column(Enum(TagOwnerType), primary_key=True, default=TagOwnerType.USER)
    owner = Column(BigInteger, primary_key=True)
    creation_guild = Column(BigInteger, nullable=False)
    is_global = Column(Boolean, nullable=False)
    content = Column(Text, nullable=False)

    @staticmethod
    async def get(
            db_session: AsyncSession,
            key: str,
            user_id: int,
            guild_id: int,
            *,
            include_bot: bool = True,
            include_user_global: bool = True,
            include_guild: bool = True,
            return_single: bool = False,
    ) -> Union[list["Tag"], "Tag"]:
        if include_user_global:
            or_list = [and_(Tag.owner_type == TagOwnerType.USER, or_(Tag.owner == user_id, Tag.is_global))]
        else:
            or_list = [and_(Tag.owner_type == TagOwnerType.USER, Tag.owner == user_id)]

        if include_bot:
            or_list.append(Tag.owner_type == TagOwnerType.BOT)
        if include_guild:
            or_list.append(and_(Tag.owner_type == TagOwnerType.GUILD, Tag.owner == guild_id),)

        result = await db_session.scalars(
            select(Tag).where(and_(Tag.key == key, or_(*or_list)))
        )
        if return_single:
            results = result.all()
            if len(results) == 0:
                return None
            return results[0]
        return result.all()

    @staticmethod
    async def get_owned_by(
            db_session: AsyncSession,
            user_id: int,
            guild_id: int,
            *,
            bot_allowed: bool = False,
            guild_allowed: bool = False,
    ) -> list["Tag"]:
        or_list = [and_(Tag.owner_type == TagOwnerType.USER, Tag.owner == user_id)]
        if bot_allowed:
            or_list.append(Tag.owner_type == TagOwnerType.BOT)
        if guild_allowed:
            or_list.append(and_(Tag.owner_type == TagOwnerType.GUILD, Tag.owner == guild_id))

        result = await db_session.scalars(select(Tag).where(or_(*or_list)).order_by(Tag.key))
        return result.all()

    @staticmethod
    async def search(
            db_session: AsyncSession,
            search_string: str,
            user_id: int,
            guild_id: int,
            *,
            include_bot: bool = True,
            include_user_global: bool = True,
            include_guild: bool = True,
    ) -> list[str]:
        or_list = [Tag.owner == user_id]
        if include_bot:
            or_list.append(Tag.owner_type == TagOwnerType.BOT)
        if include_user_global:
            or_list.append(Tag.is_global)
        if include_guild:
            or_list.append(Tag.owner == guild_id)

        result = await db_session.scalars(
            select(Tag.key).where(
                and_(
                    Tag.key.ilike(f'%{escape_like(search_string, '\\')}%', escape='\\'),
                    or_(*or_list)
                )
            ).distinct().limit(25).order_by(Tag.key)
        )
        return result.all()

    async def is_available(self, db_session: AsyncSession) -> bool:
        result = await db_session.scalars(
            select(Tag).where(
                and_(
                    Tag.key == self.key,
                    Tag.owner_type == TagOwnerType.BOT,
                )
            )
        )
        if len(result.all()) > 0:
            return False

        if self.owner_type == TagOwnerType.GUILD:
            result = await db_session.scalars(
                select(Tag).where(
                    and_(
                        Tag.key == self.key,
                        Tag.owner_type == TagOwnerType.GUILD,
                        Tag.owner == self.owner,
                    )
                )
            )
            if len(result.all()) > 0:
                return False
        elif self.owner_type == TagOwnerType.USER:
            result = await db_session.scalars(
                select(Tag).where(
                    and_(
                        Tag.key == self.key,
                        Tag.owner_type == TagOwnerType.USER,
                        Tag.owner == self.owner,
                    )
                )
            )
            if len(result.all()) > 0:
                return False
        return True

    async def can_globalize(self, db_session: AsyncSession):
        result = await db_session.scalars(
            select(Tag).where(
                and_(
                    Tag.key == self.key,
                    Tag.owner_type == TagOwnerType.BOT,
                )
            )
        )
        if len(result.all()) > 0:
            return False

        result = await db_session.scalars(
            select(Tag).where(
                and_(
                    Tag.key == self.key,
                    Tag.owner_type == TagOwnerType.USER,
                    Tag.owner != self.owner,
                    Tag.is_global,
                )
            )
        )
        if len(result.all()) > 0:
            return False
        return True
