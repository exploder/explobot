import io
import logging
import datetime
import traceback
import inspect
from functools import wraps
from typing import Callable, Optional, Union

import discord
import json_logging
from discord.ext import commands
from discord import utils


class DiscordFile:
    """
    Class that wraps around a discord.File so that it can be reused (used in
    reply_or_send in case the reply fails)
    """
    def __init__(self, fp, filename=None, *, spoiler=False):
        self.fp = fp
        self.filename = filename
        self.spoiler = spoiler

    def get_discord_file(self) -> discord.File:
        """
        Returns a discord.File object for given parameters. All buffers will be
        seeked to zero before the discord.File object is created.
        :return:
        """
        if isinstance(self.fp, io.BufferedIOBase):
            self.fp.seek(0)

        return discord.File(
            fp=self.fp,
            filename=self.filename,
            spoiler=self.spoiler
        )


class CustomContext(commands.Context):
    def __init__(self, **attrs):
        self._logger = logging.getLogger('bot')
        self.error_handled = False
        self.db_session = None
        super().__init__(**attrs)

    async def rsend(
            self,
            content: str = None,
            *,
            file: Optional[DiscordFile] = None,
            files: Optional[list[DiscordFile]] = None,
            **kwargs
    ):
        try:
            send_file = file.get_discord_file() if file is not None else None
            send_files = [f.get_discord_file() for f in files] \
                if files is not None else None

            return await self.reply(
                content,
                file=send_file,
                files=send_files,
                **kwargs
            )

        except discord.HTTPException as e:
            # The error code for 'invalid form body' is 50035 and the error
            # message seems to be 'In message_reference: Unknown message',
            # so if the error does not match these, log and reraise
            if e.code != 50035 or 'message_reference' not in e.text:
                self._logger.exception(
                    'Failed to reply to a command but the error was '
                    'unexpected!'
                )
                raise

            self._logger.debug(
                f'Failed to reply, trying to send normally',
                exc_info=True
            )
            # Original message was (probably) deleted so let's do it
            # traditionally

            # We need to regenerate the files
            send_file = file.get_discord_file() if file is not None else None
            send_files = [f.get_discord_file() for f in files] \
                if files is not None else None

            mention_author = kwargs.pop('mention_author', True)

            if mention_author:
                if content is None:
                    content = self.author.mention
                else:
                    content = f'{self.author.mention}, {content}'

                return await self.send(
                    content,
                    file=send_file,
                    files=send_files,
                    **kwargs
                )

            else:
                return await self.send(
                    content,
                    file=send_file,
                    files=send_files,
                    **kwargs
                )


class BetterJSONLogFormatter(json_logging.BaseJSONFormatter):
    """
    Custom formatter that preserves newlines
    """

    def get_exc_fields(self, record):
        if record.exc_info:
            exc_info = self.format_exception(record.exc_info)
        else:
            exc_info = record.exc_text
        return {
            'exc_info': exc_info,
            'filename': record.filename,
        }

    @classmethod
    def format_exception(cls, exc_info: tuple) -> str:
        return \
            ''.join(traceback.format_exception(*exc_info)) if exc_info else ''

    def _format_log_object(self, record: logging.LogRecord, request_util):
        json_log_object = super(BetterJSONLogFormatter, self).\
            _format_log_object(record, request_util)

        json_log_object.update({
            'msg': record.getMessage(),
            'type': 'log',
            'logger': record.name,
            'thread': record.threadName,
            'level': record.levelname,
            'module': record.module,
            'line_no': record.lineno,
        })

        if hasattr(record, 'props'):
            json_log_object.update(record.props)

        if record.exc_info or record.exc_text:
            json_log_object.update(self.get_exc_fields(record))

        return json_log_object


async def timeout_member(
        member: discord.Member,
        until: Optional[Union[
            datetime.timedelta,
            datetime.datetime
        ]],
        /,
        *,
        reason: Optional[str] = None
) -> Union[discord.Member, None]:
    """
    Applies a time out to a member until the specified date time or for the
    given :class:`datetime.timedelta`.

    You must have the :attr:`~Permissions.moderate_members` permission to
    use this.

    This raises the same exceptions as :meth:`edit`.

    Copied from discord.py.

    Parameters
    -----------
    member:
        The member to operate on.
    until: Optional[Union[:class:`datetime.timedelta`, :class:`datetime.datetime`]]
        If this is a :class:`datetime.timedelta` then it represents the amount of
        time the member should be timed out for. If this is a :class:`datetime.datetime`
        then it's when the member's timeout should expire. If ``None`` is passed then the
        timeout is removed. Note that the API only allows for timeouts up to 28 days.
    reason: Optional[:class:`str`]
        The reason for doing this action. Shows up on the audit log.

    Raises
    -------
    TypeError
        The ``until`` parameter was the wrong type or the datetime was not timezone-aware.

    Returns
    -------
    Member:
        The edited Member object
    """

    if until is None:
        timed_out_until = None
    elif isinstance(until, datetime.timedelta):
        timed_out_until = utils.utcnow() + until
    elif isinstance(until, datetime.datetime):
        timed_out_until = until
    else:
        raise TypeError(
            f'expected None, datetime.datetime, or datetime.timedelta not '
            f'{until.__class__!r}'
        )

    return await member.edit(timed_out_until=timed_out_until, reason=reason)


logger = logging.getLogger("autocompletes")


def try_except(func: Callable) -> Callable:
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            if inspect.iscoroutinefunction(func):
                return await func(*args, **kwargs)
            return func(*args, **kwargs)
        except Exception as e:
            logger.exception(f"Error in {func.__name__}: {e}")
            raise e

    return wrapper
