from typing import Any

import discord

from .paginator import ButtonPaginator


class HSComicPager(ButtonPaginator):
    def __init__(self, initial_comic_list: list[dict], allowed_user: discord.Member, comic_data_callable: callable):
        super().__init__(data=initial_comic_list, reverse=True, can_fetch_more=True, allowed_user=allowed_user)
        self.comic_data_callable = comic_data_callable

    def build_embed(self, element: Any) -> discord.Embed:
        return discord.Embed(
            colour=discord.Colour.from_str('#fff'),
            title=f'{element["title"]} {element["date"]}',
            url=element['hs_url']
        ).set_image(url=element['img_url']).set_footer(text=element['copyright'])

    async def fetch_more_items(self):
        self.data.extend(await self.comic_data_callable(start=len(self.data)))
