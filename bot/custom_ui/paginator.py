import enum
from typing import Any

import discord
from discord import ui

from .base import RestrictedView


class Direction(enum.Enum):
    BACKWARD = enum.auto()
    FORWARD = enum.auto()


class ButtonPaginator(RestrictedView):
    def __init__(
            self,
            data: list[Any],
            allowed_user: discord.User,
            reverse: bool = False,
            can_fetch_more: bool = False,
            **kwargs
    ):
        super().__init__(allowed_user, 'Vain {allowed_user.mention} saa vaihtaa sivua!', **kwargs)

        self.message = None
        self.data = data
        self.index = 0
        self.allowed_user = allowed_user
        self.reverse = reverse
        self.can_fetch_more = can_fetch_more
        self._update_buttons()

    async def on_timeout(self) -> None:
        if self.message is None:
            return
        await self.message.edit(
            view=None,
            allowed_mentions=discord.AllowedMentions.none()
        )

    @ui.button(label='<')
    async def backward(self, interaction: discord.Interaction, button: ui.Button):
        await self.move(Direction.BACKWARD, interaction)

    @ui.button(label='>')
    async def forward(self, interaction: discord.Interaction, button: ui.Button):
        await self.move(Direction.FORWARD, interaction)

    async def move(self, direction: Direction, interaction: discord.Interaction):
        fetched_more = False
        if self.can_fetch_more and self.index == len(self.data) - 1:
            await interaction.response.defer()
            await self.fetch_more_items()
            fetched_more = True
        if (self.reverse and direction == direction.BACKWARD) or (not self.reverse and direction == Direction.FORWARD):
            if self.index == len(self.data) - 1:
                await interaction.response.send_message(
                    content='Tuloksia ei ole enempää!',
                    ephemeral=True
                )
                return
            self.index += 1
        elif (self.reverse and direction == direction.FORWARD) or (not self.reverse and direction == Direction.BACKWARD):
            if self.index == 0:
                await interaction.response.send_message(
                    content='Tuloksia ei ole enempää!',
                    ephemeral=True
                )
                return
            self.index -= 1
        self._update_buttons()
        embed = self.build_embed(self.data[self.index])
        if fetched_more:
            await interaction.followup.edit_message(self.message.id, embed=embed, view=self)
        else:
            await interaction.response.edit_message(embed=embed, view=self)

    def _update_buttons(self):
        self.backward.disabled = False
        self.forward.disabled = False

        if self.reverse:
            if self.index == len(self.data) - 1 and not self.can_fetch_more:
                self.backward.disabled = True
            if self.index == 0:
                self.forward.disabled = True
            return

        if self.index == 0:
            self.backward.disabled = True
        if self.index == len(self.data) - 1 and not self.can_fetch_more:
            self.forward.disabled = True

    def build_embed(self, element: Any) -> discord.Embed:
        raise NotImplementedError()

    async def fetch_more_items(self):
        raise NotImplementedError()
