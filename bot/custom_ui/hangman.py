from typing import Union
from urllib.parse import quote_plus

import discord
from discord import ui

import processors
from .base import RestrictedView


class HangmanView(RestrictedView):
    def __init__(
            self,
            game: processors.Hangman,
            allowed_user: Union[discord.User, None],
            **kwargs
    ):
        super().__init__(allowed_user, 'Vain {allowed_user.mention} saa arvata!', **kwargs)
        self.game = game
        self.message = None
        idx = 0
        for row in range(5):
            for _ in range(5):
                self.add_item(HangmanButton(
                    label=processors.HM_ALLOWED_LETTERS[idx].upper(),
                    row=row,
                    style=discord.ButtonStyle.primary
                ))
                idx += 1

    async def on_timeout(self) -> None:
        self.game.timeout()
        await self.message.edit(
            content=self.game.get_message_content(),
            view=KotusLinkView(self.game.the_word),
            allowed_mentions=discord.AllowedMentions.none()
        )


class HangmanButton(ui.Button):
    view: HangmanView

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    async def callback(self, interaction: discord.Interaction):
        assert self.view is not None

        self.disabled = True

        if self.view.game.guess_char(self.label.lower()):
            self.style = discord.ButtonStyle.green
        else:
            self.style = discord.ButtonStyle.red

        await interaction.response.edit_message(
            content=self.view.game.get_message_content(),
            view=self.view if self.view.game.ongoing else KotusLinkView(self.view.game.the_word)
        )

        if not self.view.game.ongoing:
            self.view.stop()


class KotusLinkView(ui.View):
    def __init__(self, word):
        super().__init__(timeout=None)
        word = quote_plus(word)
        url = f'https://www.kielitoimistonsanakirja.fi/{word}'
        self.add_item(
            ui.Button(
                label='Hae sanaa Kielitoimiston sanakirjasta',
                url=url,
                style=discord.ButtonStyle.link
            )
        )
