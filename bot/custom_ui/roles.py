import logging

import discord
import discord.utils as dutils
from discord import ui

import db
from util import CustomContext
from .base import ErrorHandlerView


class RoleRemovalConfirm(ErrorHandlerView):
    def __init__(self, role: discord.Role):
        super().__init__(timeout=60)
        self.role = role

    @discord.ui.button(label='Kyllä', style=discord.ButtonStyle.red)
    async def confirm(
            self,
            interaction: discord.Interaction,
            button: discord.ui.Button
    ):
        # No checks for whether the role is in the list of addable roles since
        # I cannot easily inject my own code into the Interaction creation and
        # create a DB session there...
        try:
            await interaction.user.remove_roles(self.role)
        except discord.Forbidden:
            await interaction.response.edit_message(
                content=f'Minulla ei ole oikeutta poistaa '
                        f'{self.role.mention}-roolia!',
                view=None,
                allowed_mentions=discord.AllowedMentions.none()
            )
        except discord.HTTPException:
            self.logger.warning('Role removal failed', exc_info=True)
            await interaction.response.edit_message(
                content='Valitsemasi roolin poistaminen epäonnistui!',
                view=None
            )
        else:
            await interaction.response.edit_message(
                content=f'{self.role.mention}-rooli poistettu.',
                view=None,
                allowed_mentions=discord.AllowedMentions.none()
            )
        self.stop()

    @discord.ui.button(label='En', style=discord.ButtonStyle.grey)
    async def cancel(
            self,
            interaction: discord.Interaction,
            button: discord.ui.Button
    ):
        await interaction.response.edit_message(
            content='Roolia ei poistettu.',
            view=None
        )
        self.stop()


class RoleView(ErrorHandlerView):
    def __init__(self):
        super().__init__(timeout=None)

    def add_role(self, role_info: db.ButtonRole):
        self.add_item(RoleButton(
            emoji=discord.PartialEmoji.from_str(role_info.emoji),
            role_id=role_info.role,
            logger=self.logger
        ))

    @classmethod
    async def from_roles(
            cls,
            ctx: CustomContext,
            roles: list[db.ButtonRole]
    ) -> tuple[
        "RoleView",
        list[db.ButtonRole],
        list[db.ButtonRole]
    ]:
        view = cls()
        removed_roles = []
        added_roles = []

        for role_info in roles:
            discord_role = dutils.get(ctx.guild.roles, id=role_info.role)
            if discord_role is None:
                await ctx.db_session.delete(role_info)
                removed_roles.append(role_info)
                continue

            added_roles.append(role_info)

            view.add_role(role_info)

        return view, added_roles, removed_roles


class RoleButton(ui.Button):
    def __init__(self, role_id: int, logger: logging.Logger, **kwargs):
        super().__init__(custom_id=f'{role_id}', **kwargs)
        self._logger = logger

    async def callback(self, interaction: discord.Interaction):
        role = dutils.get(interaction.guild.roles, id=int(self.custom_id))
        if role is None:
            await interaction.response.send_message(
                'Valitsemaasi roolia ei jostain syystä löydy! '
                'Ilmoita asiasta ylläpidolle.',
                ephemeral=True,
            )
            return

        user = interaction.user
        if role in user.roles:
            view = RoleRemovalConfirm(role)
            await interaction.response.send_message(
                f'Sinulla on jo {role.mention}-rooli! '
                f'Haluatko varmasti poistaa sen?',
                ephemeral=True,
                view=view,
                allowed_mentions=discord.AllowedMentions.none()
            )
        else:
            await user.add_roles(role)
            await interaction.response.send_message(
                f'Sait {role.mention}-roolin!',
                ephemeral=True,
                allowed_mentions=discord.AllowedMentions.none()
            )
