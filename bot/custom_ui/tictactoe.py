import random

import discord
from discord import ui

from .base import ErrorHandlerView


_THREE_COMBINATIONS = [
    # Rows
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    # Columns
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    # Diagonals
    [0, 4, 8],
    [2, 4, 6]
]


def _check_winner(board: list[int]) -> int:
    for combination in _THREE_COMBINATIONS:
        if sum(board[i] for i in combination) == 3:
            return 1
        if sum(board[i] for i in combination) == -3:
            return -1

    if not any(board[i] == 0 for i in range(9)):
        return -2
    return 0


def _three_filling_pos(board: list[int], player: int) -> None | int:
    # Return the position where playing would fill a three-in-a-row, or None if no such spots can be found
    for combination in _THREE_COMBINATIONS:
        if sum(board[i] for i in combination) != 2 * player:
            continue
        for i in range(3):
            if board[combination[i]] == 0:
                return combination[i]
    return None


def _minimax(board: list[int], player: int, current_turn: int = None, depth: int = 0) -> (int, int):
    winner_state = _check_winner(board)
    if winner_state == -2:
        return 0, None
    score = winner_state * player * (10 - depth)
    if score != 0:
        return score, None

    if current_turn is None:
        current_turn = player

    if current_turn == player:
        extremum_score = -float('inf')
    else:
        extremum_score = float('inf')
    extremum_choices = None

    for i in range(9):
        if board[i] != 0:
            continue
        board[i] = current_turn
        score, _ = _minimax(board, player, -current_turn, depth + 1)

        if score == extremum_score:
            extremum_choices.append(i)

        if current_turn == player:
            if score > extremum_score:
                extremum_score, extremum_choices = score, [i]
        else:
            if score < extremum_score:
                extremum_score, extremum_choices = score, [i]

        board[i] = 0

    return extremum_score, extremum_choices


class TicTacToeBoard:
    def __init__(self):
        self._board = [
            0, 0, 0,
            0, 0, 0,
            0, 0, 0,
        ]

    def play(self, player: int, pos: int):
        if player not in (-1, 1):
            raise ValueError('Player must be either -1 or 1')
        if not 0 <= pos <= 8:
            raise ValueError('Position is outside the board!')
        if self._board[pos] != 0:
            raise ValueError('This position has already been played!')
        self._board[pos] = player

    def play_bot_easy(self, player: int):
        # First, prioritize completing own three-in-a-row
        pos = _three_filling_pos(self._board, player)

        # Then, try to block the opponent
        if pos is None:
            pos = _three_filling_pos(self._board, -player)

        # Otherwise, pick a random spot
        if pos is None:
            pos = random.randint(0, 8)
            while self._board[pos] != 0:
                pos = random.randint(0, 8)

        self.play(player, pos)
        return pos

    def play_bot_hard(self, player: int):
        # Minimax algorithm (should be perfect :D)
        _, pos_choices = _minimax(self._board, player)
        pos = random.choice(pos_choices)
        self.play(player, pos)
        return pos

    def check_winner(self) -> int:
        return _check_winner(self._board)


class TicTacToeButton(ui.Button):
    def __init__(self, pos: int, game_callback, **kwargs):
        super().__init__(**kwargs)
        self.pos = pos
        self.game_callback = game_callback

    async def callback(self, interaction: discord.Interaction):
        await self.game_callback(interaction, self, self.pos)


LABELS = ['X', 'O']
COLORS = [discord.ButtonStyle.green, discord.ButtonStyle.blurple]


class TicTacToeView(ErrorHandlerView):
    def __init__(self, initial_user: discord.User, **kwargs):
        super().__init__(**kwargs)
        self.initial_user = initial_user
        self.players: list[discord.User | None] = [initial_user]
        self.message = None
        self.in_turn = 0
        self.board = TicTacToeBoard()
        self.board_btns = []
        self._bot_play: callable = None
        self._bot_difficulty = None

    async def on_timeout(self) -> None:
        if self.message is None:
            return

        if len(self.board_btns) == 0:
            self.clear_items()
        else:
            for i in range(9):
                self.board_btns[i].disabled = True

        await self.message.edit(
            content=f'Aika loppui!\n\n{self._matchup_str()}',
            view=self,
            allowed_mentions=discord.AllowedMentions.none()
        )

    async def interaction_check(self, interaction: discord.Interaction, /) -> bool:
        if len(self.players) == 1:
            return True
        if interaction.user not in self.players:
            if self.players[1] is not None:
                await interaction.response.send_message(
                    f'Vain {self.initial_user.mention} ja {self.players[1].mention} saavat pelata!',
                    ephemeral=True
                )
            else:
                await interaction.response.send_message(
                    f'Vain {self.initial_user.mention} saa pelata!',
                    ephemeral=True
                )
            return False
        if interaction.user != self.players[self.in_turn]:
            await interaction.response.send_message('Odota omaa vuoroasi!', ephemeral=True)
            return False
        return True

    @ui.button(label='Botti (helppo)', style=discord.ButtonStyle.blurple)
    async def easy_ai_button(self, interaction: discord.Interaction, button: ui.Button):
        if interaction.user != self.initial_user:
            await interaction.response.send_message(
                f'Vain {self.initial_user.mention} saa valita bottivastustajan!',
                ephemeral=True
            )
            return
        self.players.append(None)
        self._bot_play = self.board.play_bot_easy
        self._bot_difficulty = 'helppo'
        await self._start_game(interaction)

    @ui.button(label='Botti (vaikea)', style=discord.ButtonStyle.blurple)
    async def hard_ai_button(self, interaction: discord.Interaction, button: ui.Button):
        if interaction.user != self.initial_user:
            await interaction.response.send_message(
                f'Vain {self.initial_user.mention} saa valita bottivastustajan!',
                ephemeral=True
            )
            return
        self.players.append(None)
        self._bot_play = self.board.play_bot_hard
        self._bot_difficulty = 'vaikea'
        await self._start_game(interaction)

    @ui.button(label='Pelaa!', style=discord.ButtonStyle.green)
    async def opponent_button(self, interaction: discord.Interaction, button: ui.Button):
        if interaction.user == self.initial_user:
            await interaction.response.send_message('Et voi pelata itseäsi vastaan!', ephemeral=True)
            return
        self.players.append(interaction.user)
        await self._start_game(interaction)

    async def button_callback(self, interaction: discord.Interaction, button: ui.Button, pos: int):
        button.disabled = True
        button.label = LABELS[self.in_turn]
        button.style = COLORS[self.in_turn]
        self.board.play(2 * self.in_turn - 1, pos)
        self.in_turn = (self.in_turn + 1) % 2

        if await self._is_game_over(interaction):
            return

        if self.players[self.in_turn] is None:
            pos = self._bot_play(2 * self.in_turn - 1)

            self.board_btns[pos].disabled = True
            self.board_btns[pos].label = LABELS[self.in_turn]
            self.board_btns[pos].style = COLORS[self.in_turn]
            self.in_turn = (self.in_turn + 1) % 2

            if await self._is_game_over(interaction):
                return

        await interaction.response.edit_message(
            content=f'Peli käynnissä!\n\n{self._matchup_str()}\n\nVuorossa: {self._in_turn_mention()}',
            view=self,
            allowed_mentions=discord.AllowedMentions.none()
        )

    async def _start_game(self, interaction: discord.Interaction):
        self._add_board()
        self.in_turn = 0
        await interaction.response.edit_message(
            content=f'Peli käynnissä!\n\n{self._matchup_str()}\n\nVuorossa: {self._in_turn_mention()}',
            view=self,
            allowed_mentions=discord.AllowedMentions.none()
        )

    def _add_board(self):
        self.clear_items()
        for i in range(9):
            btn = TicTacToeButton(i, self.button_callback, row=i // 3, label='_')
            self.add_item(btn)
            self.board_btns.append(btn)

    async def _is_game_over(self, interaction) -> bool:
        winner = self.board.check_winner()
        if winner == 0:
            return False

        for i in range(9):
            self.board_btns[i].disabled = True

        if winner == -2:
            await interaction.response.edit_message(
                content=f'Tasapeli!\n\n{self._matchup_str()}',
                view=self,
                allowed_mentions=discord.AllowedMentions.none()
            )
        else:
            winner_idx = (winner + 2) // 2
            if self.players[winner_idx] is None:
                await interaction.response.edit_message(
                    content=f'Botti voitti!\n\n{self._matchup_str()}',
                    view=self,
                    allowed_mentions=discord.AllowedMentions.none()
                )
            else:
                await interaction.response.edit_message(
                    content=f'{self.players[winner_idx].mention} voitti!\n\n{self._matchup_str()}',
                    view=self,
                    allowed_mentions=discord.AllowedMentions.none()
                )
        self.stop()
        return True

    def _matchup_str(self):
        if len(self.players) == 1:
            return ''
        return \
            f'Pelaajat:\n{self.initial_user.mention} vastaan ' \
            f'{self.players[1].mention if self.players[1] is not None else f"Botti ({self._bot_difficulty})"}'

    def _in_turn_mention(self):
        if self.players[self.in_turn] is None:
            return 'Botti'
        return self.players[self.in_turn].mention
