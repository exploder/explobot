import enum
from typing import Callable, Coroutine

import discord
from discord import ui
from sqlalchemy.ext.asyncio import AsyncSession

import db
from hacks import ViewWithDB
from .base import RestrictedView, ErrorHandlerModal
from .paginator import ButtonPaginator


class TagType(enum.Flag):
    NONE = 0
    BOT = enum.auto()
    GUILD = enum.auto()
    USER = enum.auto()


async def get_reserved_types(db_session: AsyncSession, key: str, user_id: int, guild_id: int):
    reserved_types = TagType.NONE
    existing_tags = await db.Tag.get(
        db_session,
        key,
        user_id,
        guild_id,
        include_user_global=False
    )
    for tag in existing_tags:
        if tag.owner_type == db.TagOwnerType.BOT:
            reserved_types |= TagType.BOT
        if tag.owner_type == db.TagOwnerType.USER:
            reserved_types |= TagType.USER
        if tag.owner_type == db.TagOwnerType.GUILD:
            reserved_types |= TagType.GUILD
    return reserved_types


class TagNameModal(ErrorHandlerModal, title='Vaihda tagin nimeä'):
    name = ui.TextInput(label='Tagin nimi', min_length=1, max_length=1000)

    def __init__(self, editor: "TagEditor"):
        super().__init__()
        self.editor = editor
        self.name.placeholder = editor.tag.key

    async def on_submit(self, interaction: discord.Interaction) -> None:
        self.editor.set_name(self.name.value)
        await self.editor.update_message(interaction)


class TagContentModal(ErrorHandlerModal, title='Aseta tagin sisältö'):
    content = ui.TextInput(label='Tagin sisältö', style=discord.TextStyle.long, max_length=2000, min_length=1)

    def __init__(self, editor: "TagEditor"):
        super().__init__()
        self.editor = editor

    async def on_submit(self, interaction: discord.Interaction) -> None:
        self.editor.set_content(self.content.value)
        await self.editor.update_message(interaction)


class TagEditor(ViewWithDB, RestrictedView):
    class State(enum.Enum):
        SELECT_TYPE = enum.auto()
        CONTENT = enum.auto()
        CONFIRM = enum.auto()
        DONE = enum.auto()

    def __init__(
            self,
            bot,
            allowed_types: TagType,
            reserved_types: TagType,
            tag: db.Tag,
            allowed_user: discord.User,
            orig_interaction: discord.Interaction
    ):
        super().__init__(bot=bot, allowed_user=allowed_user)

        self.allowed_user = allowed_user
        self.tag = tag
        self.allowed_types = allowed_types
        self.reserved_types = reserved_types
        self._state = TagEditor.State.SELECT_TYPE
        self.orig_interaction = orig_interaction
        if len(allowed_types) == 1:
            self._state = TagEditor.State.CONTENT

        self._update_buttons()

    def _update_buttons(self):
        self.clear_items()

        if self._state == TagEditor.State.CONFIRM:
            self.add_item(self.confirm)
            self.add_item(self.cancel)

        if self._state == TagEditor.State.SELECT_TYPE:
            if len(self.allowed_types) <= 1:
                raise ValueError('At least two buttons should be specified!')
            if TagType.USER in self.allowed_types:
                self.user_button.disabled = TagType.USER in self.reserved_types
                self.add_item(self.user_button)
            if TagType.GUILD in self.allowed_types:
                self.guild_button.disabled = TagType.GUILD in self.reserved_types
                self.add_item(self.guild_button)
            if TagType.BOT in self.allowed_types:
                self.bot_button.disabled = TagType.BOT in self.reserved_types
                self.add_item(self.bot_button)

        if self._state == TagEditor.State.CONTENT or self.tag.content is not None:
            if self._state == TagEditor.State.CONTENT:
                self.change_content.style = discord.ButtonStyle.blurple
            else:
                self.change_content.style = discord.ButtonStyle.grey
            self.add_item(self.change_content)

        if len(self.allowed_types) > 1 and self._state != TagEditor.State.SELECT_TYPE:
            self.add_item(self.change_type)

        self.add_item(self.change_name)

    def set_name(self, name: str):
        self.tag.key = name

    def set_content(self, content: str):
        self.tag.content = content
        if self._state == TagEditor.State.CONTENT:
            self._state = TagEditor.State.CONFIRM

    def message_content(self) -> str:
        if self._state == TagEditor.State.DONE:
            title = 'Tagin tiedot'
        else:
            title = 'Tagin luonti'
        content = f'# {title}\n**Nimi:** {self.tag.key}'

        if (
                self._state != TagEditor.State.SELECT_TYPE and
                self.tag.owner_type is not None and
                len(self.allowed_types) > 1
        ):
            content += f'\n**Tyyppi:** {self.tag.owner_type.translate()}'
        if self.tag.content is not None:
            content += f'\n**Sisältö:**\n'
            if len(self.tag.content) > 1003:
                content += f'{self.tag.content[:1000]}...'
            else:
                content += self.tag.content

        if self._state != TagEditor.State.DONE:
            content += '\n\n'

        if self._state == TagEditor.State.SELECT_TYPE:
            content += '**Valitse tagin tyyppi (disabloitujen nappien tyypit on jo varattu):**'
        elif self._state == TagEditor.State.CONTENT:
            content += '**Syötä tagin sisältö painamalla nappia:**'
        elif self._state == TagEditor.State.CONFIRM:
            content += '**Oletko varma, että haluat luoda tagin?**'
        return content

    async def update_message(self, interaction: discord.Interaction):
        self._update_buttons()
        await interaction.response.edit_message(
            content=self.message_content(),
            view=self,
            allowed_mentions=discord.AllowedMentions.none()
        )

    async def on_timeout(self) -> None:
        await self.orig_interaction.edit_original_response(content='Tagin luonti peruttu.', view=None)
        self.stop()

    @ui.button(label='Käyttäjä', style=discord.ButtonStyle.blurple)
    async def user_button(self, interaction: discord.Interaction, button: ui.Button):
        self.tag.owner = interaction.user.id
        self.tag.owner_type = db.TagOwnerType.USER
        if self.tag.content is None:
            self._state = TagEditor.State.CONTENT
        else:
            self._state = TagEditor.State.CONFIRM
        await self.update_message(interaction)

    @ui.button(label='Palvelin', style=discord.ButtonStyle.blurple)
    async def guild_button(self, interaction: discord.Interaction, button: ui.Button):
        self.tag.owner = interaction.guild_id
        self.tag.owner_type = db.TagOwnerType.GUILD
        if self.tag.content is None:
            self._state = TagEditor.State.CONTENT
        else:
            self._state = TagEditor.State.CONFIRM
        await self.update_message(interaction)

    @ui.button(label='Botti', style=discord.ButtonStyle.blurple)
    async def bot_button(self, interaction: discord.Interaction, button: ui.Button):
        self.tag.owner = interaction.client.user.id
        self.tag.owner_type = db.TagOwnerType.BOT
        if self.tag.content is None:
            self._state = TagEditor.State.CONTENT
        else:
            self._state = TagEditor.State.CONFIRM
        await self.update_message(interaction)

    @ui.button(label='Muuta nimeä', style=discord.ButtonStyle.grey)
    async def change_name(self, interaction: discord.Interaction, button: ui.Button):
        await interaction.response.send_modal(TagNameModal(self))

    @ui.button(label='Aseta sisältö')
    async def change_content(self, interaction: discord.Interaction, button: ui.Button):
        await interaction.response.send_modal(TagContentModal(self))

    @ui.button(label='Luo tagi', style=discord.ButtonStyle.green)
    async def confirm(self, interaction: discord.Interaction, button: ui.Button):
        if self.tag.owner is None:
            self.tag.owner = interaction.user.id
            self.tag.owner_type = db.TagOwnerType.USER
        async with interaction.db_session.begin():
            if not await self.tag.is_available(interaction.db_session):
                await interaction.response.send_message(
                    'Tagin nimi tai tyyppi ei ole saatavilla! '
                    'Muuta tagin tietoja ja yritä uudelleen.',
                    ephemeral=True,
                )
                return
            interaction.db_session.add(self.tag)
        await interaction.db_session.commit()
        self._state = TagEditor.State.DONE
        await interaction.response.edit_message(content=self.message_content(), view=None)
        self.stop()

    @ui.button(label='Muuta tyyppiä', style=discord.ButtonStyle.grey)
    async def change_type(self, interaction: discord.Interaction, button: ui.Button):
        async with interaction.db_session.begin():
            self.reserved_types = await get_reserved_types(
                interaction.db_session,
                self.tag.key,
                interaction.user.id,
                interaction.guild_id,
            )
        if TagType.BOT in self.reserved_types or (self.allowed_types & self.reserved_types) == self.allowed_types:
            await interaction.response.send_message('Tagin nimi on jo varattu! Muuta nimeä.', ephemeral=True)
            return
        self._state = TagEditor.State.SELECT_TYPE
        self.tag.owner_type = None
        await self.update_message(interaction)

    @ui.button(label='Peruuta', style=discord.ButtonStyle.red)
    async def cancel(self, interaction: discord.Interaction, button: ui.Button):
        await interaction.response.edit_message(content='Tagin luonti peruttu.', view=None)
        self.stop()


class TagSelector(ViewWithDB, RestrictedView):
    def __init__(
            self,
            bot,
            allowed_user: discord.User,
            orig_interaction: discord.Interaction,
            user_tag: db.Tag,
            user_global_tag: db.Tag,
            guild_tag: db.Tag,
            bot_tag: db.Tag,
            operation: Callable[[discord.Interaction, db.Tag], Coroutine],
    ):
        super().__init__(bot=bot, allowed_user=allowed_user)
        self.user_tag = user_tag
        self.user_global_tag = user_global_tag
        self.guild_tag = guild_tag
        self.bot_tag = bot_tag

        self.orig_interaction = orig_interaction
        self.message = None

        self.clear_items()

        if self.user_tag is not None:
            self.add_item(self.user)
        if self.user_global_tag is not None and (
                user_tag is None or (user_tag is not None and user_tag != user_global_tag)
        ):
            self.add_item(self.user_global)
        if self.guild_tag is not None:
            self.add_item(self.guild)
        if self.bot_tag is not None:
            self.add_item(self.bot_btn)

        self.operation = operation

    async def on_timeout(self) -> None:
        if self.orig_interaction is not None:
            await self.orig_interaction.edit_original_response(content='Tagin tyyppiä ei valittu.', view=None)
        elif self.message is not None:
            await self.message.edit(
                content='Tagin tyyppiä ei valittu.',
                view=None,
                allowed_mentions=discord.AllowedMentions.none()
            )
        self.stop()

    @ui.button(label='Oma', style=discord.ButtonStyle.blurple)
    async def user(self, interaction: discord.Interaction, button: ui.Button):
        await self.operation(interaction, self.user_tag)
        self.stop()

    @ui.button(label='Globaali', style=discord.ButtonStyle.blurple)
    async def user_global(self, interaction: discord.Interaction, button: ui.Button):
        await self.operation(interaction, self.user_global_tag)
        self.stop()

    @ui.button(label='Palvelin', style=discord.ButtonStyle.blurple)
    async def guild(self, interaction: discord.Interaction, button: ui.Button):
        await self.operation(interaction, self.guild_tag)
        self.stop()

    @ui.button(label='Botti', style=discord.ButtonStyle.blurple)
    async def bot_btn(self, interaction: discord.Interaction, button: ui.Button):
        await self.operation(interaction, self.bot_tag)
        self.stop()


class TagList(ButtonPaginator):
    def __init__(self, tag_strings: list[str], allowed_user: discord.User, display_abbrevs: bool = False, **kwargs):
        data = []
        current_str = tag_strings.pop(0)

        for tag_str in tag_strings:
            # Embed description character limit is 4096
            if len(current_str) + len(tag_str) + 1 > 4096:
                data.append(current_str)
                current_str = tag_str
            else:
                current_str += f'\n{tag_str}'

        data.append(current_str)
        self.display_abbrevs = display_abbrevs

        super().__init__(data, allowed_user, reverse=False, can_fetch_more=False, **kwargs)

    def build_embed(self, element: str) -> discord.Embed:
        embed = discord.Embed(title='Omistamasi tagit', colour=discord.Color.green())
        embed.description = element
        if self.display_abbrevs:
            embed.set_footer(text='U - Käyttäjä, G - Palvelin (Guild), B - Botti')
        return embed
