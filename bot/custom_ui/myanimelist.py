import discord

import processors
from .paginator import ButtonPaginator


_MAL_COLOR = discord.Colour.from_str('#2e51a2')


class AnimePaginator(ButtonPaginator):
    def build_embed(self, anime: processors.myanimelist.AnimeForList) -> discord.Embed:
        embed = discord.Embed(
            colour=_MAL_COLOR,
            title=anime.title,
            url=f'https://myanimelist.net/anime/{anime.id}',
            description=anime.synopsis,
        ).set_footer(text=f'Tulos {self.index + 1}/{len(self.data)} - Lähde: myanimelist.net')

        if anime.status is not None:
            if anime.status in (processors.Status.ANIME_ONGOING, processors.Status.ANIME_UPCOMING):
                # Hourglass not done
                emoj = '\u23F3'
            else:
                # Hourglass done
                emoj = '\u231B'
            embed.add_field(name=f'{emoj} Lähetykset', value=processors.translate_anime_enum(anime.status), inline=True)
        if anime.media_type is not None:
            # :dividers:
            embed.add_field(
                name='\U0001F5C2 Tyyppi',
                value=processors.translate_anime_enum(anime.media_type),
                inline=True
            )
        if anime.genres is not None:
            # Clapper
            embed.add_field(
                name='\U0001F3AC Lajityypit',
                value=', '.join(genre.name for genre in anime.genres),
                inline=False
            )
        if anime.start_date is not None:
            # Calendar (spiral)
            date = f'**{anime.start_date}**'
            if anime.end_date is None:
                date += '-**??**'
            elif anime.start_date != anime.end_date:
                date += f'-**{anime.end_date}**'
            embed.add_field(name='\U0001F5D3 Näytetty', value=date, inline=False)
        if anime.num_episodes is not None:
            # Minidisc / computer disc
            embed.add_field(
                name='\U0001F4BD Jaksoja',
                value='??' if anime.num_episodes == 0 else str(anime.num_episodes),
                inline=True
            )
        if anime.average_episode_duration is not None:
            # Stopwatch
            embed.add_field(
                name='\u23F1 Jakson pituus',
                value=f'{anime.average_episode_duration / 60:.0f} min',
                inline=True
            )
        if anime.mean is not None:
            # Star
            embed.add_field(name='\u2B50 Arvostelujen keskiarvo', value=f'**{anime.mean:.2f}/10**', inline=True)
        if anime.rank is not None:
            # Trophy
            embed.add_field(name='\U0001F3C6 Sijoitus', value=f'**{anime.rank}**', inline=True)
        if anime.main_picture is not None:
            embed.set_thumbnail(
                url=anime.main_picture.large if anime.main_picture.large is not None else anime.main_picture.medium
            )
        if anime.studios is not None:
            embed.set_author(
                name=', '.join(studio.name for studio in anime.studios),
                url=f'https://myanimelist.net/anime/producer/{anime.studios[0].id}' if len(anime.studios) == 1 else None
            )
        return embed


class MangaPaginator(ButtonPaginator):
    def build_embed(self, manga: processors.myanimelist.MangaForList) -> discord.Embed:
        embed = discord.Embed(
            colour=_MAL_COLOR,
            title=manga.title,
            url=f'https://myanimelist.net/manga/{manga.id}',
            description=manga.synopsis,
        ).set_footer(text=f'Tulos {self.index + 1}/{len(self.data)} - Lähde: myanimelist.net')
        if manga.status is not None:
            if manga.status in (processors.Status.MANGA_DISCONTINUED, processors.Status.MANGA_FINISHED):
                # Hourglass done
                emoji = '\u231B'
            else:
                # Hourglass not done
                emoji = '\u23F3'
            embed.add_field(name=f'{emoji} Julkaisu', value=processors.translate_anime_enum(manga.status), inline=True)
        if manga.media_type is not None:
            # :dividers:
            embed.add_field(
                name='\U0001F5C2 Tyyppi',
                value=processors.translate_anime_enum(manga.media_type),
                inline=True
            )
        if manga.genres is not None:
            # Clapper
            embed.add_field(
                name='\U0001F3AC Lajityypit',
                value=', '.join(genre.name for genre in manga.genres),
                inline=False
            )
        if manga.start_date is not None:
            # Calendar (spiral)
            date = f'**{manga.start_date}**'
            if manga.end_date is None:
                date += '-**??**'
            elif manga.start_date != manga.end_date:
                date += f'-**{manga.end_date}**'
            embed.add_field(name='\U0001F5D3 Julkaistu', value=date, inline=False)
        if manga.num_volumes is not None:
            # Books
            embed.add_field(
                name='\U0001F4DA Niteitä',
                value='??' if manga.num_volumes == 0 else str(manga.num_volumes),
                inline=True
            )
        if manga.num_chapters is not None:
            # Bookmark tabs
            embed.add_field(
                name='\U0001F4D1 Lukuja',
                value='??' if manga.num_chapters == 0 else str(manga.num_chapters),
                inline=True
            )
        if manga.mean is not None:
            # Star
            embed.add_field(name='\u2B50 Arvostelujen keskiarvo', value=f'**{manga.mean:.2f}/10**', inline=True)
        if manga.rank is not None:
            # Trophy
            embed.add_field(name='\U0001F3C6 Sijoitus', value=f'**{manga.rank}**', inline=True)
        if manga.main_picture is not None:
            embed.set_thumbnail(
                url=manga.main_picture.large if manga.main_picture.large is not None else manga.main_picture.medium
            )
        if manga.authors is not None:
            author_names = []
            for author in manga.authors:
                person = author.node
                name = person.last_name
                if person.first_name is not None and len(person.first_name) > 0:
                    name += f', {person.first_name}'
                author_names.append(name)
            embed.set_author(
                name='; '.join(author_names),
                url=f'https://myanimelist.net/people/{manga.authors[0].node.id}' if len(manga.authors) == 1 else None
            )
        return embed
