import discord
from aiohttp import ClientResponseError
from discord import ui

import processors
from .base import RestrictedView, ErrorHandlerModal

_YLE_COLOR = discord.Colour.from_str('#00b4c8')


class TeletextPageModal(ErrorHandlerModal, title='Siirry sivulle'):
    page = ui.TextInput(label='Sivun numero', max_length=3, min_length=3)

    def __init__(self, callback):
        super().__init__()
        self._callback = callback

    async def on_submit(self, interaction: discord.Interaction, /) -> None:
        if not all(c.isdigit() for c in self.page.value):
            await interaction.response.send_message('Anna numero!', ephemeral=True)
            return
        page = int(self.page.value)
        if page < 100:
            await interaction.response.send_message('Numeron pitää olla välillä 100-999.', ephemeral=True)
            return
        if not await self._callback(page, interaction):
            return
        await interaction.response.defer()


class TeletextPager(RestrictedView):
    def __init__(self, allowed_user: discord.User, client: processors.YleClient, page_info: dict):
        super().__init__(allowed_user, disallow_message='Vain {allowed_user.mention} saa vaihtaa sivua!', timeout=300)
        self._client = client
        self._page_info = page_info
        self._current_subpage = 1
        self.message = None
        self._update_buttons()

    async def on_timeout(self) -> None:
        try:
            image_data = await self._client.page_image(self.page_number, self._current_subpage)
        except ClientResponseError:
            await self.message.edit(
                embed=self.build_embed(),
                view=None
            )
            return
        image_data.seek(0)
        await self.message.edit(
            embed=self.build_embed(),
            attachments=[discord.File(image_data, filename='teksti-tv.png')],
            view=None
        )

    def _update_buttons(self):
        self.prev_subpage.disabled = self.next_subpage.disabled = (self.subpage_count == 1)

        if self.nextpg is None:
            self.next_page.disabled = True
            self.next_page.label = '⟩'
        else:
            self.next_page.disabled = False
            self.next_page.label = f'{self.nextpg} ⟩'

        if self.prevpg is None:
            self.prev_page.disabled = True
            self.prev_page.label = '⟨'
        else:
            self.prev_page.disabled = False
            self.prev_page.label = f'⟨ {self.prevpg}'

    def build_embed(self) -> discord.Embed:
        title = f'{self.page_number}'
        if self.page_name is not None:
            title += f' | {self.page_name}'
        return discord.Embed(
            title=title,
            colour=_YLE_COLOR,
            url=f'https://yle.fi/aihe/tekstitv?P={self.page_number}#{self._current_subpage}'
        ).set_footer(
            text=f'Alisivu {self._current_subpage}/{self.subpage_count}'
        ).set_image(
            url='attachment://teksti-tv.png'
        ).set_author(
            name='Yle Teksti-TV',
            url='https://yle.fi',
        )

    async def _handle_clientresponseerror(self, e: ClientResponseError, interaction: discord.Interaction, page_number):
        if e.status == 401:
            await interaction.response.send_message(
                'Teksti-TV:n sivuja pyydetty liian nopeasti, yritä uudelleen hetken kuluttua.',
                ephemeral=True
            )
            return False
        elif e.status == 403:
            await interaction.response.send_message('Teksti-TV:n konfiguraatiossa on jotain pielessä!', ephemeral=True)
            return False
        elif e.status == 404:
            await interaction.response.send_message(f'Teksti-TV:n sivua {page_number} ei ole olemassa!', ephemeral=True)
            return False
        raise e

    async def _update_page_data(self, page: int, interaction: discord.Interaction) -> bool:
        try:
            self._page_info = await self._client.page_info(page)
        except ClientResponseError as e:
            await self._handle_clientresponseerror(e, interaction, page)
            return False
        self._current_subpage = 1
        return True

    async def _update_message(self, interaction: discord.Interaction) -> bool:
        self._update_buttons()
        try:
            image_data = await self._client.page_image(self.page_number, self._current_subpage)
        except ClientResponseError as e:
            await self._handle_clientresponseerror(e, interaction, self.page_number)
            return False
        image_data.seek(0)
        await interaction.response.edit_message(
            embed=self.build_embed(),
            view=self,
            attachments=[discord.File(image_data, filename='teksti-tv.png')]
        )
        return True

    async def _modal_callback(self, page: int, interaction: discord.Interaction) -> bool:
        if not await self._update_page_data(page, interaction):
            return False
        self._update_buttons()
        try:
            image_data = await self._client.page_image(self.page_number, self._current_subpage)
        except ClientResponseError as e:
            await self._handle_clientresponseerror(e, interaction, page)
            return False
        image_data.seek(0)
        await self.message.edit(
            embed=self.build_embed(),
            view=self,
            attachments=[discord.File(image_data, filename='teksti-tv.png')]
        )
        return True

    @ui.button(label='⟨', style=discord.ButtonStyle.blurple)
    async def prev_page(self, interaction: discord.Interaction, button: ui.Button):
        if not await self._update_page_data(self.prevpg, interaction):
            return
        await self._update_message(interaction)

    @ui.button(label='<', style=discord.ButtonStyle.gray)
    async def prev_subpage(self, interaction: discord.Interaction, button: ui.Button):
        self._current_subpage -= 1
        if self._current_subpage < 1:
            self._current_subpage = self.subpage_count
        await self._update_message(interaction)

    @ui.button(label='Sivu...', style=discord.ButtonStyle.gray)
    async def select_page(self, interaction: discord.Interaction, button: ui.Button):
        await interaction.response.send_modal(TeletextPageModal(self._modal_callback))

    @ui.button(label='>', style=discord.ButtonStyle.gray)
    async def next_subpage(self, interaction: discord.Interaction, button: ui.Button):
        self._current_subpage += 1
        if self._current_subpage > self.subpage_count:
            self._current_subpage = 1
        await self._update_message(interaction)

    @ui.button(label='⟩', style=discord.ButtonStyle.blurple)
    async def next_page(self, interaction: discord.Interaction, button: ui.Button):
        if not await self._update_page_data(self.nextpg, interaction):
            return
        await self._update_message(interaction)

    @property
    def subpage_count(self) -> int:
        return int(self._page_info['teletext']['page']['subpagecount'])

    @property
    def nextpg(self):
        page_info = self._page_info['teletext']['page']
        return page_info.get('nextpg')

    @property
    def prevpg(self):
        page_info = self._page_info['teletext']['page']
        return page_info.get('prevpg')

    @property
    def page_number(self):
        return int(self._page_info['teletext']['page']['number'])

    @property
    def page_name(self):
        page_info = self._page_info['teletext']['page']
        if len(page_info['name'].strip()) == 0:
            return None
        return page_info['name']
