import logging
from typing import Optional, Any

import discord
from discord import ui


class ErrorHandlerView(ui.View):
    def __init__(self, logger=None, **kwargs):
        super().__init__(**kwargs)
        self.logger = logger or logging.getLogger('bot')

    async def on_error(self, interaction: discord.Interaction, error: Exception, item: ui.Item[Any]) -> None:
        self.logger.error(f'Error in View {self} with item {item}', exc_info=error)
        await interaction.response.send_message('Tapahtui odottamaton virhe!', ephemeral=True)


class RestrictedView(ErrorHandlerView):
    def __init__(self, allowed_user: Optional[discord.User], disallow_message: str = None, **kwargs):
        super().__init__(**kwargs)
        self._allowed_user = allowed_user
        self._disallow_message = disallow_message or 'Vain {allowed_user.mention} saa käyttää näitä nappeja!'

    async def interaction_check(self, interaction: discord.Interaction, /) -> bool:
        if self._allowed_user is not None and interaction.user != self._allowed_user:
            await interaction.response.send_message(
                self._disallow_message.format(allowed_user=self._allowed_user),
                ephemeral=True
            )
            return False
        return True


class ErrorHandlerModal(ui.Modal):
    def __init__(self, logger=None):
        super().__init__()
        self.logger = logger or logging.getLogger('bot')

    async def on_error(self, interaction: discord.Interaction, error: Exception) -> None:
        self.logger.error(f'Error in Modal {self}', exc_info=error)
        await interaction.response.send_message('Tapahtui odottamaton virhe!', ephemeral=True)
