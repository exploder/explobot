import aiohttp
import time
import asyncio

from processors.models import OWMWeather


OWM_API_URL = 'https://api.openweathermap.org/data/2.5'
OWM_FORECAST_API = 'forecast'
OWM_CURRENT_WEATHER_API = 'weather'
_last_call = 0


async def _owm_call(api_key: str, function: str, query: str,
                    session: aiohttp.ClientSession):
    global _last_call
    url = f'{OWM_API_URL}/{function}'
    get_params = {
        'appid': api_key,
        'q': query,
        'lang': 'fi',
        'units': 'metric'
    }

    # Some rate limiting
    current_time = time.time()
    if current_time - _last_call < 1:
        await asyncio.sleep(1.0 - (current_time - _last_call))

    async with session.get(url, params=get_params) as resp:
        _last_call = time.time()
        resp.raise_for_status()
        return OWMWeather(await resp.json())


async def owm_get_current_weather(api_key: str, query: str,
                                  session: aiohttp.ClientSession):
    return await _owm_call(api_key, OWM_CURRENT_WEATHER_API, query, session)


async def owm_get_forecast(api_key: str, query: str,
                           session: aiohttp.ClientSession):
    return await _owm_call(api_key, OWM_FORECAST_API, query, session)
