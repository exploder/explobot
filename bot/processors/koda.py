from io import BytesIO

from PIL import Image, ImageDraw, ImageFont

from processors.util import get_font_sized_for_text


LINE_Y = 285
TITLE_CENTER_Y = 122
SUB_CENTER_Y = 309
SIGNATURE_LINE_Y = 400
SIGNATURE_LINE_CENTER_X = 463

TEMPLATE_DIMENSIONS = (626, 475)

NAME_FONT_SIZE = 50
TITLE_FONT_SIZE = 40
SUB_FONT_SIZE = 27
SIGNATURE_FONT_SIZE = 17

ITC_FONT_PATH = 'files/itc.ttf'
BRUSSELS_FONT_PATH = 'files/bruh.ttf'


koda_template = Image.open('files/koda_moment.png').convert('RGBA')
empty_template = Image.open('files/moment_template.png').convert('RGBA')


def put_certified_name_and_server(
        who: str,
        server_name: str,
        cert_image: Image = None
) -> BytesIO:
    who = who.upper()

    if cert_image is None:
        certificate = koda_template.copy()
    else:
        certificate = cert_image

    draw = ImageDraw.Draw(certificate)
    font = get_font_sized_for_text(
        ITC_FONT_PATH,
        who,
        draw,
        NAME_FONT_SIZE,
        TEMPLATE_DIMENSIONS[0]
    )

    text_width = draw.textlength(who, font=font)
    text_x = TEMPLATE_DIMENSIONS[0] / 2 - text_width / 2

    draw.text(
        (text_x, LINE_Y - font.size),
        who,
        fill=(0, 0, 0, 255),
        font=font,
        align='left'
    )

    font = ImageFont.truetype(BRUSSELS_FONT_PATH, SIGNATURE_FONT_SIZE)

    draw.text(
        (SIGNATURE_LINE_CENTER_X, SIGNATURE_LINE_Y),
        server_name,
        fill=(0, 0, 0, 255),
        font=font,
        anchor='md'
    )

    final = BytesIO()
    certificate.save(final, format='PNG', optimize=True)
    return final


def create_full_certificate(
        template_who: str,
        certified_who: str,
        server_name: str
) -> BytesIO:
    certificate = empty_template.copy()
    template_who = template_who.upper()
    title = f'{template_who} MOMENT'
    draw = ImageDraw.Draw(certificate)

    font = get_font_sized_for_text(
        ITC_FONT_PATH,
        title,
        draw,
        TITLE_FONT_SIZE,
        TEMPLATE_DIMENSIONS[0]
    )

    # The height measurement is somehow fucked (it produces ~30 % too high
    # values), so the text needs to be moved up 15 % of the calculated
    # height so that it appears to be centered vertically
    draw.text(
        (TEMPLATE_DIMENSIONS[0] / 2, TITLE_CENTER_Y),
        title,
        fill=(0, 0, 0, 255),
        font=font,
        anchor='mm'
    )

    sub = f'WENT FULL {template_who}'

    font = get_font_sized_for_text(
        ITC_FONT_PATH,
        sub,
        draw,
        SUB_FONT_SIZE,
        TEMPLATE_DIMENSIONS[0]
    )

    draw.text(
        (TEMPLATE_DIMENSIONS[0] / 2, SUB_CENTER_Y),
        sub,
        fill=(0, 0, 0, 255),
        font=font,
        anchor='mm'
    )

    return put_certified_name_and_server(
        certified_who,
        server_name,
        certificate
    )
