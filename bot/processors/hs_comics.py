import asyncio
import enum

from aiohttp import ClientSession
from lxml import html


_HS_MORE_ITEMS_URL = 'https://www.hs.fi/rest/laneitems/39221/moreItems'


class ComicID(enum.Enum):
    FINGERPORI = 290
    VIIVI_JA_WAGNER = 291
    WUMO = 293
    JARLA = 2461
    FOK_IT = 295


async def get_comic_data(session: ClientSession, comic_id: ComicID, start: int = 0):
    async with session.get(_HS_MORE_ITEMS_URL, params={'from': start, 'pageId': comic_id.value}) as resp:
        resp.raise_for_status()
        content = await resp.text()

    tree = html.fromstring(content)
    titles = tree.xpath('//span[@class="title"]/text()')
    dates = tree.xpath('//span[@class="date"]/text()')
    img_urls = [f'https:{url.split()[0]}' for url in tree.xpath('//img/@data-srcset')]
    hs_urls = [f'https://www.hs.fi{url}' for url in tree.xpath('//li/div/a/@href')]
    copyrights = tree.xpath('//figcaption/text()')

    keys = ('title', 'date', 'img_url', 'hs_url', 'copyright')
    comics = [
        dict(
            zip(
                keys,
                (title, date, img_url, hs_url, copyr)
            )
        ) for title, date, img_url, hs_url, copyr in zip(titles, dates, img_urls, hs_urls, copyrights)
    ]
    return comics
