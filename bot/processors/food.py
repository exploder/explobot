import random
import typing

import aiosqlite


RECOMMENDATIONS = {
    # Source: https://www.ruokavirasto.fi/globalassets/teemat/terveytta-edistava-ruokavalio/kuluttaja-ja-ammattilaismateriaali/julkaisut/ravitsemussuositukset_2014_fi_web_versio_5.pdf
    'VITA':     900,   # µg
    'VITD':     10,    # µg
    'VITE':     10,    # mg
    'THIA':     1.4,   # mg
    'RIBF':     1.6,   # mg
    'NIAEQ':    19,    # NE
    'VITPYRID': 1.5,   # mg (B6)
    'FOL':      300,   # µg
    'VITB12':   2,     # µg
    'VITC':     75,    # mg
    'CA':       900,   # mg
    'P':        700,   # mg
    'K':        3500,  # mg
    'MG':       350,   # mg
    'FE':       9,     # mg
    'ZN':       9,     # mg
    'CU':       0.9,   # mg
    'ID':       150,   # µg
    'SE':       60,    # µg
    # Source: https://eur-lex.europa.eu/legal-content/FI/TXT/PDF/?uri=CELEX:02011R1169-20180101&qid=1543681930845&from=FI
    'VITK':     75,    # µg
    'MN':       2,     # mg
    'CR':       40,    # µg
    'MO':       0.05,  # mg
    'FAT':      70,    # g
    'CHOAVL':   260,   # g
    'SUGAR':    90,    # g
    'PROT':     50,    # g
    'NACL':     5000,  # mg
}


async def get_random_food_daily_nutrient() -> typing.Tuple[int, str, float]:
    nutrient, recommendation = random.choice(list(RECOMMENDATIONS.items()))
    async with aiosqlite.connect('files/food.db') as db:
        async with db.execute(
                'SELECT * FROM component_value WHERE eufdname=?', (nutrient,)
        ) as cursor:
            rows = await cursor.fetchall()
    foodid, _, value = random.choice(rows)
    return foodid, nutrient, recommendation / value * 100


async def get_random_nutrient_dose(
        foodid: int,
        previous_nutrient: str,
        mass: float
) -> typing.Tuple[str, float]:
    hundreds_grams = mass / 100
    food_ok = False
    async with aiosqlite.connect('files/food.db') as db:
        while not food_ok:
            async with db.execute(
                'SELECT * FROM component_value WHERE foodid=? AND eufdname!=?',
                (foodid, previous_nutrient)
            ) as cursor:
                rows = await cursor.fetchall()
            food_ok = any(
                [nut in RECOMMENDATIONS for nut in [row[1] for row in rows]]
            )
    nutrient = None
    while nutrient not in RECOMMENDATIONS:
        row = random.choice(rows)
        nutrient = row[1]
    nutrient_amount = hundreds_grams * row[2]
    return nutrient, nutrient_amount / RECOMMENDATIONS[nutrient]


async def get_food_name(foodid: int) -> str:
    async with aiosqlite.connect('files/food.db') as db:
        async with db.execute(
                'SELECT foodname FROM food WHERE foodid=?',
                (foodid,)
        ) as cursor:
            row = await cursor.fetchone()
    return row[0]
