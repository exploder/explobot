import logging
from typing import Union
from io import BytesIO
from functools import partial
from urllib.parse import ParseResult, urlparse

import discord
from discord.ext import commands
from rlottie_python import LottieAnimation

import processors
from util import CustomContext
from processors.util import extract_image_url_from_metadata


class Url(commands.Converter):
    async def convert(self, ctx: commands.Context, argument: str):
        try:
            parsed = urlparse(argument)
            if parsed.scheme in ('http', 'https') and parsed.netloc != '':
                return parsed
            else:
                raise commands.BadArgument(f'Incorrect url: \'{argument}\'')
        except ValueError as e:
            raise commands.BadArgument(f'Failed to parse url \'{argument}\'') \
                from e


class PreviousImage:
    @classmethod
    async def convert(cls, ctx: commands.Context, argument: str):
        if argument == '^':
            return cls()
        else:
            raise commands.BadArgument(f'Incorrect previous image: \'{argument}\'')


# Chunk size when reading http request data
CHUNK_SIZE = 1024


async def _download_image(
        ctx: CustomContext,
        url: str, image_bytes: BytesIO,
        complain: bool = True,
        recurse: bool = True
):
    async def _complain(msg: str):
        if complain:
            await ctx.rsend(msg)

    async with ctx.bot.session.get(url) as resp:
        if 'content-type' not in resp.headers:
            await _complain('Linkkisi ei kelpaa!')
            raise RuntimeError('Link does not include content-type')

        content_type = resp.headers['content-type']

        if not content_type.startswith('image'):
            if 'text/html' not in content_type:
                await _complain('Linkin takaa ei löydy kuvaa!')
                raise RuntimeError(f'Content-Type {content_type} not an image!')
            # Support links that do not go directly to the image, such as Tenor
            image_links = extract_image_url_from_metadata(await resp.text())
            if len(image_links) == 0:
                await _complain('Linkin takaa ei löydy kuvaa!')
                raise RuntimeError('No image found from og:image')
            if recurse:
                await _download_image(ctx, image_links[0], image_bytes, complain, False)
                return
            else:
                await _complain('Linkin takaa ei löydy kuvaa!')
                raise RuntimeError('Maximum recursion depth reached!')

        if 'Content-Length' not in resp.headers:
            await _complain('Linkkisi ei kelpaa!')
            raise RuntimeError('Link does not include content-length')

        size = int(resp.headers['Content-Length'])

        if size > 10 * (1024 ** 2):  # Image must be max 10 MB
            await _complain('Max 10 MB linkkejä kiitos!')
            raise RuntimeError('Too big image!')

        while True:
            chunk = await resp.content.read(CHUNK_SIZE)
            if not chunk:
                break
            image_bytes.write(chunk)


async def _try_message(
        ctx: CustomContext,
        message: discord.Message,
        image_bytes: BytesIO,
        complain: bool = True
) -> bool:
    if len(message.attachments) > 0:
        maybe_image = message.attachments[0]
        if maybe_image.content_type is not None and maybe_image.content_type.startswith('image'):
            # This is an image
            await maybe_image.save(image_bytes)
            return True

    if len(message.stickers) > 0:
        sticker = message.stickers[0]
        if sticker.format == discord.StickerFormatType.lottie:
            async with ctx.bot.session.get(sticker.url) as resp:
                data = await resp.text()
            out_bytes = await ctx.bot.loop.run_in_executor(ctx.bot.executor, partial(_lottie_to_apng, lottie_json=data))
            image_bytes.write(out_bytes.read())
            out_bytes.close()
        else:
            await sticker.save(image_bytes)
        return True

    for embed in message.embeds:
        if embed.type in ('image', 'gifv'):
            try:
                await _download_image(ctx, embed.url, image_bytes, complain)
                return True
            except RuntimeError:
                # Quite a bruh moment to use a logger like this but idc
                logging.getLogger('bot').debug('Failed to download image from embed', exc_info=True)

    if ' ' not in message.content:
        # Try it as a link
        converter = Url()
        try:
            url = await converter.convert(ctx, message.content)
        except commands.BadArgument:
            logging.getLogger('bot').debug('Failed to convert URL', exc_info=True)
            return False
        try:
            await _download_image(ctx, url.geturl(), image_bytes, complain)
            return True
        except RuntimeError:
            # Quite a bruh moment to use a logger like this but idc
            logging.getLogger('bot').debug('Failed to download image from embed', exc_info=True)

    return False


def _lottie_to_apng(lottie_json: str) -> BytesIO:
    anim = LottieAnimation.from_data(lottie_json)
    image_list = []

    frame_count = anim.lottie_animation_get_totalframe()
    width, height = anim.lottie_animation_get_size()
    framerate = anim.lottie_animation_get_framerate()

    for i in range(frame_count):
        image_list.append(
            anim.render_pillow_frame(
                frame_num=i,
                width=width,
                height=height
            ).copy()
        )

    out_bytes = BytesIO()
    image_list[0].save(
        out_bytes,
        format='PNG',
        save_all=True,
        append_images=image_list[1:],
        duration=int(1000 / framerate)
    )
    out_bytes.seek(0)
    return out_bytes


async def get_image(
        ctx: CustomContext,
        target: Union[discord.Member, discord.User, Url, PreviousImage] = None,
        return_info: bool = False,
        image_bytes: BytesIO = None
):
    if target is None:
        if len(ctx.message.attachments) > 0:
            target = ctx.message.attachments[0]
        elif ctx.message.reference is not None:
            ref = ctx.message.reference
            if ref.resolved is not None and isinstance(ref.resolved, discord.Message):
                target = ref.resolved
            else:
                await ctx.rsend('Viestiä, johon vastasit, ei löydy!')
                raise RuntimeError('Replied message not found')
        elif len(ctx.message.stickers) > 0:
            target = ctx.message.stickers[0]
        else:
            target = ctx.author

    return_bytes = False
    if image_bytes is None:
        return_bytes = True
        image_bytes = BytesIO()

    is_avatar = False
    if isinstance(target, (discord.Member, discord.User)):
        avatar = target.display_avatar.with_static_format('png')
        # Save the avatar to memory
        await avatar.save(image_bytes)
        is_avatar = True

    elif isinstance(target, discord.Attachment):
        if target.content_type is None or \
                not target.content_type.startswith('image'):
            await ctx.rsend('Tiedoston pitää olla kuva!')
            raise RuntimeError('Not an image')
        elif target.size > 10 * (1024 ** 2):
            await ctx.rsend('Max 10 MB kuvia kiitos!')
            raise RuntimeError('Too big image!')

        await target.save(image_bytes)

    elif isinstance(target, ParseResult):
        # Async magic
        await _download_image(ctx, target.geturl(), image_bytes)

    elif isinstance(target, PreviousImage):
        async for message in ctx.history(limit=100):
            if await _try_message(ctx, message, image_bytes, False):
                break
        else:
            await ctx.rsend('Kuvaa ei löydy!')
            raise RuntimeError('Could not find previous image.')

    elif isinstance(target, discord.Message):
        if not await _try_message(ctx, target, image_bytes, False):
            await ctx.rsend('Viestistä ei löydy kuvaa!')
            raise RuntimeError('Replied message is illegal!')

    elif isinstance(target, discord.StickerItem):
        if target.format == discord.StickerFormatType.lottie:
            async with ctx.bot.session.get(target.url) as resp:
                data = await resp.text()
            out_bytes = await ctx.bot.loop.run_in_executor(
                ctx.bot.executor,
                partial(
                    _lottie_to_apng,
                    lottie_json=data,
                )
            )
            image_bytes.write(out_bytes.read())
            out_bytes.close()
        else:
            await target.save(image_bytes)

    image_bytes.seek(0)

    if return_bytes:
        if return_info:
            return image_bytes, is_avatar
        return image_bytes
    elif return_info:
        return is_avatar


async def handle_image_error(ctx: CustomContext, error: commands.CommandError):
    ctx.error_handled = True

    orig_error = getattr(error, 'original', error)

    if isinstance(orig_error, commands.BadUnionArgument):
        await ctx.rsend('Antamaasi käyttäjää ei löydy tai url ei kelpaa!')
    elif isinstance(orig_error, OSError):
        await ctx.rsend(
            'Antamasi URL:n takaa ei löydy kuvaa, tai jotain muuta kivaa '
            'tapahtui!'
        )
    elif isinstance(orig_error, commands.BadArgument):
        await ctx.rsend('Antamaasi käyttäjää ei löydy.')
    elif isinstance(orig_error, processors.TooBigImage):
        await ctx.rsend('Antamasi kuva on liian iso!')
    else:
        ctx.error_handled = False
