import typing

from PIL import ImageFont, ImageDraw
from lxml import html


def get_font_sized_for_text(
        font_name: str,
        text: str,
        image_draw: ImageDraw,
        start_size: int,
        max_width: float
) -> ImageFont:
    font_size = start_size
    font = ImageFont.truetype(font_name, font_size)

    text_width = image_draw.textlength(text, font=font)
    while text_width > max_width:
        font_size -= 1
        font = ImageFont.truetype(font_name, font_size)
        text_width = image_draw.textlength(text, font=font)
    return font


def extract_image_url_from_metadata(html_content: str) -> typing.List[str]:
    tree = html.fromstring(html_content)
    # Find the <meta> element with property="og:image" and return the value of
    # its content attribute
    return tree.xpath('//meta[@property="og:image"]/@content')
