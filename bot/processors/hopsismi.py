from io import BytesIO

from PIL import Image, ImageDraw

from processors.util import get_font_sized_for_text


HOPS_WITH = 1000
HOPS_HEIGHT = 842
FONT_SIZE_START = 35
MAX_WIDTH = 440
TEXT_CENTER_Y = 165
FONT_PATH = 'files/animeace2_bld.ttf'
TEXT_COLOR = (45, 45, 45, 255)

hops_template = Image.open('files/hopsismi.png').convert('RGBA')


def hopsify(text: str) -> BytesIO:
    target_image = hops_template.copy()
    draw = ImageDraw.Draw(target_image)
    font = get_font_sized_for_text(
        FONT_PATH,
        text,
        draw,
        FONT_SIZE_START,
        MAX_WIDTH
    )
    text_size = draw.textlength(text, font=font)
    text_x = MAX_WIDTH - text_size
    draw.text(
        (text_x, TEXT_CENTER_Y),
        text, fill=TEXT_COLOR,
        font=font,
        anchor='lm'
    )
    final = BytesIO()
    target_image.save(final, format='PNG', optimize=True)
    return final
