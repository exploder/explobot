import zipfile
from zipfile import ZipFile
from io import BytesIO

import discord


DISCORD_MAX_FILE_SIZE = 25 * 1024 * 1024  # As per Discord documentation


async def gdpr_zip_files(all_files_dict: dict) -> list[discord.File]:
    all_zips = []

    file_bytes = BytesIO()
    zf = ZipFile(
        file_bytes,
        mode='w',
        compression=zipfile.ZIP_STORED,
        compresslevel=0
    )
    file_num = 0
    # Allow some size for headers
    current_size = 512 * 1024
    for msg_id, attachments in all_files_dict.items():
        for attachment in attachments:
            if current_size + attachment.size > DISCORD_MAX_FILE_SIZE:
                zf.close()
                file_bytes.seek(0)
                all_zips.append(
                    discord.File(file_bytes, f'attachments_{file_num}.zip')
                )
                file_num += 1
                file_bytes = BytesIO()
                zf = ZipFile(
                    file_bytes,
                    mode='w',
                    compression=zipfile.ZIP_STORED,
                    compresslevel=0
                )
                current_size = 10 * 1024
            zf.writestr(
                f'{msg_id}/{attachment.filename}',
                await attachment.read()
            )
            current_size += attachment.size
    if file_bytes.getbuffer().nbytes > 0:
        zf.close()
        file_bytes.seek(0)
        all_zips.append(
            discord.File(file_bytes, f'attachments_{file_num}.zip')
        )
    return all_zips
