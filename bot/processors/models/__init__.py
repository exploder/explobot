from datetime import datetime


_WIND_DIRECTIONS = [
    'Pohjoisesta',
    'Koillisesta',
    'Idästä',
    'Kaakosta',
    'Etelästä',
    'Lounaasta',
    'Lännestä',
    'Luoteesta'
]


class OWMWeather:
    def __init__(self, json_data: dict):
        self.orig_json = json_data

        main = json_data.get('main')
        sys_element = json_data.get('sys')
        weather = json_data.get('weather')
        wind = json_data.get('wind')
        clouds = json_data.get('clouds')
        rain = json_data.get('rain')
        snow = json_data.get('snow')
        coord = json_data.get('coord')

        self.status_code = json_data.get('cod')

        self.temp = main.get('temp')
        self.feels_like = main.get('feels_like')
        self.temp_min = main.get('temp_min')
        self.temp_max = main.get('temp_max')

        self.humidity = main.get('humidity')

        self.wind_speed = wind.get('speed')
        self.wind_dir_deg = wind.get('deg')
        self.wind_dir_readable = None

        if self.wind_dir_deg is not None:
            # Convert degree -> words
            tmp = self.wind_dir_deg + 22.5
            if tmp > 360:
                tmp = tmp - 360
            self.wind_dir_readable = _WIND_DIRECTIONS[int(tmp / 45)]

        self.cloudiness = clouds.get('all')
        self.visibility = json_data.get('visibility')

        self.icon_url = \
            f'https://openweathermap.org/img/wn/{weather[0]["icon"]}@4x.png'

        tz_offset = json_data.get('timezone')
        self.calc_time_utc = datetime.utcfromtimestamp(json_data.get('dt'))
        self.sunrise_utc = datetime.utcfromtimestamp(sys_element.get('sunrise'))
        self.sunrise_local = datetime.utcfromtimestamp(
            sys_element.get('sunrise') + tz_offset)
        self.sunset_utc = datetime.utcfromtimestamp(sys_element.get('sunset'))
        self.sunset_local = datetime.utcfromtimestamp(
            sys_element.get('sunset') + tz_offset)

        self.rain_1h = None
        self.rain_3h = None

        if rain is not None:
            self.rain_1h = rain.get('1h')
            self.rain_3h = rain.get('3h')

        self.snow_1h = None
        self.snow_3h = None

        if snow is not None:
            self.snow_1h = snow.get('1h')
            self.snow_3h = snow.get('3h')

        self.country_code = sys_element.get('country')
        self.lat = coord.get('lat')
        self.lon = coord.get('lon')

        descriptions = []
        for entry in weather:
            descriptions.append(entry.get('description'))

        self.description = ', '.join(descriptions).capitalize()

        self.name = json_data.get('name')
