import enum
import typing
from dataclasses import dataclass


class MediaType(enum.Enum):
    UNKNOWN = 'unknown'
    TV = 'tv'
    OVA = 'ova'
    MOVIE = 'movie'
    SPECIAL = 'special'
    ONA = 'ona'
    MUSIC = 'music'
    MANGA = 'manga'
    NOVEL = 'novel'
    ONE_SHOT = 'one_shot'
    DOUJINSHI = 'doujinshi'
    MANHWA = 'manhwa'
    MANHUA = 'manhua'
    OEL = 'oel'
    LIGHT_NOVEL = 'light_novel'


class Status(enum.Enum):
    ANIME_FINISHED = 'finished_airing'
    ANIME_ONGOING = 'currently_airing'
    ANIME_UPCOMING = 'not_yet_aired'
    MANGA_FINISHED = 'finished'
    MANGA_ONGOING = 'currently_publishing'
    MANGA_UPCOMING = 'not_yet_published'
    MANGA_ON_HIATUS = 'on_hiatus'
    MANGA_DISCONTINUED = 'discontinued'


class Season(enum.Enum):
    WINTER = 'winter'
    SPRING = 'spring'
    SUMMER = 'summer'
    FALL = 'fall'


class OriginalWorkType(enum.Enum):
    OTHER = 'other'
    ORIGINAL = 'original'
    MANGA = 'manga'
    FOUR_KOMA_MANGA = '4_koma_manga'
    WEB_MANGA = 'web_manga'
    DIGITAL_MANGA = 'digital_manga'
    NOVEL = 'novel'
    LIGHT_NOVEL = 'light_novel'
    VISUAL_NOVEL = 'visual_novel'
    GAME = 'game'
    CARD_GAME = 'card_game'
    BOOK = 'book'
    PICTURE_BOOK = 'picture_book'
    RADIO = 'radio'
    MUSIC = 'music'


class AgeRating(enum.Enum):
    G = 'g'
    PG = 'pg'
    PG_13 = 'pg_13'
    R = 'r'
    R_PLUS = 'r+'
    RX = 'rx'


_ENUM_TO_TEXT_MAPPING = {
    MediaType.UNKNOWN: 'Tuntematon',
    MediaType.MOVIE: 'Elokuva',
    MediaType.SPECIAL: 'Erikoisuus',
    MediaType.MUSIC: 'Musiikki',
    MediaType.MANGA: 'Manga',
    MediaType.NOVEL: 'Romaani',
    MediaType.ONE_SHOT: 'One-shot',
    MediaType.DOUJINSHI: 'Doujinshi',
    MediaType.MANHWA: 'Manhwa',
    MediaType.MANHUA: 'Manhua',
    MediaType.LIGHT_NOVEL: 'Light novel',
    Status.ANIME_FINISHED: 'Päättyneet',
    Status.ANIME_ONGOING: 'Käynnissä',
    Status.ANIME_UPCOMING: 'Tulossa',
    Status.MANGA_FINISHED: 'Valmis',
    Status.MANGA_ONGOING: 'Käynnissä',
    Status.MANGA_UPCOMING: 'Tulossa',
    Status.MANGA_ON_HIATUS: 'Tauolla',
    Status.MANGA_DISCONTINUED: 'Lopetettu',
    Season.WINTER: 'Talvi',
    Season.SPRING: 'Kevät',
    Season.SUMMER: 'Kesä',
    Season.FALL: 'Syksy',
    OriginalWorkType.OTHER: 'Muu',
    OriginalWorkType.ORIGINAL: 'Alkuperäinen',
    OriginalWorkType.MANGA: 'Manga',
    OriginalWorkType.FOUR_KOMA_MANGA: 'Yonkoma',
    OriginalWorkType.WEB_MANGA: 'Web-manga',
    OriginalWorkType.DIGITAL_MANGA: 'Digitaalinen manga',
    OriginalWorkType.NOVEL: 'Romaani',
    OriginalWorkType.LIGHT_NOVEL: 'Light novel',
    OriginalWorkType.VISUAL_NOVEL: 'Visual novel',
    OriginalWorkType.GAME: 'Peli',
    OriginalWorkType.CARD_GAME: 'Korttipeli',
    OriginalWorkType.BOOK: 'Kirja',
    OriginalWorkType.PICTURE_BOOK: 'Kuvakirja',
    OriginalWorkType.RADIO: 'Radio',
    OriginalWorkType.MUSIC: 'Musiikki',
}


def translate_anime_enum(value: typing.Union[MediaType, Status, Season, OriginalWorkType]) -> str:
    return _ENUM_TO_TEXT_MAPPING.get(value, value.name)


@dataclass(kw_only=True)
class Picture:
    medium: str
    large: str = None


@dataclass(kw_only=True)
class WorkBase:
    id: int
    title: str
    main_picture: Picture = None

    def __post_init__(self):
        if isinstance(self.main_picture, dict):
            self.main_picture = Picture(**self.main_picture)


@dataclass(kw_only=True)
class AlternativeTitles:
    synonyms: list[str] = None
    en: str = None
    ja: str = None


@dataclass(kw_only=True)
class Genre:
    id: int
    name: str


@dataclass(kw_only=True)
class AnimeSeason:
    year: int
    season: Season

    def __post_init__(self):
        if isinstance(self.season, str):
            self.season = Season(self.season)


@dataclass(kw_only=True)
class BroadcastDate:
    day_of_the_week: str
    start_time: str = None


@dataclass(kw_only=True)
class AnimeStudio:
    id: int
    name: str


@dataclass(kw_only=True)
class PersonBase:
    id: int
    first_name: str
    last_name: str


@dataclass(kw_only=True)
class PersonRoleEdge:
    node: PersonBase
    role: str

    def __post_init__(self):
        if isinstance(self.node, dict):
            self.node = PersonBase(**self.node)


@dataclass(kw_only=True)
class WorkForList(WorkBase):
    num_list_users: int = None
    num_scoring_users: int = None
    genres: list[Genre] = None
    created_at: str = None
    updated_at: str = None
    alternative_titles: AlternativeTitles = None
    start_date: str = None
    end_date: str = None
    synopsis: str = None
    mean: float = None
    rank: int = None
    popularity: int = None
    nsfw: str = None

    def __post_init__(self):
        super().__post_init__()
        if self.genres is not None and len(self.genres) > 0 and isinstance(self.genres[0], dict):
            self.genres = [Genre(**genre) for genre in self.genres]
        if isinstance(self.alternative_titles, dict):
            self.alternative_titles = AlternativeTitles(**self.alternative_titles)


@dataclass(kw_only=True)
class AnimeForList(WorkForList):
    media_type: MediaType = None
    status: Status = None
    num_episodes: int = None
    studios: list[AnimeStudio] = None
    start_season: AnimeSeason = None
    my_list_status: dict = None
    broadcast: BroadcastDate = None
    source: OriginalWorkType = None
    average_episode_duration: int = None
    rating: AgeRating = None

    def __post_init__(self):
        super().__post_init__()
        if isinstance(self.media_type, str):
            self.media_type = MediaType(self.media_type)
        if isinstance(self.status, str):
            self.status = Status(self.status)
        if self.studios is not None and len(self.studios) > 0 and isinstance(self.studios[0], dict):
            self.studios = [AnimeStudio(**studio) for studio in self.studios]
        if isinstance(self.start_season, dict):
            self.start_season = AnimeSeason(**self.start_season)
        if isinstance(self.broadcast, dict):
            self.broadcast = BroadcastDate(**self.broadcast)
        if isinstance(self.source, str):
            self.source = OriginalWorkType(self.source)
        if isinstance(self.rating, str):
            self.rating = AgeRating(self.rating)


@dataclass(kw_only=True)
class MangaForList(WorkForList):
    media_type: MediaType = None
    status: Status = None
    my_list_status: dict = None
    num_volumes: int = None
    num_chapters: int = None
    authors: list[PersonRoleEdge] = None

    def __post_init__(self):
        super().__post_init__()
        if isinstance(self.media_type, str):
            self.media_type = MediaType(self.media_type)
        if isinstance(self.status, str):
            self.status = Status(self.status)
        if self.authors is not None and len(self.authors) > 0 and isinstance(self.authors[0], dict):
            self.authors = [PersonRoleEdge(**author) for author in self.authors]
