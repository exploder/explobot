import asyncio
from io import BytesIO

from expiringdict import ExpiringDict
from aiohttp import ClientSession, ClientResponseError

_YLE_BASE = 'https://external.api.yle.fi'
_PAGE_INFO_PATH = '/v1/teletext/pages/{page}.json'
_PAGE_IMAGE_URL = '/v1/teletext/images/{page}/{subpage}.png'


class YleClient:
    def __init__(self, session: ClientSession, app_id: str, app_key: str):
        self.session = session
        self.params = {
            'app_id': app_id,
            'app_key': app_key,
        }
        self._image_cache = ExpiringDict(max_len=100, max_age_seconds=300)

    async def page_info(self, page: int) -> dict:
        async with self.session.get(f'{_YLE_BASE}{_PAGE_INFO_PATH.format(page=page)}', params=self.params) as resp:
            resp.raise_for_status()
            # YLE's API has an incorrect content-type header (binary/octet-stream instead of application/json...)
            return await resp.json(content_type=None)

    async def _page_image(self, page_number: int, subpage_number: int, recursion_depth: int):
        cache_key = (page_number, subpage_number)
        try:
            return self._image_cache[cache_key]
        except KeyError:
            async with self.session.get(
                    f'{_YLE_BASE}{_PAGE_IMAGE_URL.format(page=page_number, subpage=subpage_number)}',
                    params=self.params
            ) as resp:
                try:
                    resp.raise_for_status()
                except ClientResponseError as e:
                    # Quota exceeded: try again in 10 seconds, and raise if there were too many retries already
                    if e.status == 401 and recursion_depth < 3:
                        await asyncio.sleep(10)
                        return await self._page_image(page_number, subpage_number, recursion_depth + 1)
                    raise e
                data = BytesIO()
                data.write(await resp.read())
            data.seek(0)
            self._image_cache[cache_key] = data
            return data

    async def page_image(self, page_number: int, subpage_number: int) -> BytesIO:
        return await self._page_image(page_number, subpage_number, 0)
