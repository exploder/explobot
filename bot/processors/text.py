import enum

HM_ALLOWED_LETTERS = 'abcdefghijklmnoprstuvwyäö'

HM_MESSAGE = \
    '''```{title}
{hangman}
{word}
Väärät: {wrong}```'''

HM_TITLE_ONGOING = 'HIRSIPUU - peli käynnissä'
HM_TITLE_GAME_OVER_TIMEOUT = '''HIRSIPUU - peli ohi - aika loppui
Sana oli {word}'''
HM_TITLE_GAME_OVER_RIP = '''HIRSIPUU - peli ohi - liikaa vääriä arvauksia
Sana oli {word}'''
HM_TITLE_GAME_WON = '''HIRSIPUU - voitit pelin!'''


def get_progress_bar(
        progress: int,
        bar_length: int,
        progress_chars: str = ' ░▒▓█'
) -> str:
    if not 0 <= progress <= 100:
        raise ValueError(
            f'Expected progress to be between 0 and 100, got {progress} '
            f'instead!'
        )
    if 100 % bar_length != 0:
        raise ValueError('Progress bar length should divide 100 evenly!')

    if len(progress_chars) < 2:
        raise ValueError(
            'There should be at least 2 value characters, one for empty and '
            'one for filled progress!'
        )

    one_bar_length = int(100 / bar_length)

    bar = ''
    bar += int(progress / one_bar_length) * progress_chars[-1]
    left = progress % one_bar_length

    if progress < 100 and len(progress_chars) > 2:
        one_char_length = (one_bar_length - 1) / (len(progress_chars) - 1)
        for i in range(len(progress_chars) - 1):
            if left <= (i * one_char_length + 1):
                bar += progress_chars[i]
                break

        bar += (bar_length - int(progress / one_bar_length) - 1) * ' '

    return bar


def get_long_progress_bar(progress: int) -> str:
    if not 0 <= progress <= 100:
        raise ValueError(
            f'Expected progress to be between 0 and 100, got {progress} '
            f'instead!'
        )

    bar = ''
    bar += int(progress / 5) * '█'
    left = progress % 5

    if progress < 100:
        if left == 0:
            bar += ' '
        elif 1 <= left <= 2:
            bar += '░'
        elif left == 3:
            bar += '▒'
        else:  # left == 4
            bar += '▓'
        bar += (19 - int(progress / 5)) * ' '
    return bar


def get_hangman_hidden_word(
        word,
        allowed_letters=HM_ALLOWED_LETTERS,
        right_guesses=None
) -> str:
    if right_guesses is None:
        right_guesses = []
    hidden_word = ''
    for ch in word:
        if ch not in allowed_letters or ch in right_guesses:
            hidden_word += ch
            hidden_word += ' '
        else:
            hidden_word += '_ '

    hidden_word = hidden_word.rstrip()
    return hidden_word.upper()


class HangmanState(enum.Enum):
    ONGOING = enum.auto()
    GAME_OVER_TIMEOUT = enum.auto()
    GAME_OVER_RIP = enum.auto()
    VICTORY = enum.auto()


class Hangman:
    _ascii_art = []

    def __init__(self, word: str, allowed_letters: str = HM_ALLOWED_LETTERS):
        self.the_word = word
        self.should_edit = False
        self.ongoing = True
        self._allowed_letters = allowed_letters
        self._wrong_guesses = []
        self._right_guesses = []
        self._state = HangmanState.ONGOING

    def get_message_content(self) -> str:
        hidden_word = self._get_hidden_word()

        msg = 'Tämän ei pitäisi koskaan näkyä (jotain hassua on tapahtunut)'

        if self._state == HangmanState.GAME_OVER_TIMEOUT:
            msg = HM_MESSAGE.format(
                word=hidden_word,
                wrong=', '.join(self._wrong_guesses).upper(),
                title=HM_TITLE_GAME_OVER_TIMEOUT.format(
                    word=self.the_word
                ),
                hangman=self._ascii_art[len(self._wrong_guesses)]
            )
        elif self._state == HangmanState.GAME_OVER_RIP:
            msg = HM_MESSAGE.format(
                word=hidden_word,
                wrong=', '.join(self._wrong_guesses).upper(),
                title=HM_TITLE_GAME_OVER_RIP.format(
                    word=self.the_word
                ),
                hangman=self._ascii_art[len(self._wrong_guesses)]
            )
        elif self._state == HangmanState.VICTORY:
            msg = HM_MESSAGE.format(
                word=hidden_word,
                wrong=', '.join(self._wrong_guesses).upper(),
                title=HM_TITLE_GAME_WON,
                hangman=self._ascii_art[len(self._wrong_guesses)]
            )
        elif self._state == HangmanState.ONGOING:
            msg = HM_MESSAGE.format(
                word=hidden_word,
                wrong=', '.join(self._wrong_guesses).upper(),
                title=HM_TITLE_ONGOING,
                hangman=self._ascii_art[len(self._wrong_guesses)]
            )

        self.should_edit = False

        return msg

    def timeout(self):
        self._state = HangmanState.GAME_OVER_TIMEOUT
        self.ongoing = False
        self.should_edit = True

    def guess_char(self, char: str) -> bool:
        if len(char) > 1:
            raise ValueError('You can only guess single characters!')

        if char in self.the_word and char not in self._right_guesses:
            self._right_guesses.append(char)

            hidden_word = self._get_hidden_word()

            if '_' not in hidden_word:
                self._state = HangmanState.VICTORY
                self.ongoing = False

            self.should_edit = True
            return True

        elif char not in self.the_word and char not in self._wrong_guesses:
            self._wrong_guesses.append(char)

            if len(self._wrong_guesses) > len(self._ascii_art) - 2:
                self._state = HangmanState.GAME_OVER_RIP
                self.ongoing = False

            self.should_edit = True
            return False

    def _get_hidden_word(self) -> str:
        hidden_word = ''
        for ch in self.the_word:
            if ch not in self._allowed_letters or ch in self._right_guesses:
                hidden_word += ch
                hidden_word += ' '
            else:
                hidden_word += '_ '
        return hidden_word.rstrip().upper()

    @classmethod
    def set_ascii_art(cls, ascii_art):
        cls._ascii_art = ascii_art
