import math
from io import BytesIO

from PIL import Image, ImageOps


AVATAR_OFFSET = (1116, 34)
AVATAR_ROTATION = -6.89
TAN_AVATAR_ROTATION = abs(math.tan(math.radians(AVATAR_ROTATION)))
SIN_AVATAR_ROTATION = abs(math.sin(math.radians(AVATAR_ROTATION)))
AVATAR_SIZE = (480, 480)
DISTANCE_TO_CORNER = AVATAR_SIZE[0] / (TAN_AVATAR_ROTATION + 1)
COLOR_TRANSPARENT = (0, 0, 0, 0)
COLOR_BLACK = (0, 0, 0)
# If tank image height / width is larger, the image cannot be processed
MAX_RATIO = 30


mask = Image.open('files/mask2.png').convert('L')
mask = ImageOps.invert(mask)
tank_raw = Image.open('files/tank.png').convert('RGBA')
empty = Image.new('RGBA', AVATAR_SIZE, COLOR_TRANSPARENT)


class TooBigImage(Exception):
    pass


def tankify(target_image_bytes: BytesIO, recalculate_size: bool) -> BytesIO:
    # Convert image to RGBA
    # FIXME: GIFs cannot be processed into GIF tanks
    # due to https://github.com/python-pillow/Pillow/issues/1525
    target_image = Image.open(target_image_bytes)
    target_image = target_image.convert('RGBA')

    if target_image.height / target_image.width > MAX_RATIO:
        raise TooBigImage(
            f'The image is too big to process with dimensions '
            f'(w={target_image.width} h={target_image.height})'
        )

    # Transform the avatar image
    height_before_rotate = target_image.height
    target_image = target_image.rotate(AVATAR_ROTATION, expand=True)
    if recalculate_size:
        height_orig = target_image.height
        width_orig = target_image.width

        # Calculate the dimensions and resize
        width = int(
            DISTANCE_TO_CORNER * (
                1 + (
                    (height_before_rotate * SIN_AVATAR_ROTATION) /
                    (width_orig - height_before_rotate * SIN_AVATAR_ROTATION)
                )
            )
        )
        height = int((width / width_orig) * height_orig)
        target_image = target_image.resize((int(width), int(height)))

        # Create a mask
        tmp_mask = Image.new('L', (width, height), 0)
        tmp_mask.paste(mask, (0, height - mask.height))

        tmp_empty = Image.new('RGBA', (width, height), COLOR_TRANSPARENT)
        new_mask = Image.composite(tmp_empty, target_image, tmp_mask)

        # Get a fresh copy of tank image
        tank = tank_raw.copy()

        # Calculate the paste position and canvas dimensions
        # left offset is always the same
        left = AVATAR_OFFSET[0]
        # top is negative if the image goes beyond tank image limits vertically

        top = AVATAR_OFFSET[1] - (target_image.height - AVATAR_SIZE[1])
        # How much larger the image should be horizontally to fit the whole pic
        right_padding = (AVATAR_OFFSET[0] + target_image.width) - tank.width

        if right_padding < 0:
            right_padding = 0
        top_padding = 0
        if top < 0:
            top_padding = abs(top)

        # Create correctly sized canvas
        new_tank = Image.new(
            'RGBA',
            (tank.width + right_padding, tank.height + top_padding),
            COLOR_TRANSPARENT
        )
        new_tank.paste(tank, (0, top_padding))
        tank = new_tank
        if top < 0:
            top = 0
        tank.paste(target_image, (left, top), new_mask)

    else:
        tank = tank_raw.copy()
        target_image = target_image.resize(AVATAR_SIZE)
        new_mask = Image.composite(empty, target_image, mask)
        tank.paste(target_image, AVATAR_OFFSET, new_mask)
    # Resize and save to memory
    tank = tank.resize((int(tank.width / 2), int(tank.height / 2)))
    final_bytes = BytesIO()
    tank.save(final_bytes, format='PNG', optimize=True)
    return final_bytes
