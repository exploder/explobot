import aiohttp


BASE_URL = 'https://gitlab.com/api/v4'
NEW_ISSUE_API = '/projects/{project}/issues'


async def create_issue(
        token: str,
        repo: int,
        title: str,
        description: str,
        labels: str,
        session: aiohttp.ClientSession
):
    post_data = {
        'private_token': token,
        'title': title,
        'description': description,
        'labels': labels
    }
    post_url = f'{BASE_URL}{NEW_ISSUE_API.format(project=repo)}'
    async with session.post(post_url, data=post_data) as resp:
        resp.raise_for_status()
        return await resp.json()
