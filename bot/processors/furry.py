from io import BytesIO

from PIL import Image


AVATAR_OFFSET = (190, 60)
AVATAR_ROTATION = 20
AVATAR_SIZE = (130, 130)
raw_furry = Image.open('files/furry.png').convert('RGBA')
COLOR_TRANSPARENT = (0, 0, 0, 0)
empty = Image.new('RGBA', AVATAR_SIZE, COLOR_TRANSPARENT)


def furrify(target_image_bytes: BytesIO) -> BytesIO:
    target_image = Image.open(target_image_bytes)
    target_image = target_image.convert('RGBA')
    target_image = target_image.rotate(AVATAR_ROTATION, expand=True)
    target_image = target_image.resize(AVATAR_SIZE)
    furry = raw_furry.copy()
    furry.paste(target_image, AVATAR_OFFSET, target_image)
    final = BytesIO()
    furry.save(final, format='PNG', optimize=True)
    return final
