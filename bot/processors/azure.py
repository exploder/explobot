import re
import typing
import asyncio
import enum
import logging
from contextlib import suppress
from io import BytesIO

import aiohttp
from aiohttp.client_exceptions import ContentTypeError


class JobStatus(enum.Enum):
    WAITING = 'waiting'
    QUEUED = 'queued'
    PROCESSING = 'processing'
    DONE = 'ready'
    ERROR = 'error'
    CANCELLED = 'cancelled'


class ClusterStatus(enum.Enum):
    UNKNOWN = '-'
    STOPPED = 'Sammutettu'
    STOPPING = 'Sammuu'
    STARTING = 'Käynnistyy'
    RUNNING = 'Päällä'

    @classmethod
    def get_state(cls, cluster_data: dict):
        power = cluster_data['power']
        state = cluster_data['state']
        if state == 'Succeeded':
            if power == 'Stopped':
                return cls.STOPPED
            elif power == 'Running':
                return cls.RUNNING
            return cls.UNKNOWN
        elif state == 'Stopping':
            return cls.STOPPING
        elif state == 'Starting':
            return cls.STARTING
        return cls.UNKNOWN


INCORRECT_STATE = 'INCORRECT_STATE'
UUID_REGEX = re.compile(
    r'^[a-f\d]{8}-[a-f\d]{4}-4[a-f\d]{3}-[89aAbB][a-f\d]{3}-[a-f\d]{12}$'
)


class Job:
    def __init__(
            self,
            job_id: str,
            callback: typing.Callable[
                [str, JobStatus, ClusterStatus], typing.Awaitable
            ],
            user_id: int
    ):
        self.id = job_id
        self.user_id = user_id
        self.status = JobStatus.WAITING
        self.cluster_status = ClusterStatus.UNKNOWN
        self._callback = callback
        self._event = asyncio.Event()

    async def set_status(self, new_status: JobStatus):
        if new_status == self.status:
            return

        self.status = new_status
        await self._callback(self.id, self.status, self.cluster_status)

        if self.status in \
                [JobStatus.DONE, JobStatus.ERROR, JobStatus.CANCELLED]:
            self._event.set()

    async def set_cluster_status(self, new_status: ClusterStatus):
        if new_status == self.cluster_status:
            return

        self.cluster_status = new_status
        await self._callback(self.id, self.status, self.cluster_status)

    async def wait(self):
        await self._event.wait()


class ProcessingError(RuntimeError):
    pass


class NotFound(ValueError):
    pass


class IncorrectState(ValueError):
    pass


def _task_exception_log(task: asyncio.Task):
    logger = logging.getLogger('bot')
    try:
        exception = task.exception()
        if exception is None:
            return
        logger.exception(
            f'Error in task {task}.',
            exc_info=(
                type(exception),
                exception,
                exception.__traceback__
            )
        )
    except asyncio.CancelledError:
        # Ignore
        return
    except asyncio.InvalidStateError:
        # This should not happen /shrug
        logger.debug('Task is somehow in invalid state', exc_info=True)


class AzureJobManager:
    _SUBMIT_URL = '{func_url}/api/start-gpu-processing'
    _STATUS_URL = '{func_url}/api/status'
    _RESULT_URL = '{func_url}/api/get-gpu-result'
    _CANCEL_URL = '{func_url}/api/cancel'
    _QUEUE_URL = '{func_url}/api/queue-status'
    _CHUNK_SIZE = 1024

    def __init__(
            self,
            function_url: str,
            function_key: str,
            session: aiohttp.ClientSession
    ):
        self._func_url = function_url
        self._func_code = function_key
        self._running = False
        self._task = None
        self._session = session
        self._pending_jobs: typing.Dict[str, Job] = {}
        self._logger = logging.getLogger('bot')
        self._cluster_status = None

    def __del__(self):
        # Stop the loop
        try:
            loop = asyncio.get_event_loop()
            if loop.is_running():
                if self._running:
                    loop.create_task(self._stop())
            else:
                if self._running:
                    loop.run_until_complete(self._stop())
        except Exception:  # noqa
            pass

    async def submit_job(
            self,
            job_data: BytesIO,
            status_callback: typing.Callable[
                [str, JobStatus, ClusterStatus], typing.Awaitable
            ],
            user_id: int,
            driving_video_id: int = 0,
            skip_cluster: bool = False,
    ) -> BytesIO:
        await status_callback(None, JobStatus.WAITING, self._cluster_status)
        url = self._SUBMIT_URL.format(func_url=self._func_url)
        params = self._default_params()
        params['driving_video'] = driving_video_id
        if skip_cluster:
            params['skip_cluster'] = 'true'
        files = {'file': job_data}

        async with self._session.post(url, params=params, data=files) as resp:
            resp.raise_for_status()
            job_id = await resp.text()
        if not UUID_REGEX.match(job_id):
            raise ProcessingError(f'Expected a UUID, got {job_id}')
        self._logger.debug(job_id)

        the_job = Job(job_id, status_callback, user_id)

        await the_job.set_cluster_status(self._cluster_status)
        await the_job.set_status(JobStatus.QUEUED)

        self._pending_jobs[job_id] = the_job

        await self._start()

        await the_job.wait()

        self._pending_jobs.pop(job_id)

        if the_job.status == JobStatus.ERROR:
            raise ProcessingError()
        elif the_job.status == JobStatus.CANCELLED:
            return None

        output = BytesIO()
        url = self._RESULT_URL.format(func_url=self._func_url)
        params = self._default_params()
        params['job'] = job_id

        async with self._session.get(url, params=params) as resp:
            resp.raise_for_status()
            async for chunk in resp.content.iter_chunked(self._CHUNK_SIZE):
                output.write(chunk)

        output.seek(0)
        return output

    async def cancel_job(self, job_id: str):
        params = self._default_params()
        params['job'] = job_id
        url = self._CANCEL_URL.format(func_url=self._func_url)

        async with self._session.post(url, params=params) as resp:
            if resp.status == 404:
                raise NotFound()
            elif resp.status == 400 and await resp.text() == INCORRECT_STATE:
                raise IncorrectState()
            resp.raise_for_status()

    async def get_queue(self) -> list:
        params = self._default_params()
        url = self._QUEUE_URL.format(func_url=self._func_url)

        async with self._session.get(url, params=params) as resp:
            resp.raise_for_status()
            return await resp.json()

    def check_job(self, user_id: int, job_id: str) -> bool:
        for p_job_id in self._pending_jobs:
            if p_job_id != job_id:
                continue
            if self._pending_jobs[p_job_id].user_id == user_id:
                return True
        return False

    def get_job_user(self, job_id: str) -> int:
        if job_id in self._pending_jobs:
            return self._pending_jobs[job_id].user_id
        return None

    def get_processing_jobs(self) -> dict[str, Job]:
        ret = {}
        for job_id in self._pending_jobs:
            if self._pending_jobs[job_id].status == JobStatus.PROCESSING:
                ret[job_id] = self._pending_jobs[job_id]
        return ret

    def _default_params(self) -> dict:
        return {'code': self._func_code}

    async def _start(self):
        if self._running:
            return
        self._running = True
        self._task = asyncio.ensure_future(self._loop())
        self._task.add_done_callback(_task_exception_log)

    async def _stop(self):
        if not self._running:
            return
        self._running = False
        self._task.cancel()
        with suppress(asyncio.CancelledError):
            await self._task
        self._task = None
        self._cluster_status = ClusterStatus.UNKNOWN

    async def _loop(self):
        url = self._STATUS_URL.format(func_url=self._func_url)
        try:
            while True:
                if len(self._pending_jobs) == 0:
                    self._running = False
                    self._task = None
                    self._cluster_status = ClusterStatus.UNKNOWN
                    return

                job_ids = ','.join(self._pending_jobs)
                params = self._default_params()
                params['jobs'] = job_ids

                async with self._session.get(url, params=params) as resp:
                    try:
                        json_resp = await resp.json()
                    except ContentTypeError:
                        # This happens when there are zero jobs
                        continue
                self._logger.debug(json_resp)

                new_status = ClusterStatus.get_state(json_resp['cluster'])
                cluster_changed = new_status != self._cluster_status
                if cluster_changed:
                    self._cluster_status = new_status

                for job in json_resp['jobs']:
                    job_id = job.get('jobid')
                    the_job = self._pending_jobs.get(job_id)
                    if the_job is None:
                        continue

                    status = job.get('status')
                    the_status = JobStatus(status)

                    await the_job.set_status(the_status)

                    if cluster_changed:
                        await the_job.set_cluster_status(self._cluster_status)

                if len(self._pending_jobs) == 0:
                    self._running = False
                    self._task = None
                    self._cluster_status = ClusterStatus.UNKNOWN
                    return

                await asyncio.sleep(10)
        except Exception as _:
            # If an exception happens, reset running status so that the loop can
            # be restarted
            self._running = False
            self._task = None
            self._cluster_status = ClusterStatus.UNKNOWN
            raise
