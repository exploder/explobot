import logging
import multiprocessing
from io import BytesIO

from wand.image import Image as WImage, BaseImage


MAGIC_STRENGTH_MAX = 1073741823


_logger = logging.getLogger('bot')


def liquify(image: BaseImage, factor: float, delta_x: float):
    image.liquid_rescale(
        width=int(image.width * factor),
        height=int(image.height * factor),
        delta_x=delta_x,
        rigidity=0
    )


def makia(image_bytes: BytesIO, strength: float) -> BytesIO:
    with WImage(file=image_bytes) as image:
        image.coalesce()
        image.format = 'PNG'
        image.transform(resize='400x400>')

        liquify(image, 0.5, strength)
        liquify(image, 2, strength)

        final = BytesIO()
        image.save(file=final)

    return final


def _make_looping(image: BaseImage):
    for i in range(len(image.sequence) - 2, 0, -1):
        image.sequence.append(image.sequence[i])


def gif_makia(
        image_bytes: BytesIO,
        strength: float,
        amount: float,
        frames=15,
        reverse=False,
        no_loop=False
) -> BytesIO:
    log_prefix = f'{multiprocessing.current_process().name}: gif-makia:'
    _logger.debug(f'{log_prefix} Opening image.')

    with WImage(file=image_bytes) as image:
        _logger.debug(f'{log_prefix} Coalescing.')
        image.coalesce()

        _logger.debug(f'Resizing to 400x400>.')
        image.transform(resize='400x400>')

        if image.animation:
            image.iterator_reset()
            image.dispose = 'background'

            liquify(image, 0.5, strength)
            liquify(image, 2, strength)

            _logger.debug(
                f'{log_prefix} processing {len(image.sequence)} frames.'
            )

            i = 1

            while image.iterator_next():
                image.dispose = 'background'
                _logger.debug(
                    f'{log_prefix} Processing frame {i}, size={image.size}'
                )
                liquify(image, 0.5, strength)
                liquify(image, 2, strength)
                i += 1

            image.format = 'GIF'

            _logger.debug(f'{log_prefix} resizing final image to 256x256.')
            image.transform(resize='256x256')

            if reverse:
                _logger.debug(
                    f'{log_prefix} reversing.'
                )
                # We get these frames for free, yay!
                _make_looping(image)

            # This needs to be done BEFORE other processing, otherwise it won't
            # work for some reason...
            image.iterator_reset()
            if no_loop:
                image.loop = 1

            _logger.debug(
                f'{log_prefix} optimizing transparency.'
            )
            image.optimize_transparency()

            final = BytesIO()
            image.save(file=final)
            return final

        with WImage() as output:
            output.dispose = 'background'
            orig_width = image.width
            orig_height = image.height

            _logger.debug(f'{log_prefix} processing {frames} frames.')
            _logger.debug(f'{log_prefix} processing frame 1.')

            frame = image.clone()
            frame.dispose = 'background'
            frame.transform(resize='256x256')
            frame.delay = 10
            output.sequence.append(frame)

            # We alredy did the first image, so one less
            for i in range(frames - 1):
                _logger.debug(f'{log_prefix} processing frame {i + 2}.')

                liquify(image, 0.9, strength)

                image.resize(width=orig_width, height=orig_height)
                image.unsharp_mask(radius=1, amount=amount)
                frame = image.clone()
                frame.dispose = 'background'
                frame.transform(resize='256x256')
                frame.delay = 10
                output.sequence.append(frame)

            if reverse:
                _logger.debug(f'{log_prefix} reversing.')
                # We get these frames for free, yay!
                _make_looping(output)

            output.format = 'GIF'
            _logger.debug(f'{log_prefix} optimizing transparency.')
            output.optimize_transparency()
            if no_loop:
                output.loop = 1
            final = BytesIO()
            output.save(file=final)
            return final


def gif_makia2(
        image_bytes: BytesIO,
        strength: float,
        frames=15,
        reverse=False,
        no_loop=False
) -> BytesIO:
    log_prefix = f'{multiprocessing.current_process().name}: gif-makia2:'
    _logger.debug(f'{log_prefix} Opening image.')

    with WImage(file=image_bytes) as image:
        _logger.debug(f'{log_prefix} Coalescing.')
        image.coalesce()

        image.format = 'PNG'

        _logger.debug(f'Resizing to 400x400>.')
        image.transform(resize='400x400>')

        with WImage() as output:
            orig_width = image.width
            orig_height = image.height

            _logger.debug(f'{log_prefix} processing 15 frames.')
            _logger.debug(f'{log_prefix} processing frame 1.')

            frame = image.clone()
            frame.dispose = 'background'
            frame.transform(resize='256x256')
            frame.delay = 10
            output.sequence.append(frame)

            for i in range(frames - 1):
                _logger.debug(f'{log_prefix} processing frame {i + 2}.')

                liquify(image, 1.1, strength)

                image.resize(width=orig_width, height=orig_height)
                frame = image.clone()
                frame.dispose = 'background'
                frame.transform(resize='256x256')
                frame.delay = 10
                output.sequence.append(frame)

            if reverse:
                # We get these frames for free, yay!
                _logger.debug(f'{log_prefix} reversing.')
                _make_looping(output)

            output.format = 'GIF'
            _logger.debug(f'{log_prefix} optimizing transparency.')
            output.optimize_transparency()
            if no_loop:
                output.loop = 1
            final = BytesIO()
            output.save(file=final)
            return final
