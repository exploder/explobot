from io import BytesIO

import aiohttp


GEOAPIFY_API_URL = 'https://maps.geoapify.com/v1/staticmap'


async def geoapify_get_static_map(
        session: aiohttp.ClientSession,
        api_key: str,
        lat: float,
        lon: float,
        width: int = 600,
        height: int = 400,
        zoom_level: float = 10,
        center_marker: bool = True
) -> bytes:
    get_params = {
        'apiKey': api_key,
        'width': width,
        'height': height,
        'center': f'lonlat:{lon},{lat}',
        'zoom': str(zoom_level)
    }

    if center_marker:
        get_params['marker'] = \
            f'lonlat:{lon},{lat};color:#ff0000;text: ;whitecircle:no'

    async with session.get(GEOAPIFY_API_URL, params=get_params) as resp:
        resp.raise_for_status()
        return await resp.read()


async def get_cached(lat: float, lon: float):
    pass


async def put_cached(image: BytesIO):
    pass
