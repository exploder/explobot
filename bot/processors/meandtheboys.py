import typing
from io import BytesIO

from PIL import Image, ImageDraw, ImageFont


COLOR_WHITE = (255, 255, 255)
PADDING = 10
FONT_SIZE = 50
LINE_SPACING = 20
MATB_WIDTH = 1200
MATB_HEIGHT = 616

SIZES = [
    (202, 202),
    (209, 209),
    (208, 208),
    (224, 224)
]

ROTATIONS = [0, 4.99, 0, -6.76]

# ORIG_OFFSETS = [(73, 406),
#                 (299, 261),
#                 (626, 187),
#                 (886, 245)]

OFFSETS = [
    (73, 406),
    (290, 200),
    (626, 140),
    (870, 210)
]

font = ImageFont.truetype(
    'files/HelveticaNeueLt.ttf',
    50,
    layout_engine=ImageFont.Layout.BASIC
)

_ascent, _descent = font.getmetrics()
ROW_HEIGHT = _ascent + _descent


def split_text(text: str) -> list[str]:
    lines = []

    orig_lines = text.split('\n')  # Preserve original line wraps

    for orig_line in orig_lines:
        line = []
        for word in orig_line.split():
            line.append(word)
            width = font.getlength(' '.join(line))
            if width > MATB_WIDTH - 2 * PADDING:  # Padding left and right
                line = line[:-1]  # The last word didn't fit
                lines.append(' '.join(line))
                line = [word]
        if len(line) > 0:
            lines.append(' '.join(line))
    return lines


def matbify(avatars: typing.List[BytesIO], text: str) -> BytesIO:
    if len(avatars) == 1:
        matb_image = Image.open('files/me.png')
    elif len(avatars) == 2:
        matb_image = Image.open('files/boy.png')
    elif len(avatars) == 3:
        matb_image = Image.open('files/3boys.png')
    else:
        matb_image = Image.open('files/matb.jpg')

    for i in range(len(avatars)):
        avatar = Image.open(avatars[i]).convert('RGBA')
        avatar = avatar.rotate(ROTATIONS[i], expand=True).resize(SIZES[i])
        matb_image.paste(avatar, OFFSETS[i], avatar)

    lines = split_text(text)
    text = '\n'.join(lines)

    # Create a image where we can draw the text (one that is tall enough)
    tmp_image = Image.new('RGB', (MATB_WIDTH, len(lines) * ROW_HEIGHT))
    tmp_draw = ImageDraw.Draw(tmp_image)
    (left, top, right, bottom) = tmp_draw.multiline_textbbox(
        (0, 0),
        text,
        font=font,
        spacing=LINE_SPACING
    )
    text_height = bottom - top

    if text != '':
        full_height = MATB_HEIGHT + LINE_SPACING + text_height
    else:
        full_height = MATB_HEIGHT

    # Create the final image
    final_image = Image.new('RGB', (MATB_WIDTH, full_height), COLOR_WHITE)
    # Draw text
    if text != '':
        draw = ImageDraw.Draw(final_image)
        draw.multiline_text(
            (PADDING, 0),
            text,
            fill=(0, 0, 0, 255),
            font=font,
            align='left',
            spacing=LINE_SPACING
        )
    # Paste the image
    final_image.paste(matb_image, (0, full_height - MATB_HEIGHT))
    final = BytesIO()
    final_image.save(final, format='PNG', optimize=True)
    return final
