import aiohttp

from .models.myanimelist import AnimeForList, MangaForList


_MAL_BASE_URL = 'https://api.myanimelist.net/v2'


class MALClient:
    def __init__(self, client_id: str, session: aiohttp.ClientSession, base_url: str = _MAL_BASE_URL):
        self._client_id = client_id
        self._session = session
        self._base_url = base_url

    async def _call(self, method: str, route: str, params: dict = None) -> dict:
        headers = {
            'X-MAL-CLIENT-ID': self._client_id
        }
        async with self._session.request(method, f'{self._base_url}{route}', params=params, headers=headers) as resp:
            resp.raise_for_status()
            return await resp.json()

    async def search_anime(self, query: str, limit: int = 10, fields: list[str] = None) -> list[AnimeForList]:
        params = {
            'q': query,
            'limit': str(limit),
        }

        if fields is not None:
            params['fields'] = ','.join(fields)

        results = await self._call('GET', '/anime', params=params)
        results = results['data']
        animes = [AnimeForList(**result['node']) for result in results]
        return animes

    async def search_manga(self, query: str, limit: int = 10, fields: list[str] = None) -> list[MangaForList]:
        params = {
            'q': query,
            'limit': str(limit),
        }

        if fields is not None:
            params['fields'] = ','.join(fields)

        results = await self._call('GET', '/manga', params=params)
        results = results['data']
        mangas = [MangaForList(**result['node']) for result in results]
        return mangas
