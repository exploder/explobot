from .gdpr import gdpr_zip_files
from .meandtheboys import matbify
from .tank import tankify, TooBigImage
from .koda import put_certified_name_and_server, create_full_certificate
from .text import (
    get_progress_bar,
    get_long_progress_bar,
    get_hangman_hidden_word,
    HM_ALLOWED_LETTERS,
    Hangman
)
from .furry import furrify
from .gitlab import create_issue
from .weather import owm_get_current_weather, owm_get_forecast
from .maps import geoapify_get_static_map
from .hopsismi import hopsify
from .imagemagick import makia, gif_makia, gif_makia2, MAGIC_STRENGTH_MAX
from .azure import (
    JobStatus,
    AzureJobManager,
    ClusterStatus,
    ProcessingError,
    NotFound,
    IncorrectState,
)
from .food import (
    get_random_food_daily_nutrient,
    get_random_nutrient_dose,
    get_food_name
)
from .myanimelist import MALClient
from .models.myanimelist import translate_anime_enum, AnimeForList, MangaForList, Status
from .hs_comics import get_comic_data, ComicID
from .teletext import YleClient
