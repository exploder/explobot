import argparse
import asyncio

from bot import bot_main


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('config_path', type=str)

    args = parser.parse_args()

    asyncio.run(bot_main(args.config_path))
