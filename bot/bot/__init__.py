import os
import logging
import typing
import atexit
import sys
import asyncio
import signal
import warnings
import traceback
from concurrent.futures.process import ProcessPoolExecutor
from logging.handlers import RotatingFileHandler
from datetime import datetime, timedelta

import discord
import toml
import aiohttp
import json_logging
from sqlalchemy import select
from discord.ext import commands
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.exc import SQLAlchemyError

import db
from util import CustomContext, BetterJSONLogFormatter
from custom_ui import RoleView
from hacks import HackyCommandTree, patch_discord_state_interaction


patch_discord_state_interaction()


_close_task = None


class DiscordBot(commands.Bot):
    def __init__(self, config: dict, logfile_path: str = 'discord.log'):
        self.config = config
        self.bot_config = self.config.get('bot')
        self.tokens = self.config.get('tokens')

        self.default_prefix = self.bot_config.get('prefix', '?')

        stream = logging.StreamHandler(sys.stdout)
        handler = RotatingFileHandler(logfile_path, backupCount=5)

        self._logger = logging.getLogger('bot')
        self._logger.setLevel(logging.INFO)
        self._logger.addHandler(stream)
        self._logger.addHandler(handler)

        discord_logger = logging.getLogger('discord')
        discord_logger.addHandler(handler)
        discord_logger.addHandler(stream)

        warn_log = logging.getLogger('py.warnings')
        warn_log.addHandler(handler)
        warn_log.addHandler(stream)

        handler.doRollover()

        help_cmd = commands.DefaultHelpCommand(
            command_attrs=dict(
                name='apuva',
                help='Näyttää tämän viestin.'
            )
        )

        self.executor = ProcessPoolExecutor(max_workers=3, max_tasks_per_child=1)

        intents = discord.Intents.default()
        intents.members = True
        intents.typing = False
        intents.emojis = False
        intents.integrations = False
        intents.webhooks = False
        intents.invites = False
        intents.voice_states = False
        intents.presences = False
        intents.messages = True
        intents.message_content = True

        self._process_lock = asyncio.Lock()

        super(DiscordBot, self).__init__(
            prefix,
            help_command=help_cmd,
            intents=intents,
            case_insensitive=True,
            tree_cls=HackyCommandTree,
        )

        self.session = aiohttp.ClientSession()
        self.create_db_session = None

        self.twitch = None
        self.event_sub = None

    def __del__(self):
        self.executor.shutdown()

    async def setup_hook(self) -> None:
        sqlalchemy_config = self.config.get('sqlalchemy')
        if sqlalchemy_config is None:
            raise ValueError('SQLAlchemy URL must be set!')
        sqlalchemy_url = sqlalchemy_config.get('connection')
        if sqlalchemy_url is None:
            raise ValueError('SQLAlchemy URL must be set!')

        engine = create_async_engine(sqlalchemy_url, echo=False)

        try:
            conn = await engine.connect()
            await conn.close()
        except (OSError, SQLAlchemyError):
            self._logger.exception('Database connection failed!')
            raise

        self.create_db_session = async_sessionmaker(
            engine,
            expire_on_commit=False
        )

        self._logger.info('Constructing persistent views...')

        views = {}

        async with self.create_db_session() as session:
            async with session.begin():
                setting_stream = await session.stream(
                    select(db.GuildSetting)
                    .where(
                        db.GuildSetting.setting_info ==
                        db.GuildSettingInfo.BUTTON_ROLE_MESSAGE
                    )
                )

                async for role_message_info in setting_stream.scalars():
                    views[role_message_info.guild] = (
                        role_message_info.get_value(),
                        RoleView()
                    )

                role_stream = await session.stream(select(db.ButtonRole))

                async for role_info in role_stream.scalars():
                    views[role_info.guild][1].add_role(role_info)

        for guild_id, (msg_id, view) in views.items():
            self.add_view(view, message_id=msg_id)

        if self.twitch is not None:
            async with self.create_db_session() as session:
                await self.twitch.subscribe(session)

    async def close(self):
        self._logger.info('Closing the bot...')
        await self.session.close()
        await super(DiscordBot, self).close()

    async def run_exclusive(self, func: callable) -> (typing.Any, bool):
        start_time = datetime.utcnow()
        async with self._process_lock:
            result = await self.loop.run_in_executor(
                self.executor,
                func
            )
        end_time = datetime.utcnow()
        should_ping_time = self.bot_config.get('delay_before_ping', -1)
        if should_ping_time < 0:
            return result, False
        return \
            result, end_time - start_time > timedelta(seconds=should_ping_time)

    async def on_message(self, message: discord.Message):
        pass

    async def on_ready(self):
        self._logger.info(f'Ready! I am \'{self.user}\' (ID={self.user.id}).')

    async def on_error(self, event_method: str, /, *args, **kwargs):
        self._logger.error(f'Error in {event_method}.')
        self._logger.error(f'args={args}')
        self._logger.exception(f'kwargs={kwargs}', exc_info=sys.exc_info())

    async def process_commands(self, message: discord.Message, /) -> None:
        """
        Implementation copied from discord.py
        """
        if message.author.bot:
            return

        async with self.create_db_session() as session:
            ctx = await self.get_context(message)
            # Give the session to the context like this for now. At some point
            # in the future the DB will probably also be needed during context
            # creation (prefix fetching etc), which means more copying from
            # discord.py. But that's a problem for future me :)
            ctx.db_session = session
            await self.invoke(ctx)

    async def get_context(
            self,
            /,
            message: discord.Message,
            *,
            cls=CustomContext
    ) -> CustomContext:
        return await super().get_context(message, cls=cls)


async def prefix(bot: DiscordBot, message: discord.Message) -> list[str]:
    if message.guild is None:
        # This is a direct message, so we'll use the default prefix
        return bot.default_prefix

    async with bot.create_db_session() as session:
        async with session.begin():
            setting = await db.GuildSetting.settings_for(
                session,
                message.guild.id,
                settings=db.GuildSettingInfo.COMMAND_PREFIX
            )
            if setting is None:
                extras = [bot.default_prefix]
            else:
                extras = [setting.get_value()]

    return commands.when_mentioned_or(*extras)(bot, message)


async def bot_main(config_path: str):
    # Patch warnings logging so that the full stack is displayed
    _formatwarning = warnings.formatwarning

    def formatwarning_tb(*args, **kwargs):
        s = _formatwarning(*args, **kwargs)
        tb = traceback.format_stack()
        # The -3 gets rid of the things happening inside warnings and logging
        s += ''.join(tb[:-3])
        return s

    warnings.formatwarning = formatwarning_tb

    # Send warnings to logger
    logging.captureWarnings(True)

    config = toml.load(config_path)

    bot_config = config.get('bot')

    pid_file = 'bot.pid'
    if bot_config is not None:
        pid_file = bot_config.get('pid_file', pid_file)

    def cleanup():
        if os.path.isfile(pid_file):
            os.remove(pid_file)

    # Override for debugging purposes
    json_logging_override = os.environ.get('JSON_LOGGING_DISABLE')
    if json_logging_override is None:
        enable_json = bot_config.get('json_logging', False)
    else:
        enable_json = False
    json_logging.init_non_web(
        custom_formatter=BetterJSONLogFormatter,
        enable_json=enable_json
    )

    client = DiscordBot(config)
    if client.tokens is None:
        raise ValueError('At least bot token must be set under [tokens]!')

    secret = client.tokens.get('discord')
    if secret is None:
        raise ValueError("Bot secret must be set in config.toml")

    await client.load_extension('cogs.server')
    await client.load_extension('cogs.events')
    await client.load_extension('cogs.images')
    await client.load_extension('cogs.gdpr')
    await client.load_extension('cogs.system')
    await client.load_extension('cogs.fun')
    await client.load_extension('cogs.utility')
    await client.load_extension('cogs.magick')
    await client.load_extension('cogs.azure')
    await client.load_extension('cogs.mod')
    await client.load_extension('cogs.roles')
    await client.load_extension('cogs.greetings')
    await client.load_extension('cogs.twitch')
    await client.load_extension('cogs.tags')

    atexit.register(cleanup)

    def signal_handler(n, frame):
        print('Stopping')
        global _close_task
        if _close_task:
            return
        _close_task = asyncio.create_task(client.close())
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        signal.signal(signal.SIGTERM, signal.SIG_DFL)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    async with client:
        await client.start(secret)


if __name__ == '__main__':
    asyncio.run(bot_main('../config.toml'))
