import time
import typing

import discord
from discord import ui


class ViewWithDB(ui.View):
    def __init__(self, bot: "DiscordBot", **kwargs):
        super().__init__(**kwargs)
        self.bot = bot

    # Copied from discord.py. Will break.
    async def _scheduled_task(self, item: ui.Item, interaction: discord.Interaction):
        try:
            item._refresh_state(interaction, interaction.data)  # type: ignore

            allow = await self.interaction_check(interaction)
            if not allow:
                return

            if self.timeout:
                self.__timeout_expiry = time.monotonic() + self.timeout

            async with self.bot.create_db_session() as db_session:
                interaction.db_session = db_session
                await item.callback(interaction)
        except Exception as e:
            return await self.on_error(interaction, e, item)
