def patch_discord_state_interaction():
    """
    Patches discord.py's Interaction class to be more receiving towards having an attribute for a database session.

    The class discord.Interaction (implemented in discord/interactions.py) uses the __slots__ class variable, probably
    for memory micro-optimization. This means that new, custom attributes cannot be created on Interaction objects,
    which makes providing a database session inside slash commands or View callbacks extremely difficult.

    To include the database session attribute in an Interaction object, the attribute must be added to __slots__. This
    is not as easy as `Interaction.__slots__ += ('db_session',)` because the __slots__ attribute is only used when the
    class itself is created. This means that we need to basically subclass the original Interaction class and substitute
    the version in discord.py with our subclass.

    This function creates a subclass of Interaction with the same name and additional __slots__ using the three-argument
    type() constructor. The subclass is assigned to discord.state because that's the only place where the Interactions
    are constructed. Note that this means that other instances of the Interaction class (e.g. discord.Interaction,
    discord.interactions.Interaction) are actually references to the _original_ implementation of the Interaction class.
    This is because importing the state subpackage also imports the parent package, which of course imports the original
    class from interactions.py. This should not matter at the moment since as far as I know, discord/state.py is the
    only place where Interactions are actually constructed. This, however, is bound to change and I'm sure my code will
    break at some point...
    """
    import discord.state
    discord.state.Interaction = type('Interaction', (discord.state.Interaction,), {'__slots__': ('db_session',)})
