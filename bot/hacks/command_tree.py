"""
Hack: Subclass CommandTree and override its private _from_interaction method to provide a database session in slash
commands. This mimics the process_commands behavior in the bot subclass, however this uses a private method instead of a
public, documented one. Let's see when this breaks...
"""
import logging

import discord
from discord import app_commands


class HackyCommandTree(app_commands.CommandTree):
    def __init__(self, client):
        super().__init__(client)
        self._logger = logging.getLogger('bot')

    def _from_interaction(self, interaction: discord.Interaction) -> None:
        async def wrapper():
            try:
                async with self.client.create_db_session() as db_session:
                    interaction.db_session = db_session
                    await self._call(interaction)
            except app_commands.AppCommandError as e:
                await self._dispatch_error(interaction, e)

        self.client.loop.create_task(wrapper(), name='HackyCommandTree-invoker')

    async def on_error(self, interaction: discord.Interaction, error: app_commands.AppCommandError):
        orig_error = getattr(error, 'original', error)

        if isinstance(error, app_commands.TransformerError):
            await interaction.response.send_message(f'Parametrin arvo \'{error.value}\' ei kelpaa!', ephemeral=True)
            return

        cmd = interaction.command
        command_name = []
        while cmd is not None:
            command_name.append(cmd.name)
            cmd = cmd.parent
        command_name.reverse()

        self._logger.exception(
            f'Error in app command \'{" ".join(command_name)}\'.',
            exc_info=(
                type(orig_error),
                orig_error,
                orig_error.__traceback__
            )
        )
        await interaction.response.send_message('Jotain meni pieleen! Pingaa Explo.', ephemeral=True)
