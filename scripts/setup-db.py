"""
Script for initializing the bot database
"""


import argparse
from getpass import getpass

from sqlalchemy import create_engine, URL, pool
from sqlalchemy.sql import text


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-H', '--host',
        type=str,
        required=True,
        help='Database host'
    )
    parser.add_argument(
        '-u', '--admin-user',
        type=str,
        required=False,
        help='Database admin user',
        default='postgres'
    )
    parser.add_argument(
        '-d', '--database',
        type=str,
        required=True,
        help='Database name to create'
    )
    parser.add_argument(
        '-p', '--admin-password',
        type=str,
        required=False,
        help='Database admin password',
        default=None
    )
    parser.add_argument(
        '-U', '--bot-user',
        type=str,
        required=True,
        help='Database user to create'
    )
    parser.add_argument(
        '-P', '--bot-password',
        type=str,
        required=False,
        help='Password for the created user',
        default=None
    )

    args = parser.parse_args()
    if args.admin_password is None:
        args.admin_password = getpass('Database admin user password: ')
    if args.bot_password is None:
        args.bot_password = \
            getpass(f'Password for the user to be created ({args.bot_user}): ')

    engine = create_engine(
        URL.create(
            drivername='postgresql+psycopg2',
            username=args.admin_user,
            password=args.admin_password,
            host=args.host
        ),
        poolclass=pool.NullPool,
        echo=True
    )
    with engine.connect() as connection:
        # Hack: CREATE DATABASE cannot be executed inside a transaction
        connection.execute(text("COMMIT"))

        # This is unsafe, but this script is only intended to be manually
        # executed once by the person setting the bot up. SQLAlchemy seems to
        # force the quotes around a string passed through text() as a bind
        # parameter to be single quotes, which PostgreSQL does not accept as
        # identifiers. SQLAlchemy's quoted_name does not work either.
        create_db = text(f'CREATE DATABASE "{args.database}"')
        connection.execute(create_db)

        # Hack: SQLAlchemy internal state thinks it's still inside a transaction
        # block, so commit here
        connection.commit()

        with connection.begin():
            # Do not allow connections to either database by default
            connection.execute(
                text('REVOKE CONNECT ON DATABASE "postgres" FROM public')
            )
            connection.execute(
                text(
                    f'REVOKE CONNECT ON DATABASE "{args.database}" FROM public'
                )
            )

            # Add the bot user
            connection.execute(
                text(
                    f'CREATE USER "{args.bot_user}" '
                    f'WITH ENCRYPTED PASSWORD :password'
                ),
                dict(password=args.bot_password)
            )

            # Remove all default privileges from the created user
            connection.execute(
                text(
                    f'REVOKE ALL PRIVILEGES ON DATABASE postgres '
                    f'FROM "{args.bot_user}"'
                ),
            )
            connection.execute(
                text(
                    f'REVOKE ALL PRIVILEGES ON DATABASE "{args.database}" '
                    f'FROM "{args.bot_user}"'
                ),
            )
            # Allow only connecting to the new database
            connection.execute(
                text(
                    f'GRANT CONNECT ON DATABASE "{args.database}" '
                    f'TO "{args.bot_user}"'
                ),
            )

    engine = create_engine(
        URL.create(
            drivername='postgresql+psycopg2',
            username=args.admin_user,
            password=args.admin_password,
            host=args.host,
            database=args.database
        ),
        poolclass=pool.NullPool,
        echo=True
    )
    with engine.connect() as connection:
        with connection.begin():
            # Allow reading and writing to the new user
            connection.execute(
                text(f'GRANT pg_write_all_data TO "{args.bot_user}"'),
            )
            connection.execute(
                text(f'GRANT pg_read_all_data TO "{args.bot_user}"'),
            )
            # Allow the new user to create tables (TODO: create tables as admin)
            connection.execute(
                text(f'GRANT CREATE ON SCHEMA public TO "{args.bot_user}"'),
            )


if __name__ == '__main__':
    main()
