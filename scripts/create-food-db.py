"""
Converts the food data from Terveyden ja hyvinvoinnin laitos, Fineli into a
readable SQL database.

First, download the "Peruspaketti 2" from https://fineli.fi/fineli/fi/avoin-data
and export its contents to a directory. Then, run this script in that directory.
A file called food.db should appear and there should be no output in terminal.
Place food.db into bot/files/ and you are good to go!
"""

import sqlite3
import csv


CREATE_TABLES = """
PRAGMA FOREIGN_KEYS=ON;

CREATE TABLE food (
    foodid INTEGER PRIMARY KEY NOT NULL,
    foodname TEXT NOT NULL
);

CREATE TABLE eufdname (
    eufdname TEXT PRIMARY KEY NOT NULL,
    descript TEXT NOT NULL
);

CREATE TABLE component_value (
    foodid INTEGER NOT NULL,
    eufdname TEXT NOT NULL,
    bestloc REAL NOT NULL,
    PRIMARY KEY (foodid, eufdname),
    FOREIGN KEY(foodid) REFERENCES food(foodid),
    FOREIGN KEY(eufdname) REFERENCES eufdname(eufdname)
);

"""


def create_tables(cursor):
    cursor.executescript(CREATE_TABLES)


def populate_eufds(cursor):
    rows = []
    with open('eufdname_FI.csv', 'r', encoding='ISO-8859-15') as f:
        reader = csv.reader(f, delimiter=';')
        next(reader)
        for line in reader:
            rows.append((line[0], line[1]))
    cursor.executemany('INSERT INTO eufdname VALUES (?, ?)', rows)


def populate_food(cursor):
    rows = []
    with open('food.csv', 'r', encoding='ISO-8859-15') as f:
        reader = csv.reader(f, delimiter=';')
        next(reader)
        for line in reader:
            rows.append((int(line[0]), line[1]))
    cursor.executemany('INSERT INTO food VALUES (?, ?)', rows)


def populate_values(cursor):
    rows = []
    with open('component_value.csv', 'r', encoding='ISO-8859-15') as f:
        reader = csv.reader(f, delimiter=';')
        next(reader)
        for line in reader:
            try:
                # Damn Finnish decimal separator
                value = float(line[2].replace(',', '.'))
            except ValueError:
                continue
            if value == 0:
                continue
            rows.append((
                int(line[0]),
                line[1],
                value
            ))
    cursor.executemany('INSERT INTO component_value VALUES (?, ?, ?)', rows)


def create_and_populate():
    connection = sqlite3.connect('food.db')
    cursor = connection.cursor()
    create_tables(cursor)
    populate_eufds(cursor)
    populate_food(cursor)
    populate_values(cursor)
    connection.commit()
    connection.close()


if __name__ == '__main__':
    create_and_populate()
