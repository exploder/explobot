import tomlkit


def main():
    with open('requirements-common.txt', 'r') as f:
        req_common = f.readlines()
    with open('requirements-bot.txt', 'r') as f:
        req_bot = f.readlines()[1:]
    with open('requirements-web.txt', 'r') as f:
        req_web = f.readlines()[1:]

    bot_deps = req_common + req_bot
    web_deps = req_common + req_web

    bot_deps = list(map(lambda d: d.strip(), bot_deps))
    web_deps = list(map(lambda d: d.strip(), web_deps))

    with open('bot/pyproject.toml', 'r') as f:
        bot_toml = tomlkit.load(f)

    bot_toml['project']['dependencies'] = bot_deps
    bot_toml['project']['dependencies'] = \
        bot_toml['project']['dependencies'].multiline(True)
    with open('bot/pyproject.toml', 'w') as f:
        tomlkit.dump(bot_toml, f)
        
    with open('web/pyproject.toml', 'r') as f:
        web_toml = tomlkit.load(f)

    web_toml['project']['dependencies'] = web_deps
    web_toml['project']['dependencies'] = \
        web_toml['project']['dependencies'].multiline(True)
    with open('web/pyproject.toml', 'w') as f:
        tomlkit.dump(web_toml, f)

    print(bot_deps)
    print(web_deps)


if __name__ == '__main__':
    main()
