import csv
import pathlib
import argparse


LETTERS = 'abcdefghijklmnoprstuvwyäö'


def count_letters(phrase):
    count = 0
    for char in phrase:
        if char in LETTERS:
            count += 1
    return count


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('wordlist', help='Path to the wordlist file', type=pathlib.Path)
    parser.add_argument('output', help='Path to the output .txt file', type=pathlib.Path)

    args = parser.parse_args()

    word_lengths = set()
    words = []

    with open(args.wordlist, 'r', newline='') as f:
        reader = csv.DictReader(f, delimiter='\t')
        for row in reader:
            word = row['Hakusana'].lower()
            # Sanitization
            if word.startswith('-') or \
                    word.endswith('-') or \
                    'neekeri' in word or \
                    'nekru' in word:
                continue
            length = count_letters(word)
            word_lengths.add(length)
            words.append(f'{length} {word}')

    print(f'Found {len(words)} eligible words')
    print(f'Unique lengths: {word_lengths}')

    with open(args.output, 'w') as f:
        f.write(' '.join([str(num) for num in word_lengths]))
        f.write('\n')
        f.write('\n'.join(words))


if __name__ == '__main__':
    main()
