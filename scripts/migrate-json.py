import argparse
from getpass import getpass

from tinydb import TinyDB
from sqlalchemy import create_engine, URL, pool
from sqlalchemy.orm import sessionmaker

import db


SETTINGS_MAP = {
    'gif-channel': db.GuildSettingInfo.LOG_CHANNEL,
    'delete-channel': db.GuildSettingInfo.DELETE_CHANNEL,
    'gif-delay': db.GuildSettingInfo.GIF_DELAY,
    'image-link-warnings': db.GuildSettingInfo.IMAGE_LINK_WARNING_TOGGLE,
    'prefix': db.GuildSettingInfo.COMMAND_PREFIX,
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-H', '--host',
        type=str,
        required=True,
        help='Database host'
    )
    parser.add_argument(
        '-d', '--database',
        type=str,
        required=True,
        help='Database name to connect'
    )
    parser.add_argument(
        '-u', '--bot-user',
        type=str,
        required=True,
        help='Database user'
    )
    parser.add_argument(
        '-p', '--bot-password',
        type=str,
        required=False,
        help='Password for the user',
        default=None
    )
    parser.add_argument(
        '-j', '--json-file',
        type=str,
        required=True,
        help='Path to the JSON file to migrate'
    )

    args = parser.parse_args()
    if args.bot_password is None:
        args.bot_password = \
            getpass(f'Password for the user: ')

    engine = create_engine(
        URL.create(
            drivername='postgresql+psycopg2',
            username=args.bot_user,
            password=args.bot_password,
            host=args.host
        ),
        poolclass=pool.NullPool,
        echo=True
    )
    session_maker = sessionmaker(engine)

    json_db = TinyDB(args.json_file)
    guilds = json_db.table('guilds')
    with session_maker.begin() as session:
        for g in guilds:
            guild_id = g.pop('id')
            for key, setting_info in SETTINGS_MAP.items():
                value = g.get(key)
                if value is None:
                    continue
                if isinstance(value, bool):
                    value = int(value)
                setting = db.GuildSetting(
                    guild=guild_id,
                    setting_info=setting_info,
                    value=str(value)
                )
                session.merge(setting)


if __name__ == '__main__':
    main()
