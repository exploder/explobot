from packaging.version import Version

import requirements
import requests


def main():
    with open('requirements.txt', 'r') as f:
        reqs = list(requirements.parse(f))
    needs_update = []

    for req in reqs:
        for spec in req.specs:
            if spec[0] == '==':
                current_ver = Version(spec[1])
                break
        else:
            continue
        resp = requests.get(f'https://pypi.org/pypi/{req.name}/json')
        data = resp.json()
        latest_ver = Version(data['info']['version'])
        if latest_ver > current_ver:
            needs_update.append((req.name, current_ver, latest_ver))
    needs_update = set(needs_update)
    name_width = max(len(u[0]) for u in needs_update)

    print(f'Name{" " * (name_width - 3)}Old version  New version')
    for req in needs_update:
        print(f'{req[0]:<{name_width}} {str(req[1]):>11}  {str(req[2]):>11}')


if __name__ == '__main__':
    main()
