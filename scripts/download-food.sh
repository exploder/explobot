#!/bin/bash

set -ex
if [[ -z "${TOKEN}" ]]
then
  wget -O food.zip https://fineli.fi/fineli/content/file/49
else
  wget --header="JOB-TOKEN: ${TOKEN}" -O food.zip "https://gitlab.com/api/v4/projects/32801858/packages/generic/fineli-food/0.0.1/food.zip"
fi
unzip food.zip
python create-food-db.py
rm *.csv *.txt *.zip
