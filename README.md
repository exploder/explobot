# What's this?

This is a Discord bot, created by me.
It has a web control panel, but it can also be run without it.

## How to run with `docker-compose`

The recommended way to run the bot is to use [`docker-compose`](https://docs.docker.com/compose/).
A compose file is provided with the repo, and this README outlines the process.
Docker images are provided for x86_64 and ARM64 architectures on Docker Hub ([bot](https://hub.docker.com/r/exploder98/explobot), [web](https://hub.docker.com/r/exploder98/explobot-web)), and you can build your own ones for systems with different architectures.
It is possible to run the bot without Docker, but it is not recommended.

**NOTE:** Due to Docker Hub deleting inactive images, I'm also maintaining a private copy of all images.
The CI pipelines that push to Docker Hub also push to my private registry.
If you need access to an image of some older tagged version, please let me know.

### 1. Clone this repo

```commandline
git clone https://gitlab.com/exploder/explobot.git
cd explobot
```

### 2. Create configuration files

You need to create the following files in the repo root:

- `config.toml` (an example file, `config.example.toml`, is provided)
- `users.json` (empty)

In addition, you need to create a file called `.env` and provide values for the following items:
- `POSTGRES_DATA`: Path to the directory containing data files for the PostgreSQL database
- `POSTGRES_PASSWORD`: Password for the `postgres` user. Recommended to be complex.

### 3. Set up the PostgreSQL database

To run the bot, a PostgreSQL database needs to be set up with a database and user for the bot.
A setup script is provided within this repo, in `scripts/setup_db.py`.
First, run the PostgreSQL Docker image standalone, mounting the data directory:

```commandline
docker run --rm -it -v /data/dir/path:/var/lib/postgresql/data --env-file .env -p 127.0.0.1:5432:5432 postgres:15
```

Then, in another terminal, run the setup script.
Use `--help` to get descriptions for all the parameters.

```commandline
python scripts/setup-db.py \
    --host 127.0.0.1 \
    --database explobot \
    --bot-user explobot \
    --bot-password SomethingReallyComplex
```

After the script has been executed, you can kill the postgres container (ctrl-C) and proceed to configure the SQLAlchemy connection string in `config.toml` so that it corresponds to database, username and password you just created.

### 4. Start the stack with `docker-compose`

```commandline
docker-compose up
```

This command should first pull the images and then run them.
The web panel should be accessible from `localhost:8000`.
If you want to have it available to the outside world, I recommend setting up an [nginx reverse proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/).

### 5. Create an user for the web panel

When you run the web panel for the first time, a *Registration token* will be printed to the console:

![Registration token](images/registration_token.png)

This token is randomized, single-use and it's needed for registering a new user for the web panel, so copy it to your clipboard.
To create a user, navigate to `localhost:8000/register`, paste in the token and choose a username and a good password.
After creating a user, you should be redirected to `/login` so that you can login with your fresh credentials.

Note that currently you can only create one user this way.
Also, the method I use for persisting the token is quite crappy (absolutely not process-safe, so if you start multiple `gunicorn` workers you will see several tokens...) and I will change it at some point, probably when I manage to implement the SQL database (#32).
Mayyybe I'll then allow multiple users to be created this way?

### 6. Re-run the stack in the background

If you don't want to have a terminal open all the time when running the bot, you should run the stack in the background.
To do that, kill the stack (ctrl-C) and then run
```commandline
docker-compose up -d
```

## Configuration

This bot and the webserver are configured using [TOML](https://github.com/toml-lang/toml).
`config.toml` must be in the project root directory.
To configure the bot, you need to rename `config-example.toml` to `config.toml` and populate at least values that are needed for the bot to work.
The bare minimum is `discord` under `[tokens]` and `connection` under `[sqlalchemy]`.
The values are documented in the following tables:

### `[tokens]`

This table holds all tokens the bot will use.
These values should usually **NOT** be published, **not even accidentally.**

| Key              | Purpose                                                                                                                                                         | Example value |
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| `discord`        | The Discord secret for the bot. **DO NOT PUBLISH THIS VALUE (commit to git, etc.), WITH THIS TOKEN ANYONE CAN CONTROL YOUR BOT.**                               | -             |
| `gitlab`         | Your personal Gitlab Access token with API access. **DO NOT PUBLISH THIS VALUE (commit to git, etc.), WITH THIS TOKEN ANYONE CAN CONTROL YOUR GITLAB ACCOUNT.** | -             |
| `openweathermap` | Your [OpenWeatherMap](https://openweathermap.org/api) API token (used for the weather command).                                                                 | -             |
| `geoapify`       | Your [Geoapify](https://myprojects.geoapify.com/) map API token (currently only used for the weather command).                                                  | -             |
| `myanimelist`    | Your [MyAnimeList](https://myanimelist.net/) API Client ID. Used for anime/manga search.                                                                        | -             |

### `[server]`

This table controls the server.

| Key            | Purpose                                                                                                                                                                                                                                                                                                                                  | Example value              |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
| `flask_secret` | Base64-encoded [Flask secret key](https://flask.palletsprojects.com/en/1.1.x/api/#flask.Flask.secret_key) used for Cookie signing and other cryptographics. Defaults to `0FLWj2F9jO/EMzRdqQnp5Q==`. While it's generally safe to expose development configurations for this, you **SHOULD NOT** expose the version used in "production". | `0FLWj2F9jO/EMzRdqQnp5Q==` |

### `[bot]`

This table controls the bot.

| Key                  | Purpose                                                                                                                                                                           | Example value                                    |
|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| `prefix`             | The default prefix for Discord bot commands. Defaults to `?` if unfilled.                                                                                                         | `!`                                              |
| `json_logging`       | Enables or disables JSON logging. Needs to be truthy for the web panel to work.                                                                                                   | `1`                                              |
| `pid_file`           | The file where the bot process' Process ID (PID) should be stored. Used by the webserver to stop the bot process and access its state. Defaults to `bot/bot.pid` (relative path). | `/tmp/bot.pid`                                   |
| `hangman_timeout`    | How long the bot will wait (in seconds) for a reaction while a hangman game is ongoing. Defaults to 60.                                                                           | `100`                                            |
| `safe_image_domains` | A list of domains that are ignored while scanning for image links to warn about. At least you should put Discord domains here.                                                    | `["cdn.discordapp.com", "media.discordapp.net"]` |
| `delay_before_ping`  | Describes how long image/gif/etc processing should take before the user who invoked the command is mentioned. Set to negative or remove from config to disable.                   | `30`                                             |

#### `[bot.circus]`

This section contains information about the `circusd` process.
You only need to change this if you have changed `circus.ini`.

| Key    | Purpose                                                              | Example value |
|--------|----------------------------------------------------------------------|---------------|
| `host` | The host of the `circusd` control endpoint. Defaults to `127.0.0.1`. | `localhost`   |
| `port` | The port of the `circusd` control endpoint. Defaults to `5555`.      | `1234`        |

#### `[bot.azure]`

This section contains information that the bot uses to connect to a Function App running on Microsoft Azure.
This is used for GPU processing in Azure Kubernetes Services (AKS).
Currently, the only command using these values is the damedane command.
If you don't know what these are about, don't try to fill these.

| Key            | Purpose                                                                                                                                                                              | Example value                        |
|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `key`          | Azure Function (default) key that is needed to execute the functions. **DO NOT PUBLISH THIS VALUE (commit to git, etc.), WITH THIS KEY ANYONE CAN CALL (AND ABUSE) YOUR FUNCTIONS.** | -                                    |
| `function_url` | The URL for the Function App.                                                                                                                                                        | `https://app-name.azurewebsites.net` |
| `skip_cluster` | Whether to skip Kubernetes (AKS) cluster startup. Intended for use in local testing.                                                                                                 | `true`                               |

### `[gitlab]`

This table controls Gitlab related information.
Currently this is only used for creating new issues with commands.

| Key       | Purpose                                                                                                                                   | Example value |
|-----------|-------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| `project` | The ID or path of the project you want to create issues in with the bot. You need to have permissions for creating issues in the project. | `123456789`   |

### `[sqlalchemy]`

SQLAlchemy configuration, currently contains the URL for connecting to a database.
As of version 2.0.1, SQLAlchemy only supports PostgreSQL with an async backend, so this needs to be a Postgres database URL.

| Key          | Purpose                                                                                                                                                                                                      | Example value                                        |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|
| `connection` | SQLAlchemy connection string for async postgres database. **DO NOT PUBLISH THIS VALUE (commit to git, etc.), WITH THIS URL ANYONE CAN CONNECT TO YOUR DATABASE IF IT'S PUBLICLY AVAILABLE ON THE INTERNET.** | `postgresql+asyncpg://user:pass@domain/databasename` |

### `[twitch]`

Contains information related to Twitch integration (Stream online notifications).

| Key               | Purpose                                                                                                                | Example value |
|-------------------|------------------------------------------------------------------------------------------------------------------------|---------------|
| `app_id`          | Twitch application ID                                                                                                  | -             |
| `app_secret`      | Twitch application secret                                                                                              | -             |
| `eventsub_secret` | A string that the EventSub client uses to make sure the messages it receives are authentic.                            | `Very secret` |
| `webhook_url`     | The URL that Twitch should call when when the event is triggered.                                                      | -             |
| `port`            | The local port the EventSub client should listen on. This port needs to be forwarded (reverse proxy) to `webhook_url`. | `8008`        |

## How to run without Docker (outdated; note: I didn't really check this works, please use Docker)

To run this bot, you need to create an application in the [Discord developer portal](https://discordapp.com/developers/) and enable the bot account.

I recommend creating a virtual environment and installing the dependencies using pip (note the custom Circus repository):

```commandline
pip install git+https://gitlab.com/exploder/circus.git
pip install -r requirements.txt
pip install bot/
```
When you have created a `config.toml`, you can start up the bot in the `bot` subdirectory (important so that the bot finds assets):

```commandline
explobot path/to/config.toml
```

By default, JSON logging is turned off.
This is to make debugging easier.
If you want to use JSON logging (or use the web panel for viewing the log, then it's mandatory) you'll need to set `json_logging` to `1` in `config.toml`.
If you want to disable JSON logging even if it was enabled in `config.toml`, you can set the `JSON_LOGGING_DISABLE` environment variable.
This disables JSON logging if set.

### Running using Circus

You can also use [Circus](https://circus.readthedocs.io), a process manager, to run the bot (and if you want to use the web panel, it's mandatory).
Note that you'll need to use [my fork](https://gitlab.com/exploder/circus), because the web panel needs a command I have added.
If you installed the requirements from `requirements.txt`, you should have it already installed properly.

To run the bot, first you need to run `circusd` and point it to the provided configuration file, `circusd.ini`:

```commandline
circusd circus.ini
```

This will start `circusd` in the foreground.
If you want it to go to the background, use the `--daemon` flag:

```commandline
circusd --daemon circus.ini
```

After you have started `circusd`, you can use `circusctl` to start or stop the bot:

```commandline
# This starts the bot
circusctl start bot

# This stops the bot
circusctl stop bot
```

If you invoke `circusctl` without any parameters, you'll enter a shell where you can type `help` and see all available commands.
You can also [read the source code](https://gitlab.com/exploder/circus/-/tree/add-readfile-command/circus/commands) to find docstrings for the commands.
The Circus documentation generation is currently broken for the commands so you won't find their documentation on the website (I plan to open a pull request to fix this).

### Running the web panel (and initial user creation)

To run the web panel (Flask based), you need to set at least the following configuration variables:

- `json_logging` to `1` (without this the log view won't work)
- `flask_secret` to a secret generated by you

Additionally, you'll need to have `circusd` running as described in the previous section.
If you changed the control endpoint in `circus.ini`, you'll need to edit the configuration to match the new endpoint.

Then, use [Gunicorn](https://gunicorn.org/) to run the Flask app:

```commandline
gunicorn application:app
```

*Side note: If you want to connect to the panel outside your computer, you should add `--bind 0.0.0.0` to the parameter list (before `application:app`).*
*I also use `--timeout 600` but it isn't necessary.*

Then create the initial user as explained in the docker-compose section above.

## Versioning

This project uses `major.minor.patch` versioning which may resemble [semantic versioning](https://semver.org/).
However, this bot does not have an API.
These versions are incremented as follows:

1. `major` is incremented when some big changes (e.g. totally new database) are introduced.
2. `minor` is for additions and other changes.
3. `patch` is for fixes.

Changelog is available both in English and in Finnish, however unreleased changes will mostly be documented only in the English version.

## Found a bug? Suggestions?

Cool, just [open an issue](https://gitlab.com/exploder/explobot/issues/new).

## License

This project is licensed under the GNU AGPLv3 license, except for the wordlist located at `bot/files/fi.txt`.
This file is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed).
That file is based on the year 2022 version of the wordlist by [Institute for the Languages of Finland](https://www.kotus.fi/aineistot/sana-aineistot/nykysuomen_sanalista) (Kotimaisten kielten keskus or KOTUS in Finnish).
