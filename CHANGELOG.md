# Explobot changelog

## 2.6.3 (2024-12-30)

- updated Docker python to 3.12

## 2.6.2 (2024-12-30)

- add `!mute` and `!tempmute` aliases to `!timeout`
- minor fixes

## 2.6.1 (2024-05-04)

- made bot difficulty visible in tictactoe messages
- fixed tag name autocomplete failing with long (>100 characters) tag names

## 2.6.0 (2024-04-30)

### Additions

- added `!tictactoe` with two players or against a bot
- added `/tag list` for listing own tags
- added `/ban`
- added `!bingchilling` deepfake
- added Lottie sticker support for image commands

### Fixes

- misc fixes to tag system
- fixed transparency in `!gif-makia`
- fixed image commands sometimes not accepting linked images (e.g. Tenor)

## 2.5.2 (2023-09-24)

- fixed `/tag global` so that it works

## 2.5.1 (2023-09-24)

- fixed tag system so that users can actually create them

## 2.5.0 (2023-09-24)

- TAG SYSTEM! WITH SLASH COMMANDS!
  - `/tag add`, `/tag show` etc.
  - `!t` works for showing tags
- Comics from Helsingin Sanomat (`!fingerpori` etc.) and XKCD (`!xkcd`, duh)
- Teksti-TV (`!teksti-tv` or `!ttv`)
- fixed hangman not providing the Kotus link on timeout
- limited MAL query length to 64 characters due to API limitations

## 2.4.0

- added !anime and !manga search (MyAnimeList)
- upgraded Python to 3.11 in Docker images

## 2.3.0

- added Twitch live notifications

## 2.2.0

- updated hangman wordlist to the year 2022 release
- hangman now prefills unguessable characters instead of ignoring those words altogether
- added "search kotus dictionary" button after a hangman game ends
- added join and leave greetings to replace mee6
- migrated to Pillow 10.0.0 and modified image processing to be done properly
- increased GDPR ZIP file size due to minimum upload size increase

## 2.1.0

- migrate everything to postgres
- fix embed warnings being emitted when there are several embeds in a message

## 2.0.1

- fix psycopg2 requirement for aarch64 to work

## 2.0.0

- reaction roles
- PostgreSQL database
- things are Python modules now

## 1.16.0

- support for the actual 2.0 version of discord.py
- internal code cleanup
- buttons for `!hangman`
- new Discord's timeout-based mute commands

## 1.15.0

- changes from 1.14.0 actually working
- Docker is now the preferred method to run the bot, images and compose are provided

## 1.14.0

- support for the last discord.py version
- sticker support for image commands

## 1.13.0

- added `!tainted` deepfake

## 1.12.0

- added `!päivänannos`
- tweaked Azure to report cluster status better

## 1.11.0

- added `!egg`, a new deepfake command

## 1.10.1

### Changes

- added `gifv` as a supported embed type for image commands
- also check message content for a link

## 1.10.0

### Changes

- improvements to robustness of azure function calls
- do not show cluster on when it is not
- links that do not go directly to an image (tenor) are now supported!

### Additions

- `!8ball` command

## 1.9.1

### Changes

- better !queue: shows processing jobs, shows whose job is which

### Additions

- added !sairasta, a sick command

## 1.9.0

### Additions

- added !riseandshine deepfake command

### Fixes

- fixed bot trying to use video files as images

## 1.8.5

### Fixes

- hopefully fixed crash when there are no jobs left but the status still fires

## 1.8.4

### Additions

- !cancel and !queue commands to cancel jobs and show the job queue
  - only own jobs can be cancelled

## 1.8.3

### Fixes

- fixed erroneous pings in PreviousImage

## 1.8.2

### Additions

- remote error handling for damedane

## 1.8.1

### Additions

- better error handling for damedane

## 1.8.0

### Additions

- !damedane command that does a deepfake
  - this uses Azure so it is slow (cluster startup takes a lot of time)

## 1.7.0

### Changes

- decoupled the web panel from the bot
  - [Circus](https://circus.readthedocs.io) is now used to run the bot instead of using subprocesses
- FrameCount accepts any number of frames

### Fixes

- fixed the coinflip command not working

## v1.6.0

### Changes

- changed almost all bot interaction to use replies

## v1.5.0

### Additions

- added a new first-time registration to the web panel
- added actual functionality to the buttons in the log view

### Changes

- made web panel more mobile friendly
- changed the API route for the log to not trip uBo
- made commands case-insensitive

### Fixes

- fixed `noloop` flag working for animated GIF input

## v1.4.3

### Changes

- reworked gif command exclusive running
- made warnings to to a proper logger

## v1.4.2

### Additions

- added !gif-makia flags
  - `noloop` prevents the result from looping
  - `reverse` makes the gif go back to original after the magic
  - `<number>f` makes the gif to be `number` frames long (e.g. `30f`)
- added the ability to use previously posted content in makia commands and in !tank
  - use the `^`-character as target to use the previous image posted in channel
- more debug logging in imagemagick processors for better progress following

### Changes

- changed the !loglevel command to support specifying the logger

## v1.4.1

### Fixes

- fixed !gif-makia errors
- better error handling to image fetching
- additional debugging to !gif-makia to maybe debug something

## v1.4.0

### Additions

- !makia, !gif-makia and !gif-makia2 commands!
  - do magic to normal pictures and gifs!
  - try !gif-makia2 for a different magical experience.
  - all commands support using a number to adjust the effect intensity
  - in !gif-makia, it's also possible to adjust the amount of sharpening by using another number!
  - target image specification works the same way as in !tank (uses profile picture by default, supports attachments, pings, names and links)
- the weather command now shows a map showing where the place the user searched for is
  - map zoom level can be adjusted by adding a number before the query
  - map can be completely disabled by adding `nomap` to the end of your query

### Changes

- the weather command embed now uses city name and country code for its title
- the bot now types during the processing of the weather command

## v1.3.4

### Fixes

- updated the bot to use Gateway Intents
- updated packages

## v1.3.3

### Fixes

- updated Pillow to 8.0.1

## v1.3.2

### Changes

- the bot now ignores halloween bot commands for delete log (h!trick and h!treat)

## v1.3.1

### Fixes

- added error handling to Höpsismi command
- fixed original text showing in Höpsismi image

## v1.3.0

### Additions

- höpsismi command

### Fixes

- fixed hidden link detection not detecting all whitespace

## v1.2.0

### Additions

- added a feature to warn about image links that get hidden by Discord client
  - this feature is turned off by default, and can be turned on with `!image-link-warn`
  - the bot ignores links if the domain is one of the "safe" domains configured in `config.toml`

## v1.1.1

- updated discord.py to 1.3.4 to fix crashes

## v1.1.0

Improvements to the web app and logging:

### Change

- different log levels are now colored differently in the log viewer

### Addition

- added a command for changing log level (!loglevel)

## v1.0.1

This is a small bugfix release.

### Fixes

- fixed hangman error messages not showing up to users
- others' reactions are now removed while someone is playing hangman if the player has not allowed everyone to guess

## v1.0.0

This is the first versioned release, woo!

### Additions

- added a hangman command
  - type !hangman to start the game
  - use reactions to guess letters
  - parameters: one number gives exactly that length, 2 numbers gives a word between those numbers and third parameter allows everyone to react
  - Uses ~93k Finnish words
- added a weather command
  - use !weather to query weather in your favorite location!
  - uses OpenWeatherMap's API

### Changes

- the bot now uses TOML for configuration instead of some shitty environment variables
- the bot now notifies user if they use quotations incorrectly in !moment commands
