/* jshint moz: true, multistr: true */
var log = {};
var last_line = -1;
var intervalId = null;
var auto_scroll = true;
var rotated = false;

$(function(e) {
    $.fn.appendText = function(text) {
        return this.each(function() {
            var textNode = document.createTextNode(text);
            $(this).append(textNode);
        });
    };
    $(".alert").alert();

    if (document.location.pathname === "/") {
        preparePanel();
    } else if (document.location.pathname === "/register") {
        prepareRegistrationForm();
    }
});

function prepareRegistrationForm() {
    $("#repeat_password").on("keyup", function () {
        let pw = $("#password").val();
        let confirm = $("#repeat_password").val();

        if (pw === "" || confirm === "") {
            $("#submit").attr("disabled", "");
            $("#repeat_password").removeClass("is-invalid");
            $("#repeat_password").removeClass("is-valid");
            return;
        }

        if (pw === confirm) {
            $("#submit").removeAttr("disabled");
            $("#repeat_password").addClass("is-valid");
            $("#repeat_password").removeClass("is-invalid");
        } else {
            $("#repeat_password").removeClass("is-valid");
            $("#repeat_password").addClass("is-invalid");
        }
    });

    $("#password").on("keyup", function () {
        let pw = $("#password").val();
        let confirm = $("#repeat_password").val();

        if (confirm === "" || pw === "") {
            $("#submit").attr("disabled", "");
            $("#repeat_password").removeClass("is-invalid");
            $("#repeat_password").removeClass("is-valid");
            return;
        }

        if (pw === confirm) {
            $("#submit").removeAttr("disabled");
            $("#repeat_password").addClass("is-valid");
            $("#repeat_password").removeClass("is-invalid");
        } else {
            $("#repeat_password").removeClass("is-valid");
            $("#repeat_password").addClass("is-invalid");
        }
    });
}

function preparePanel() {
    $("#start").on("click", function () {
        $.post("/api/control", {
            action: "start"
        }, function () {
            $("#start").prop("disabled", true);
            if (rotated) {
                rotate();
            }
            updateStatus();
        });
    });

    $("#stop").on("click", function () {
        $.post("/api/control", {
            action: "stop"
        }, updateStatus);
    });

    $("#log-reload").on("click", function() {
        updateLog();
    });

    $("#log-download").on("click", function() {
        window.location.href = "/download_log";
    });

    setInterval(updateStatus, 2000);
    updateStatus();
}

function rotate() {
    $(".log-text").wrapAll($('<div/>', {
        "class": "rotated-text"
    }));
    $("#log-area hr").remove();
    $('<hr>', {
        "class": "rotate-divider"
    }).appendTo("#log-area");
    last_line = -1;
    log = {};
}

function updateStatus() {
    $.get("/api/status", function(resp) {
        let status = resp.status;
        $("#status").text(status);
        if (status === "active") {
            if (intervalId === null) {
                intervalId = setInterval(updateLog, 2000);
                updateLog();
            }
            $("#start").prop("disabled", true);
            $("#stop").prop("disabled", false);
        } else if (status === "stopped") {
            if (intervalId !== null) {
                clearInterval(intervalId);
                intervalId = null;
            }
            $("#stop").prop("disabled", true);
            $("#start").prop("disabled", false);
            last_line = -1;
            rotated = true;
        }
    });
}

function updateLog() {
    $.ajax({
        url: "/api/botlog",
        method: "GET",
        data: { last_line: last_line }
    }).done(function(data) {
        var update = true;
        if (data.error !== undefined && data.error) {
            $("#log-area").empty();
            $("<h1/>", {
                "class": "row white-text justify-content-center",
                text: "Error loading the log",
                id: "error-text"
            }).appendTo("#log-area");
            update = false;
        } else {
            $("#error-text").remove();
        }
        if (rotated) {
            rotated = false;
            update = false;
        }
        if (update) {
            for (var line in data) {
                if (!isInt(line)) {
                    continue;
                }
                var colorClass = "";
                switch (data[line].level) {
                    case "CRITICAL":
                        colorClass = "message-crit";
                        break;

                    case "ERROR":
                        colorClass = "message-err";
                        break;

                    case "WARNING":
                        colorClass = "message-warn";
                        break;

                    case "DEBUG":
                        colorClass = "message-debug";
                        break;

                    default:
                        colorClass = "message-info";
                        break;
                }
                getMessageElement(
                    data[line].written_at,
                    data[line].msg,
                    colorClass)
                .appendTo("#log-area");

                if (data[line].exc_info !== undefined) {
                    getMessageElement(
                        data[line].written_at,
                        data[line].exc_info,
                        colorClass)
                    .appendTo("#log-area");
                }
                last_line = line;
            }
        }

        if ($("#auto-scroll").prop("checked")) {
            $("#log-wrapper").scrollTop($("#log-wrapper")[0].scrollHeight);
        }
    }).fail(function(xhr, stat, err) {
        console.log(xhr);
        console.log(stat);
        console.log(err);
    });
}

function getMessageElement(time, msg, messageTextClass) {
    let div = $("<span/>", {
        "class": "white-text",
        text: "["
    })
    .wrap("<p/>")
    .parent()
    .append($("<span/>", {
        "class": "time",
        text: time
    }))
    .append($("<span/>", {
        "class": "white-text",
        text: "]"
    }))
    .wrap($("<div/>", {
        "class": "log-text d-flex"
    }))
    .parent();

    $("<span/>", {
        "class": messageTextClass + " message d-block",
        text: msg.replace(/\\n/g, '\n')
    })
    .wrap($("<p/>"))
    .parent()
    .appendTo(div);

    return div;
}

function isInt(value) { // Some magicc
    return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value));
}
