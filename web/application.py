import base64
import os
import logging
import json
import typing

import sys
from urllib.parse import urlparse, urljoin
from io import StringIO, BytesIO

import toml
from tinydb import Query, TinyDB
from wtforms import validators, StringField, PasswordField
from flask import (
    Flask,
    render_template,
    request,
    jsonify,
    flash,
    abort,
    redirect,
    url_for,
    send_file,
    Response
)
from flask_login import (
    LoginManager,
    UserMixin,
    login_required,
    login_user,
    logout_user,
    login_url
)
from flask_wtf import FlaskForm
from flask_bcrypt import Bcrypt
from circus.client import CircusClient


app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False  # Does not crash with str and int keys

config = toml.load('config.toml')
server_config = config.get('server')

secret_key = b'\xd0R\xd6\x8fa}\x8c\xef\xc434]\xa9\t\xe9\xe5'

if server_config is not None:
    secret_key = base64.b64decode(
        server_config.get('flask_secret', base64.b64encode(secret_key))
    )

app.secret_key = secret_key
logger = logging.getLogger('application')
logger.setLevel(logging.INFO)
form = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s')
hand = logging.StreamHandler(sys.stdout)
hand.setFormatter(form)
logger.addHandler(hand)

DISCORD_LOG = 'bot/discord.log'

bot_config = config.get('bot')

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

bcrypt = Bcrypt(app)

usr_db = TinyDB('users.json')

regtoken = None

circus_config = bot_config.get(
    'circus',
    {
        'host': '127.0.0.1',
        'port': 5555
    }
)

circus_host = circus_config.get('host', '127.0.0.1')
circus_port = circus_config.get('port', 5555)

client = CircusClient(endpoint=f'tcp://{circus_host}:{circus_port}')

if len(usr_db.all()) == 0:
    print('No users registered!')
    regtoken = base64.b64encode(os.urandom(32)).decode("utf-8")
    print(f'Registration token: {regtoken}')


class User(UserMixin):
    def __init__(self, username: str, password: str):
        self.username = username
        self.password = password

    def get_id(self) -> str:
        return self.username

    @classmethod
    def get(cls, name: str):
        db_user = Query()
        usr = usr_db.get(db_user.name == name)
        if not usr:
            return None
        return User(name, usr['password'])

    def save(self):
        usr_db.insert({'name': self.username, 'password': self.password})


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[validators.DataRequired()])
    password = PasswordField('Password', validators=[validators.DataRequired()])


class RegisterForm(FlaskForm):
    token = StringField(
        'Register token',
        validators=[validators.DataRequired()]
    )
    username = StringField('Username', validators=[validators.DataRequired()])
    password = PasswordField(
        'Password',
        validators=[
            validators.DataRequired(),
            validators.EqualTo(
                'repeat_password',
                message='Passwords must match!'
            )
        ]
    )
    repeat_password = PasswordField(
        'Repeat password',
        validators=[validators.DataRequired()]
    )


def make_hash(password: str) -> str:
    hashed = bcrypt.generate_password_hash(password)
    return hashed.decode('utf-8')


def check_hash(correct_hash: str, password: str) -> bool:
    return bcrypt.check_password_hash(correct_hash.encode('utf-8'), password)


def is_safe_url(url: str) -> bool:
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, url))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


@login_manager.user_loader
def load_user(user_id: int) -> User:
    return User.get(user_id)


@login_manager.unauthorized_handler
def unauthorized() -> Response:
    parsed = urlparse(request.url)
    if parsed.path.startswith('/api'):
        abort(403)
    else:
        flash(
            login_manager.login_message,
            category=login_manager.login_message_category
        )
        redirect_url = login_url(login_manager.login_view, next_url=request.url)
        return redirect(redirect_url)


@app.route("/")
@login_required
def bot_dashboard() -> str:
    return render_template('bot.html')


@app.route('/ping')
def ping() -> str:
    return 'pong!'


@app.route('/login', methods=['GET', 'POST'])
def login() -> typing.Union[Response, str]:
    login_form = LoginForm()
    if login_form.validate_on_submit():
        user = User.get(login_form.username.data)
        if user:
            if check_hash(user.password, login_form.password.data):
                login_user(user)
                next_path = request.args.get('next')
                if not is_safe_url(next_path):
                    abort(400)
                return redirect(next_path or url_for('bot_dashboard'))
    if request.method == 'POST':
        flash(
            'Incorrect username or password.',
            category=login_manager.login_message_category
        )
    return render_template('login.html', form=login_form)


@app.route('/register', methods=['GET', 'POST'])
def register() -> typing.Union[Response, str]:
    # Yes, this is shit, but how else should I do it?
    # Yeah probably with some cache or a database
    global regtoken
    if regtoken is None:
        abort(401)
    register_form = RegisterForm()
    if register_form.validate_on_submit():
        if register_form.token.data != regtoken:
            flash('Incorrect token or username.')
            return render_template('register.html', form=register_form)
        regtoken = None
        user = User(
            register_form.username.data,
            make_hash(register_form.password.data)
        )
        user.save()
        flash('User created, now you can log in.')
        return redirect('/login')
    return render_template('register.html', form=register_form)


@app.route('/logout')
def logout() -> Response:
    logout_user()
    return redirect(url_for('login'))


@app.route('/download_log')
@login_required
def download_log() -> Response:
    zmq_resp = client.send_message('readfile', filepath='bot/discord.log')
    print(zmq_resp)

    proxy = StringIO()
    proxy.write('\n'.join(zmq_resp['lines']))

    file_bytes = BytesIO()
    file_bytes.write(proxy.getvalue().encode('utf-8'))
    file_bytes.seek(0)

    proxy.close()

    return send_file(
        file_bytes,
        as_attachment=True,
        mimetype='text',
        max_age=-1,
        download_name='discord.log'
    )


@app.route('/api/<name>', methods=['GET', 'POST'])
@login_required
def api(name: str) -> Response:
    response = {}
    if name == 'status' and request.method == 'GET':
        zmq_resp = client.send_message('status', name='bot')
        response['status'] = zmq_resp['status']

    elif name == 'botlog' and request.method == 'GET':
        min_num = -1
        last_line = request.args.get('last_line')
        if last_line is not None:
            try:
                min_num = int(last_line)
            except TypeError:
                logger.error(
                    f'Incorrect param in /api/log: last_line={last_line}'
                )
                min_num = -1
            except ValueError:
                logger.error(
                    f'Incorrect param in /api/log: last_line={last_line}'
                )
                min_num = -1

        if min_num < 0:
            min_num = -1

        zmq_resp = client.send_message(
            'readfile',
            filepath='bot/discord.log',
            skiplines=min_num + 1
        )

        if zmq_resp['status'] == 'ok':
            current_line = min_num
            for line in zmq_resp['lines']:
                current_line += 1
                line_dict = json.loads(line.replace('\\n', '\\\\n'))
                response[current_line] = line_dict
        else:
            response['error'] = True

    elif name == 'control' and request.method == 'POST':
        if request.form['action'] == 'start':
            client.send_message('start', name='bot')

        elif request.form['action'] == 'stop':
            client.send_message('stop', name='bot')

    return jsonify(response)
