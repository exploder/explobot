# Explobotin muutosloki

## 2.6.3 (2024-12-30)

- päivitetty Dockerin python 3.12:een

## 2.6.2 (2024-12-30)

- lisätty `!mute` ja `!tempmute` -aliakset `!timeout`-komentoon
- pikkukorjauksia

## 2.6.1 (2024-05-04)

- lisätty botin vaikeusaste näkyviin ristinollaan
- korjattu tagien automaattitäytön epäonnistuminen, jos tageilla on liian pitkiä (>100 merkkiä) nimiä

## 2.6.0 (2024-04-30)

### Lisäykset

- lisätty `!ristinolla` kahdelle pelaajalle tai bottia vastaan
- lisätty `/tag list` omien tagien listaukseen
- lisätty `/ban`
- lisätty `!bingchilling` deepfake
- lisätty tuki Lottie-tarrojen käytölle kuvakomennoissa

### Korjaukset
 
- lisää korjauksia tageihin
- korjattu läpinäkyvyys `!gif-makia`:ssa
- korjattu joidenkin kuvalinkkien (esim. Tenor) epäkelpoisuus kuvakomennoissa

## 2.5.2 (2023-09-24)

- korjattu `/tag global`

## 2.5.1 (2023-09-24)

- korjattu tagisysteemi niin, että ihmiset pystyy oikeesti tekemään niitä

## 2.5.0 (2023-09-24)

- TAGISYSTEEMI! SLASH-KOMENNOILLA!
  - `/tag add`, `/tag show` jne.
  - `!t` toimii tagien näyttämiseen
- Helsingin Sanomien sarjakuvat (`!fingerpori` jne.) ja XKCD (`!xkcd`, duh)
- Teksti-TV (`!teksti-tv` tai `!ttv`)
- hirsipuu antaa nyt Kotus-napin myös ajan loppuessa
- rajoitettu MAL-hakutermin pituus 64 merkkiin API-rajoitusten takia

## 2.4.0

- lisätty !anime ja !manga -hakutoiminnot (MyAnimeList)
- päivitetty Python versioon 3.11 Docker-kuvissa

## 2.3.0

- lisätty Twitch-striimi-ilmoitukset

## 2.2.0

- päivitetty hirsipuun sanalista vuoden 2022 versioon
- kirjaimet, joita ei voi arvata, ovat nyt suoraan näkyvissä hirsipuussa
  - aiemmin tällaiset sanat jätettiin pois listalta
- lisätty hirsipuun jälkeen ilmestyvä "Hae sanaa Kotuksen sanakirjasta" -nappi
- lisätty liittymis- ja poistumistervehdykset mee6:n korvaamiseksi
- siirrytty Pillow 10.0.0:aan ja tehty kuvaprosessointi vihdoinkin kunnolla
- suurennettu GDPR-ZIP:ien kokoa Discordin salliman tiedostokoon kasvun myötä

## 2.1.0

- siirretty kaikki Postgresiin
- korjattu: embed-varoituksia ei enää anneta, jos viestissä on useampi embed

## 2.0.1

- korjattu psycopg2:n vaatimus, jotta aarch64-arkkitehtuudin Docker-kuvat toimivat

## 2.0.0

- reaktioroolit
- PostgreSQL-tietokanta
- Python-moduulit

## 1.16.0

- päivitetty discord.py 2.0:aan
- code cleanup
- napit `!hangman`-komentoon
- Discordin timeout-ominaisuuteen perustuvat mutekomennot

## 1.15.0

- 1.14.0:n muutokset toiminnassa
- Docker nyt suositeltu tapa ajaa botti, imaget ja compose-stäkki tarjolla

## 1.14.0

- tuki viimeiselle discord.py-versiolle
- tarrat kuvakomentoihin

## 1.13.0

- lisätty `!tainted`-deepfake

## 1.12.0

- lisätty `!päivänannos`
- muokattu Azure-klusterin tilan raportointia paremmaksi

## 1.11.0

- lisätty `!egg`, uusi deepfake-komento

## 1.10.1

### Muutokset

- `gifv` on nyt tuettu upotetyyppi kuvakomennoissa
- viestin sisältö tarkastetaan nyt linkkien varalta kuvakomennoissa

## 1.10.0

### Muutokset

- parannuksia azureen
- klusteria ei enää näytetä päällä kun se ei ole
- lisätty tuki linkeille, jotka eivät mene suoraan kuvaan, mutta kuva silti löytyy (Tenor)

### Lisäykset

- `!8pallo`-komento

## 1.9.1

### Muutokset

- parempi !queue: näyttää prosessoivat jobit ja kenelle kukin jobi kuuluu

### Lisäykset

- !sairasta, ihan sairas komento

## 1.9.0

### Lisäykset

- lisätty !riseandshine deepfake-komento

### Korjaukset

- korjattu: botti yritti käyttää videoita kuvina

## 1.8.5

### Korjaukset

- toivottavasti korjattu poikkeus, kun jobeja ei ole ja statusta yritetään silti hakea

## 1.8.4

### Lisäykset

- !cancel- ja !queue -komennot, joilla voi perua jobit ja nähdä jobijonon
  - vain omat jobit voi perua

## 1.8.3

### Korjaukset

- korjattu virheelliset pingit PreviousImagessa

## 1.8.2

### Lisäykset

- remote-virheiden käsittely damedaneen

## 1.8.1

### Lisäykset

- parempi virhekäsittely damedaneen

## 1.8.0

### Lisäykset

- !damedane-komento, joka tekee deepfaken
  - hidas, koska käyttää Azurea (klusteri käynnistyy hitaasti)

## 1.7.0

### Muutokset

- web-paneeli on nyt erotettu bottiprosessista
  - paneeli käyttää nyt [Circus](https://circus.readthedocs.io)-kirjastoa aliprosessien sijasta
- FrameCount hyväksyy minkä tahansa määrän ruutuja

### Korjaukset

- korjattu coinflip-komento

## v1.6.0

### Muutokset

- muutettu (lähes) kaikki interaktio käyttämään replyjä

## v1.5.0

### Lisäykset

- lisätty uusi rekisteröintisivu kontrollipaneeliin
- lisätty toiminnot kontrollipaneelin lokin nappuloihin

### Korjaukset

- tehty kontrollipaneelista mobiiliystävällisempi
- muutettu lokiAPI-routea ettei se suututtaisi uBoa
- komennot ovat nyt kirjainkoosta riippumattomia

### Korjattu

- korjattu `noloop`-flagin toimimattomuus GIF-kuvien kanssa

## v1.4.3

### Muutokset

- muokattu gif-komentojen eksklusiivista suoritusta
- varoitukset menevät nyt kunnon loggeriin

## v1.4.2

### Lisäykset

- flageja !gif-makia-komentoon
  - `noloop` estää gifin looppauksen
  - `reverse`: gif palaa takaisin alkutilaan toistaen kuvat käänteisessä järjestyksessä
  - `<number>f` tekee gifistä `number` kuvan mittaisen (esim. `30f`)
- mahdollisuus käyttää viimeksi kanavalle lähetettyä kuvaa !makiassa ja !tankissa
  - käytä `^`-merkkiä kohteena
- lisää debugloggausta imagemagick-prosessoreihin

### Muutokset

- !loglevel-komennossa voi nyt määrittää loggerin

## v1.4.1

### Korjaukset

- korjattu !gif-makian erroreita
- parempi virhekäsittely kuvien hakemiseen
- lisää debuggausta !gif-makiaan

## v1.4.0

### Lisäykset

- !makia, !gif-makia ja !gif-makia2 -komennot!
  - tee taikoja kuville ja gifeille!
  - kokeile !gif-makia2:ta ja koe vaihtoehtotaikuus!
  - jokaisen komennon voimakkuutta voi säätää numerolla
  - !gif-makia-komennon terävöityksen voimakkuutta voi säätää lisäämällä komentoon toisen numeron
  - kohdekuvan haku toimii samalla tavalla kuin !tank (oletuksena profiilikuva, tukee liitteitä, pingejä, nimiä ja linkkejä)
- sääkomento näyttää nyt kartan, missä haettu paikka sijaitsee
  - zoomaustasoa voi säätää lisäämällä komentoon numeron ennen paikkaa
  - kartan voi poistaa kokonaan lisäämällä kyselyn loppuun `nomap`

### Muutokset

- sääkomennon embedin otsikko koostuu nyt kaupungin nimestä ja maakoodista
- botti kirjoittaa sääkomennon prosessoinnin aikana

## v1.3.4

### Korjaukset

- päivitetty botti käyttämään Gateway Intentejä
- päivitetty paketteja

## v1.3.3

### Korjaus

- päivitetty Pillow-kirjasto versioon 8.0.1

## v1.3.2

### Muutos

- Halloween-botin komennot eivät enää tule poistolokiin (h!trick ja h!treat)

## v1.3.1

### Korjaukset

- error-käsittely höpsismikomentoon
- alkuperäinen teksti ei enää näy höpsismikuvassa

## v1.3.0

### Lisäys

- höpsismikomento

### Korjaus

- korjattu tyhjien merkkien havaitseminen piilotettujen linkkien yhteydessä

## v1.2.0

### Lisäykset

- lisätty ominaisuus: botti voi varoittaa kuvalinkeistä, jotka Discordin sovellus piilottaa
  - oletuksena pois päältä, voi laittaa päälle `!image-link-warn`-komennolla
  - botti ei varoita linkistä, jos domain on yksi `config.toml`:ssa määritellyistä "turvallisista" domaineista

## v1.1.1

- päivitetty discord.py versioon 1.3.4 kaatumisen korjaamiseksi

## v1.1.0

Parannuksia web appiin ja logaukseen:

### Muutos

- eri loglevelit näkyvät eri väreillä

### Lisäys

- komento loglevelin muuttamista varten (!loglevel)

## v1.0.1

Tämä on pieni korjausversio.

### Korjaukset

- korjattu: hirsipuupelin virheilmoitukset tulevat nyt näkyviin
- muiden reaktiot poistetaan nyt käynnissä olevasta hirsipuupelistä, jos pelaaja ei ole päästänyt kaikkia arvaamaan

## v1.0.0

Ensimmäinen versio, jee!

### Lisäykset

- hirsipuukomento
  - kirjoita !hirsipuu (tai !hangman) aloittaaksesi pelin
  - kirjaimia arvataan reaktioilla
  - parametrit: yksi numero antaa tasan numeron mittaisen sanan, kaksi numeroa antaa sanan, jonka pituus on näiden numeroiden väliltä
  - ~93k sanaa
- sääkomento
  - kirjoita !sää (tai !weather) tarkistaaksesi sään missä vain!
  - käyttää OpenWeatherMapin APIa

### Muutokset

- botti käyttää nyt TOMLia konfigurointiin paskojen ympäristömuuttujien sijasta
- botti huomauttaa, jos lainausmerkkejä käytetään väärin !moment-komennoissa
