"""Tags table

Revision ID: 8d26bb6fb959
Revises: 493a1b888a9e
Create Date: 2023-08-04 21:04:47.963303

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8d26bb6fb959'
down_revision = '493a1b888a9e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('tag',
    sa.Column('key', sa.Text(), nullable=False),
    sa.Column('owner_type', sa.Enum('USER', 'GUILD', 'BOT', name='tagownertype'), nullable=False),
    sa.Column('owner', sa.BigInteger(), nullable=False),
    sa.Column('creation_guild', sa.BigInteger(), nullable=False),
    sa.Column('is_global', sa.Boolean(), nullable=False),
    sa.Column('content', sa.Text(), nullable=False),
    sa.PrimaryKeyConstraint('key', 'owner_type', 'owner')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('tag')
    # ### end Alembic commands ###
