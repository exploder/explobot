import os
from logging.config import fileConfig

import toml
from sqlalchemy import engine_from_config, create_engine
from sqlalchemy import pool

from alembic import context

import db

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = db.Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def _read_url() -> str:
    config_path = os.environ.get('CONFIG_PATH')
    if config_path is None:
        raise RuntimeError('CONFIG_PATH environment variable not set')
    conf = toml.load(config_path)
    db_conf = conf.get('sqlalchemy')
    if db_conf is None:
        raise RuntimeError('[sqlalchemy] not found in config')
    conn_string = db_conf.get('connection')
    if conn_string is None:
        raise RuntimeError('Connection string not found in config')

    # Alembic doesn't do async
    conn_string = conn_string.replace('asyncpg', 'psycopg2')
    return conn_string


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    conn_string = _read_url()

    context.configure(
        url=conn_string,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    conn_string = _read_url()

    connectable = create_engine(conn_string, poolclass=pool.NullPool)
    #     engine_from_config(
    #     config.get_section(config.config_ini_section),
    #     prefix="sqlalchemy.",
    #     poolclass=pool.NullPool,
    # )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
